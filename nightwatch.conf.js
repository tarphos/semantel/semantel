module.exports = {
  webdriver: {
    start_process: true,
    server_path: "node_modules/.bin/chromedriver",
    cli_args: ["--verbose"],
    port: 9515,
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: "chrome",
        "goog:chromeOptions": {
          args: ["headless", "no-sandbox"],
        },
      },
      screenshots: {
        enabled: true,
        path: "./tests_output/nightwatch-html-report",
      },
    },
  },
};
