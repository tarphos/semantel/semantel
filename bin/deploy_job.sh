#!/bin/bash

set -euo pipefail

if [ ${PROD:?} = true ]; then
    export STACKNAME=SemantelStack
else
    export STACKNAME=SemantelStackDev$STACK_SUFFIX
    aws cloudformation describe-stacks --stack-name ${STACKNAME:?} --query "Stacks[0].Outputs" --output json | jq -rc ".[] | select(.OutputKey==\"SemantelWebsiteBucketUrl\") | .OutputValue " > url.export
fi

export BUCKET=$(aws cloudformation describe-stacks --stack-name ${STACKNAME:?} --query "Stacks[0].Outputs" --output json | jq -rc ".[] | select(.OutputKey==\"SemantelWebsiteBucket\") | .OutputValue ")
aws s3 sync out/ s3://${BUCKET:?} --delete
aws s3 cp s3://${BUCKET:?}/health.html s3://${BUCKET:?}/health.htm --cache-control "no-cache, no-store, must-revalidate"

if [ ${PROD:?} = true ]; then
    export DISTRIBUTION=$(aws cloudformation describe-stacks --stack-name ${STACKNAME:?} --query "Stacks[0].Outputs" --output json | jq -rc ".[] | select(.OutputKey==\"SemantelDistribution\") | .OutputValue ")
    aws cloudfront create-invalidation --distribution-id ${DISTRIBUTION:?} --paths / "/android-chrome-*.png" /favicon.ico "/*.html*"
fi