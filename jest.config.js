/** @type {import('jest').Config} */
const config = {
  clearMocks: true,
  resetMocks: true,
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": ["babel-jest", { presets: ["next/babel"] }],
  },
  moduleNameMapper: {
    "@/(.*)": "<rootDir>/src/$1",
    "\\.(css|less)$": "<rootDir>/src/__mocks__/styleMock.js",
  },
  collectCoverageFrom: [
    "**/*.{ts,tsx}",
    "!**/node_modules/**",
    "!.next/**",
    "!next-env.d.ts",
    "!nightwatch/**",
  ],
};

module.exports = config;
