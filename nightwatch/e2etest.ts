import { decodeBase64 } from "../src/base64";

const url = process.env.URL;
if (url === undefined) {
  throw new Error("URL is not defined");
}

const secretUrl = process.env.SECRET_URL ?? url + "/api/secret";

let secret = "";
let pns = "";

before(async () => {
  const response = await fetch(secretUrl);
  const { data, puzzleNumber } = await response.json();
  secret = decodeBase64((data as Array<string>)[data.length - 1]);
  pns = `${puzzleNumber}`;
});

const top1kUrl = `${url}/top1k${
  url.indexOf("localhost") === -1 ? ".html" : ""
}`;

const top1kSuffix = (word: string) => {
  return `?w=${encodeURIComponent(encodeBase64(word))}`;
};

const encodeBase64 = (str: string) =>
  Buffer.from(str, "utf8").toString("base64");

const top1kKnappUrl = `${top1kUrl}${top1kSuffix("knapp")}`;

afterEach(() => browser.execute(() => localStorage.clear()));

describe(url + " E2E Test", function () {
  it("should guess and show result", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .assert.titleContains("Semantel")
      .assert.visible("#rules-heading")
      .click("#rules-close")
      .assert.not.elementPresent("#rules-heading")
      .assert.textContains(
        "#similarityStory",
        "Das heutige Rätsel ist Semantel Nummer"
      )
      .assert.textMatches("#similarityStory > b", "[0-9]+")
      .setValue("#guess", " weilheim")
      .click("#guess + input")
      .assert.textContains("#guesses td + td", "weilheim");
  });

  it.skip("should guess and show result when current secret contains special characters", function () {
    // TODO make this work again
    browser
      .url(url + "?e2eSpecialCharacters=true")
      .waitForElementVisible("body")
      .assert.titleContains("Semantel")
      .assert.visible("#rules-heading")
      .click("#rules-close")
      .assert.not.elementPresent("#rules-heading")
      .assert.textContains(
        "#similarityStory",
        "Das heutige Rätsel ist Semantel Nummer"
      )
      .assert.textMatches("#similarityStory > b", "[0-9]+")
      .setValue("#guess", " weilheim")
      .click("#guess + input")
      .assert.textContains("#guesses td + td", "weilheim");
  });

  it("should show empty table when state empty", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .assert.not.textContains("#guesses", "1");
  });

  it("should show PayPal button in dialog", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .assert.not.elementPresent("#rules-heading")
      .click("#openDonate")
      .waitForElementVisible("#donate-heading")
      .assert.attributeEquals(
        "#donate-underlay > div > div > p > a",
        "href",
        "https://www.paypal.com/donate/?hosted_button_id=U47WZSCC634RJ"
      );
  });

  it("should show correct secret word when giving up", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .click("#give-up-btn")
      .acceptAlert()
      .assert.textContains("body", `Das Lösungswort ist: ${secret}`);
  });

  it("should clean up old guesses", function () {
    browser.execute(
      (pns) => {
        const puzzleNumber = parseInt(pns);
        localStorage.setItem("puzzleNumber", `${puzzleNumber - 1}`);
        localStorage.setItem(
          "guesses",
          JSON.stringify([[9.762034430134667, "qwert", null, 1]])
        );
      },
      [pns]
    );

    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .assert.not.textContains("body", "qwert");
  });

  it("should show stored guesses", function () {
    browser.execute(
      (pns) => {
        localStorage.setItem("puzzleNumber", pns);
        localStorage.setItem(
          "guesses",
          JSON.stringify([[9.762034430134667, "qwert", null, 1]])
        );
      },
      [pns]
    );

    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .assert.textContains("body", "qwert");
  });

  it("should show last guess separately", async () => {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .setValue("#guess", " weilheim")
      .click("#guess + input")
      .setValue("#guess", "augsburg")
      .click("#guess + input")
      .assert.attributeEquals(
        "#guesses td + td",
        "style",
        "color: rgb(204, 0, 204);"
      );

    const isDarkMode = await isDark();

    browser.assert.attributeEquals(
      "#guesses tr + tr + tr td + td",
      "style",
      `color: rgb(${isDarkMode ? "250, 250, 250" : "0, 0, 0"});`
    );
  });

  it("should close dialog on Escape", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .assert.visible("#rules-heading")
      .perform(function () {
        // @ts-ignore
        const actions = this.actions({ async: true });
        return actions.keyDown(browser.Keys.ESCAPE).keyUp(browser.Keys.ESCAPE);
      })
      .assert.not.elementPresent("#rules-heading");
  });

  it("should not reveal solution if last day's solution was shown", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .execute(
        (pns) => {
          const puzzleNumber = parseInt(pns);
          localStorage.setItem("winState", "0");
          localStorage.setItem(
            "stats",
            JSON.stringify({
              firstPlay: 1,
              lastEnd: puzzleNumber - 1,
              lastPlay: puzzleNumber - 1,
              winStreak: 1,
              playStreak: 0,
              totalGuesses: 8,
              wins: 1,
              giveups: 0,
              abandons: 1,
              totalPlays: 2,
              hints: 0,
            })
          );
          localStorage.setItem("puzzleNumber", `${puzzleNumber - 1}`);
        },
        [pns]
      )
      .refresh()
      .assert.not.textContains("body", `Das Lösungswort ist: ${secret}.`);
  });

  it("should not set stats to 'undefined'", async function () {
    await browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .setValue("#guess", " weilheim")
      .click("#guess + input")
      .assert.textContains("body", `weilheim`);

    const stats1 = await getStats();
    browser.assert.notEqual(stats1, undefined);

    await browser.refresh();

    const stats2 = await getStats();
    await browser.assert.deepEqual(stats2, stats1);
  });

  it("should show 'GEFUNDEN' when secret is found", async function () {
    await browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .setValue("#guess", secret)
      .click("#guess + input")
      .assert.textContains("body", "GEFUNDEN");
  });

  it("should update winning streak correctly", function () {
    browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .execute(
        (pns) => {
          const puzzleNumber = parseInt(pns);
          localStorage.setItem(
            "stats",
            JSON.stringify({
              firstPlay: 1,
              lastEnd: puzzleNumber - 1,
              lastPlay: puzzleNumber - 1,
              winStreak: 1,
              playStreak: 0,
              totalGuesses: 8,
              wins: 1,
              giveups: 0,
              abandons: 1,
              totalPlays: 2,
              hints: 0,
            })
          );
          localStorage.setItem("puzzleNumber", `${puzzleNumber - 1}`);
        },
        [pns]
      )
      .refresh()
      .setValue("#guess", secret)
      .click("#guess + input")
      .assert.textEquals(
        '[data-testid="responseStatistics"] > tbody > tr:nth-child(4) > td',
        "2"
      );
  });

  it("should not update stats when guessing after won", async function () {
    await browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .setValue("#guess", secret)
      .click("#guess + input")
      .assert.textContains(
        'p[data-testid="won"]',
        "Du hast es mit 1 Versuch gefunden!"
      )
      .setValue("#guess", "weilheim")
      .click("#guess + input")
      .assert.textContains(
        'p[data-testid="won"]',
        "Du hast es mit 1 Versuch gefunden!"
      );
  });

  it("should not count guesses made after won after reload", async function () {
    await browser
      .url(url)
      .waitForElementVisible("body")
      .click("#rules-close")
      .setValue("#guess", secret)
      .click("#guess + input")
      .setValue("#guess", "weilheim")
      .click("#guess + input")
      .refresh()
      .assert.textContains(
        'p[data-testid="won"]',
        "Du hast es mit 1 Versuch gefunden!"
      );
  });
});

describe(top1kUrl + " E2E Test", function () {
  it("should show top1k for 'knapp'", function () {
    browser
      .url(top1kKnappUrl)
      .waitForElementVisible("body")
      .click("#rules-close")
      .assert.titleContains("Semantel: nächste Wörter")
      .assert.textEquals(
        ".nearestTable > caption",
        "Die Wörter, die knapp am nächsten sind, sind:"
      )
      .assert.textEquals(
        ".nearestTable > tbody > tr:nth-child(2)",
        "998 etwa 54,59"
      )
      .assert.textEquals(
        ".nearestTable > tbody > tr:nth-child(998)",
        "2 erkletterung 21,07"
      );
  });

  it("should show top1k for today's secret", function () {
    browser
      .url(`${top1kUrl}${top1kSuffix(secret)}`)
      .waitForElementVisible("body")
      .click("#rules-close") // TODO avoid this in most tests
      .assert.titleContains("Semantel: nächste Wörter")
      .assert.textEquals(
        ".nearestTable > caption",
        "Die Wörter, die " + secret + " am nächsten sind, sind:"
      )
      .assert.textMatches(".nearestTable > tbody > tr:nth-child(2)", "^998 ")
      .assert.textMatches(".nearestTable > tbody > tr:nth-child(998)", "2 ");
  });
});

const getStats = async () => {
  let stats;
  await browser.execute(
    () => {
      return localStorage.getItem("stats");
    },
    [],
    (result) => {
      stats =
        typeof result.value === "string" ? JSON.parse(result.value) : undefined;
    }
  );
  return stats;
};
const isDark = async () => {
  let isDark;
  await browser.execute(
    () => {
      return document.querySelector("body").getAttribute("class");
    },
    [],
    (result) => {
      isDark = result.value === "dark";
    }
  );
  return isDark;
};
