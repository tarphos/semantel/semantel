import assert from "assert";
import { confirmIfNotGameOver } from "./confirm";

describe("confirm", function () {
  describe("#confirmIfNotGameOver()", function () {
    let actionCalled = false;
    const action = async () => {
      actionCalled = true;
    };

    beforeEach(() => (actionCalled = false));

    it("should not do anything when gameOver", () => {
      global.confirm = () => true;
      confirmIfNotGameOver(true, "foo", action);

      assert.equal(actionCalled, false);
    });

    it("should not do anything when not confirmed", () => {
      global.confirm = () => false;
      confirmIfNotGameOver(false, "foo", action);

      assert.equal(actionCalled, false);
    });

    it("should call action when confirmed", () => {
      global.confirm = () => true;
      confirmIfNotGameOver(false, "foo", action);

      assert.equal(actionCalled, true);
    });
  });
});
