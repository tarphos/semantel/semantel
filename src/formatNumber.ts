export default function formatNumber(num: number): string {
  return num.toFixed(2).replace(".", ",");
}
