/**
 * @jest-environment jsdom
 */

import { waitFor } from "@testing-library/dom";
import { renderHook } from "@testing-library/react";
import useSecrets from "./useSecrets";

jest.mock("fetch-retry"); // can't import data module otherwise
const dataModule = require("../src/data");

describe("useSecrets", () => {
  it("should fetch secrets and puzzle number on mount", async () => {
    const secrets = [
      "secret1",
      "secret2",
      "secret3",
      "secret4",
      "secret5",
      "secret6",
      "secret7",
      "secret8",
      "secret9",
    ];
    const puzzleNumber = 42;
    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockResolvedValue({
        puzzleNumber,
        secrets,
      });
    const setError = jest.fn();

    const { result } = renderHook(() => useSecrets(setError));

    await waitFor(() => {
      expect(result.current[0]).toBe(secrets);
      expect(result.current[1]).toBe(puzzleNumber);
    });

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
    expect(setError).not.toHaveBeenCalled();
  });

  it("should be able to reset secrets and puzzleNumber", async () => {
    const secrets = [
      "secret1",
      "secret2",
      "secret3",
      "secret4",
      "secret5",
      "secret6",
      "secret7",
      "secret8",
      "secret9",
    ];
    const puzzleNumber = 42;
    jest.spyOn(dataModule, "getSecrets").mockResolvedValue({
      puzzleNumber,
      secrets,
    });
    const setError = jest.fn();

    const { result } = renderHook(() => useSecrets(setError));

    await waitFor(() => {
      expect(result.current[0]).not.toBeUndefined();
    });

    result.current[2]();
    await waitFor(() => {
      expect(result.current[0]).toBe(undefined);
      expect(result.current[1]).toBe(undefined);
    });
  });

  it("should handle error when secrets array is too short", async () => {
    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockResolvedValue({
        puzzleNumber: 42,
        secrets: ["secret1", "secret2"],
      });
    const setError = jest.fn();
    console.error = jest.fn();

    renderHook(() => useSecrets(setError));

    await waitFor(() => {
      expect(setError).toHaveBeenCalledWith(
        "Fehler beim Laden des Lösungsworts. Bitte Netzwerkverbindung überprüfen und die Seite neu laden. Sollte der Fehler weiterhin bestehen, bitte melden."
      );
    });
    expect(console.error).toHaveBeenCalledWith("Secrets array is too short");

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
  });

  it("should handle error when fetching secrets fails", async () => {
    const errorMessage = "Error fetching secrets";
    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockRejectedValue(errorMessage);
    const setError = jest.fn();
    console.error = jest.fn();

    renderHook(() => useSecrets(setError));

    await waitFor(() => {
      expect(setError).toHaveBeenCalledWith(
        "Fehler beim Laden des Lösungsworts. Bitte Netzwerkverbindung überprüfen und die Seite neu laden. Sollte der Fehler weiterhin bestehen, bitte melden."
      );
    });
    expect(console.error).toHaveBeenCalledWith(errorMessage);

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
  });

  it("should fetch secrets and puzzle again between 0 and 1am", async () => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date("2023-04-27T00:00:01Z"));

    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockResolvedValueOnce({
        puzzleNumber: 42,
        secrets: [
          "secret1",
          "secret2",
          "secret3",
          "secret4",
          "secret5",
          "secret6",
          "secret7",
          "secret8",
          "secret9",
        ],
      })
      .mockResolvedValueOnce({
        puzzleNumber: 43,
        secrets: [
          "secret10",
          "secret11",
          "secret12",
          "secret13",
          "secret14",
          "secret15",
          "secret16",
          "secret17",
          "secret18",
        ],
      });
    const setError = jest.fn();

    const { result } = renderHook(() => useSecrets(setError));

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
    expect(setError).not.toHaveBeenCalled();
    await waitFor(() => expect(result.current[1]).toBe(42));

    jest.advanceTimersByTime(45 * 60 * 1000);

    await waitFor(() => {
      expect(mockGetSecrets).toHaveBeenCalledTimes(2);
    });
    expect(setError).not.toHaveBeenCalled();
    await waitFor(() => expect(result.current[1]).toBe(43));

    jest.useRealTimers();
  });

  it("should not fetch secrets and puzzle again if it's not between 0 and 1am", async () => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date("2023-04-27T09:00:01Z"));

    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockResolvedValueOnce({
        puzzleNumber: 42,
        secrets: [
          "secret1",
          "secret2",
          "secret3",
          "secret4",
          "secret5",
          "secret6",
          "secret7",
          "secret8",
          "secret9",
        ],
      })
      .mockResolvedValueOnce({
        puzzleNumber: 43,
        secrets: [
          "secret10",
          "secret11",
          "secret12",
          "secret13",
          "secret14",
          "secret15",
          "secret16",
          "secret17",
          "secret18",
        ],
      });
    const setError = jest.fn();

    renderHook(() => useSecrets(setError));

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
    expect(setError).not.toHaveBeenCalled();

    jest.advanceTimersByTime(45 * 60 * 1000);

    await waitFor(() => {
      expect(mockGetSecrets).toHaveBeenCalledTimes(1);
    });
    expect(setError).not.toHaveBeenCalled();

    jest.useRealTimers();
  });

  it("should clear interval on unmount", async () => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date("2023-04-27T23:20:00Z"));

    const mockGetSecrets = jest
      .spyOn(dataModule, "getSecrets")
      .mockResolvedValueOnce({
        puzzleNumber: 42,
        secrets: [
          "secret1",
          "secret2",
          "secret3",
          "secret4",
          "secret5",
          "secret6",
          "secret7",
          "secret8",
          "secret9",
        ],
      })
      .mockResolvedValueOnce({
        puzzleNumber: 43,
        secrets: [
          "secret10",
          "secret11",
          "secret12",
          "secret13",
          "secret14",
          "secret15",
          "secret16",
          "secret17",
          "secret18",
        ],
      })
      .mockImplementation(() => {
        throw new Error("Should not be called");
      });
    const setError = jest.fn();

    const { result, unmount } = renderHook(() => useSecrets(setError));

    expect(mockGetSecrets).toHaveBeenCalledTimes(1);
    expect(setError).not.toHaveBeenCalled();
    await waitFor(() => expect(result.current[1]).toBe(42));

    jest.advanceTimersByTime(45 * 60 * 1000);

    await waitFor(() => {
      expect(mockGetSecrets).toHaveBeenCalledTimes(2);
    });
    expect(setError).not.toHaveBeenCalled();
    await waitFor(() => expect(result.current[1]).toBe(43));

    unmount();

    jest.advanceTimersByTime(45 * 60 * 1000);

    expect(mockGetSecrets).toHaveBeenCalledTimes(2);
    expect(setError).not.toHaveBeenCalled();

    jest.useRealTimers();
  });
});
