import { decodeBase64, encodeBase64 } from "./base64";
import { doGuess } from "./guess";
import {
  HintParams,
  Model2Type,
  SimilarityStoryType,
  Top1kEntry,
} from "./types";
import { baseUrl } from "./urls";
import { hintNumber } from "./utils";

const fetch = require("fetch-retry")(global.fetch);

export async function getModel(
  word: string,
  secret: string
): Promise<Model2Type | undefined> {
  const isSecret = word === secret;
  const vecUrl = isSecret
    ? baseUrl + "/modelv2/" + encodeURIComponent(encodeBase64(word))
    : baseUrl + "/model/" + word.replace(/[ /#]/gi, "_");
  const percentileUrl = isSecret
    ? undefined
    : baseUrl +
      "/percentilev2/" +
      encodeURIComponent(encodeBase64(secret)) +
      "/" +
      word.replace(/[ /#]/gi, "_");

  const vecRequest = fetch(vecUrl);
  const percentileRequest =
    percentileUrl === undefined ? undefined : fetch(percentileUrl);

  const vecResponse = await vecRequest;

  switch (vecResponse.status) {
    case 200: {
      const vec = await vecResponse.json();
      let percentile;
      if (percentileRequest !== undefined) {
        const percentileResponse = await percentileRequest;
        switch (percentileResponse.status) {
          case 200:
            percentile = await percentileResponse.json();
            break;
          case 404:
            percentile = undefined;
            break;
          default:
            throw new Error(`Could not get percentile data for ${word}`);
        }
      } /*- can only happen if word === secret -*/ else {
        percentile = 1000;
      }

      return { vec, percentile };
    }
    case 404:
      return undefined;
    default:
      throw new Error(`Could not get word2vec data for ${word}`);
  }
}

export async function getSimilarityStory(
  secretLocal: string
): Promise<SimilarityStoryType | undefined> {
  const url =
    baseUrl + "/similarityv2/" + encodeURIComponent(encodeBase64(secretLocal));
  const response = await fetch(url);
  return await response.json();
}

export async function getNearby(word: string): Promise<Array<string>> {
  const url = baseUrl + "/nearby/" + word;
  const response = await fetch(url);
  return await response.json();
}

export async function hint(params: HintParams) {
  const n = hintNumber(params.guesses);
  // Stryker disable next-line EqualityOperator
  if (n <= 0) {
    // 0 is never returned by hintNumber
    params.alert("Keine weiteren Hinweise verfügbar.");
  } else {
    const url = baseUrl + "/nth_nearby/" + params.secret + "/" + n;
    const response = await fetch(url);
    try {
      const hint_word = await response.json();
      params.setHintsUsed((hintsUsed) => hintsUsed + 1);
      await doGuess({
        guess: hint_word,
        is_hint: true,
        puzzleKey: params.puzzleKey,
        secret: params.secret,
        secretVec: params.secretVec,
        setChronoForward: params.setChronoForward,
        setError: params.setError,
        setLatestGuess: params.setLatestGuess,
        stats: params.stats,
        setStats: params.setStats,
        puzzleNumberFromStorage: params.puzzleNumberFromStorage,
        winState: params.winState,
        setWinState: params.setWinState,
        guesses: params.guesses,
        setGuesses: params.setGuesses,
        hintsUsed: params.hintsUsed,
        setNumberOfGuessesToEnd: params.setNumberOfGuessesToEnd,
      });
    } catch (e) {
      console.error(e);
      params.alert("Holen von Hinweis fehlgeschlagen");
    }
  }
}

export async function getSecrets(): Promise<{
  puzzleNumber: number;
  secrets: Array<string>;
}> {
  const secretUrl = baseUrl + "/secret";
  const response = await fetch(secretUrl);

  if (response.ok) {
    const { puzzleNumber, data } = await response.json();
    const secrets = (data as Array<string>).map(decodeBase64);
    return { puzzleNumber, secrets };
  } else {
    throw new Error(
      `Could not get secrets (HTTP status ${response.status}): ${response.statusText}`
    );
  }
}

export async function getNearby1k(word: string): Promise<Array<Top1kEntry>> {
  const url = baseUrl + "/nearby_1k/" + encodeURIComponent(word);
  const response = await fetch(url);
  return await response.json();
}
