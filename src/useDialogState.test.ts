/**
 * @jest-environment jsdom
 */

import { act, renderHook } from "@testing-library/react";
import useDialogState from "./useDialogState";

describe("useDialogState", () => {
  it("should open and close dialog", () => {
    const { result } = renderHook(() => useDialogState());

    expect(result.current[0]).toBe(false);

    act(result.current[1]); // open

    expect(result.current[0]).toBe(true);

    act(result.current[2]); // close

    expect(result.current[0]).toBe(false);
  });
});
