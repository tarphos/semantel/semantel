import {
  readRules,
  storageKey_guesses,
  storageKey_hints_used,
  storageKey_numberOfGuessesToEnd,
  storageKey_prefersDarkColorScheme,
  storageKey_puzzleNumber,
  storageKey_stats,
  storageKey_winState,
} from "@/constants/localStorageKeys";

describe("localStorageKeys", () => {
  test("readRules constant is correct", () => {
    expect(readRules).toBe("readRules");
  });

  test("storageKey_stats constant is correct", () => {
    expect(storageKey_stats).toBe("stats");
  });

  test("storageKey_puzzleNumber constant is correct", () => {
    expect(storageKey_puzzleNumber).toBe("puzzleNumber");
  });

  test("storageKey_winState function is correct", () => {
    expect(storageKey_winState("suffix")).toBe("winStatesuffix");
  });

  test("storageKey_guesses function is correct", () => {
    expect(storageKey_guesses("suffix")).toBe("guessessuffix");
  });

  test("storageKey_hints_used function is correct", () => {
    expect(storageKey_hints_used("suffix")).toBe("hints_usedsuffix");
  });

  test("storageKey_prefersDarkColorScheme constant is correct", () => {
    expect(storageKey_prefersDarkColorScheme).toBe("prefersDarkColorScheme");
  });

  test("storageKey_numberOfGuessesToEnd constant is correct", () => {
    expect(storageKey_numberOfGuessesToEnd).toBe("numberOfGuessesToEnd");
  });
});
