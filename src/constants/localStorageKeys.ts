export const readRules = "readRules";
export const storageKey_stats = "stats";
export const storageKey_puzzleNumber = "puzzleNumber";
export const storageKey_winState = (suffix: string) => `winState${suffix}`;
export const storageKey_guesses = (suffix: string) => `guesses${suffix}`;
export const storageKey_hints_used = (suffix: string) => `hints_used${suffix}`;
export const storageKey_prefersDarkColorScheme = "prefersDarkColorScheme";
export const storageKey_numberOfGuessesToEnd = "numberOfGuessesToEnd";
