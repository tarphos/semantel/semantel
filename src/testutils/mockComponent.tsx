import { v4 as uuidv4 } from "uuid";

export function mockComponent(
  module: any,
  content?: (props: any) => React.ReactNode
): TestIdAndMockedComponent {
  const testId = uuidv4();
  const mockedComponent = jest.fn();

  jest.replaceProperty(module, "default", (props: any) => {
    mockedComponent(props);
    return (
      <div data-testid={testId}>
        {content
          ? content(props)
          : // Stryker disable next-line StringLiteral
            `Mocked ${testId}`}
      </div>
    );
  });

  return { testId, mockedComponent };
}

type TestIdAndMockedComponent = { testId: string; mockedComponent: jest.Mock };
