import copy from "copy-to-clipboard";

export const copyToClipboard = (message: string) => copy(message);
