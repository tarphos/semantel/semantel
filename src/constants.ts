import { GuessesEntry, Stats } from "./types";

export const INITIAL_DAY = 19089;

export const EMPTY_STATS: Stats = {
  firstPlay: 0,
  lastEnd: 0,
  lastPlay: 0,
  winStreak: 0,
  playStreak: 0,
  totalGuesses: 0,
  wins: 0,
  giveups: 0,
  abandons: 0,
  totalPlays: 0,
  hints: 0,
};

export const DEFAULT_WIN_STATE = -1;

export const DEFAULT_GUESSES: Array<GuessesEntry> = [];

export const DEFAULT_numberOfGuessesToEnd = -1;
