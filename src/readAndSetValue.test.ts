/**
 * @jest-environment jsdom
 */

import { readAndSetValue } from "./readAndSetValue";

describe("readAndSetValue", () => {
  const key = "key";
  const defaultValue = { a: 0 };
  const setter = jest.fn();
  let ref = { current: undefined };
  const defaultStoredValue = { a: 1 };
  const defaultMapper = (originalValue: any) => ({ ...originalValue, b: 2 });
  const defaultUpdater = (originalValue: any) => ({ a: originalValue.a + 1 });

  beforeEach(() => {
    ref = { current: undefined };
  });

  afterEach(() => {
    window.localStorage.clear();
  });

  it("should set default value if no value stored", () => {
    testReadAndSetValue({ expectedValue: defaultValue });
  });

  it("should set stored value", () => {
    window.localStorage.setItem(key, JSON.stringify(defaultStoredValue));

    testReadAndSetValue({ expectedValue: defaultStoredValue });
  });

  it("should set stored value with mapper", () => {
    window.localStorage.setItem(key, JSON.stringify(defaultStoredValue));

    testReadAndSetValue({
      mapper: defaultMapper,
      expectedValue: defaultMapper(defaultStoredValue),
    });
  });

  it("should set stored value with updater", () => {
    window.localStorage.setItem(key, JSON.stringify(defaultStoredValue));

    testReadAndSetValue({
      updater: defaultUpdater,
      expectedValue: defaultUpdater(defaultStoredValue),
    });
  });

  it("should set stored value with mapper and updater", () => {
    window.localStorage.setItem(key, JSON.stringify(defaultStoredValue));

    testReadAndSetValue({
      mapper: defaultMapper,
      updater: defaultUpdater,
      expectedValue: defaultUpdater(defaultMapper(defaultStoredValue)),
    });
  });

  it("should set stored value if undefined", () => {
    window.localStorage.setItem(key, "undefined");
    testReadAndSetValue({ expectedValue: undefined });
  });

  type TestType = { a: number } | undefined;
  function testReadAndSetValue(props: {
    mapper?: (originalValue: any) => TestType;
    updater?: (originalValue?: TestType) => TestType | undefined;
    expectedValue: TestType;
  }) {
    const result = readAndSetValue(
      key,
      { a: 0 },
      setter,
      props.mapper,
      props.updater,
      ref
    );

    expect(result).toStrictEqual(props.expectedValue);
    expect(setter).toHaveBeenCalledWith(props.expectedValue);
    expect(ref.current).toStrictEqual(props.expectedValue);
    if (props.updater !== undefined) {
      expect(window.localStorage.getItem(key)).toBe(
        JSON.stringify(props.expectedValue)
      );
    }
  }
});
