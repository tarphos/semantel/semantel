import withLoadingSpinnerAndFallback from "@/withLoadingSpinnerAndFallback";

describe("withLoadingSpinnerAndFallback", () => {
  it("should return component if all props are defined", () => {
    const foo = "foo";
    const bar = "bar";

    const component = withLoadingSpinnerAndFallback<{
      foo?: string;
      bar?: string;
    }>((props) => <div>{props.foo + props.bar}</div>, { foo, bar });

    expect(component.type).toBe("div");
    expect(component.props.children).toBe(foo + bar);
  });

  it("should return loading spinner if only some props are defined", () => {
    const foo = "foo";
    const bar = undefined;

    const component = withLoadingSpinnerAndFallback<{
      foo?: string;
      bar?: string;
    }>((props) => <div>{props.foo + props.bar}</div>, { foo, bar });

    expect((component.type.type as Function).name).toBe(
      "LoadingSpinnerOverlay_"
    );
    expect(component.props.active).toBe(true);
    expect(component.props.children.type).toBe("div");
    expect(component.props.children.props.children).toBe("Loading...");
  });

  it("should return loading spinner if no props are defined", () => {
    const foo = undefined;
    const bar = undefined;

    const component = withLoadingSpinnerAndFallback<{
      foo?: string;
      bar?: string;
    }>((props) => <div>{props.foo + props.bar}</div>, { foo, bar });

    expect((component.type.type as Function).name).toBe(
      "LoadingSpinnerOverlay_"
    );
    expect(component.props.active).toBe(true);
    expect(component.props.children.type).toBe("div");
    expect(component.props.children.props.children).toBe("Loading...");
  });
});
