import LoadingSpinnerOverlay from "./components/LoadingSpinnerOverlay";

export default function withLoadingSpinnerAndFallback<T extends object>(
  comp: (props: Required<T>) => JSX.Element,
  componentProps: T
) {
  let allPropsPresent =
    Object.entries(componentProps).find(([_, value]) => value === undefined) ===
    undefined;

  if (allPropsPresent) {
    const requiredProps = componentProps as Required<T>;
    return comp(requiredProps);
  } else {
    // Stryker disable ObjectLiteral,StringLiteral : TODO use external css
    return (
      <LoadingSpinnerOverlay active={true}>
        <div style={{ height: "100px" }}>Loading...</div>
      </LoadingSpinnerOverlay>
    );
    // Stryker restore ObjectLiteral,StringLiteral
  }
}
