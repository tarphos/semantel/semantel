import { DEFAULT_numberOfGuessesToEnd } from "./constants";
import { copyToClipboard } from "./copyToClipboard";
import formatNumber from "./formatNumber";
import { GuessesEntry } from "./types";

export function share(
  puzzleNumber: number,
  won: boolean,
  guesses: Array<GuessesEntry>,
  hintsUsed: number,
  numberOfGuessesToEnd: number
) {
  const hints_used = hintsUsed;
  const text = solveStory(
    numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
      ? guesses
      : guesses.slice(0, numberOfGuessesToEnd),
    puzzleNumber,
    won,
    hints_used
  );
  const copied = copyToClipboard(text);

  if (copied) {
    window.alert("In die Zwischenablage kopiert");
  } else {
    window.alert("Fehler beim Kopieren in die Zwischenablage");
  }
}

export function solveStory(
  guessesLocal: GuessesEntry[],
  puzzleNumberLocal: number,
  won: boolean,
  hintsUsedLocal: number
) {
  const guess_count = guessesLocal.length;
  if (guess_count === 0) {
    return `Ich habe Semantel ${puzzleNumberLocal} aufgegeben, ohne überhaupt einmal zu raten. https://semantel.tarphos.de/`;
  }

  const guesses_less_hints = guess_count - hintsUsedLocal;

  if (guess_count === 1) {
    return finishedAfterOneGuess(won, puzzleNumberLocal);
  }

  const guesses_chrono = guessesLocal.slice();
  guesses_chrono.sort(function (a, b) {
    return a.index - b.index;
  });

  const first_guess = `Mein erster Tipp ${describe(guesses_chrono[0])}. `;

  const first_hit = getFirstHit(guesses_chrono);

  let last_guess_msg;
  if (won) {
    const penultimate_guess = guesses_chrono[guesses_chrono.length - 2];
    last_guess_msg = `Mein vorletzter Tipp ${describe(penultimate_guess)}.`;
  } else {
    const last_guess = guesses_chrono[guesses_chrono.length - 1];
    last_guess_msg = `Mein letzter Tipp ${describe(last_guess)}.`;
  }

  let hints = "";
  if (hintsUsedLocal > 0) {
    hints = ` und ${hintsUsedLocal} ${plural(hintsUsedLocal, "Hinweis")}`;
  }

  const solved = won ? "gelöst" : "aufgegeben";
  return `Ich habe Semantel #${puzzleNumberLocal} mit ${guesses_less_hints} Versuchen${hints} ${solved}. ${first_guess}${first_hit}${last_guess_msg} https://semantel.tarphos.de/`;
}

export function plural(count: number, word: string): string {
  if (count === 1) {
    return word;
  }

  return word + "en";
}

export function finishedAfterOneGuess(
  won: number | boolean,
  puzzleNumberLocal: number
) {
  if (won) {
    return `Ich habe Semantel ${puzzleNumberLocal} mit dem ersten Versuch gelöst! https://semantel.tarphos.de/`;
  } else {
    return `Ich habe Semantel ${puzzleNumberLocal} nach dem ersten Versuch aufgegeben! https://semantel.tarphos.de/`;
  }
}

export function describe(guessLocal: GuessesEntry) {
  const similarityLocal = guessLocal.similarity;
  const percentileLocal = guessLocal.percentile;
  let out = `hatte eine Ähnlichkeit von ${formatNumber(similarityLocal)}`;
  if (percentileLocal) {
    out += ` (${percentileLocal}/1000)`;
  }
  return out;
}

export function getFirstHit(guesses_chrono: GuessesEntry[]) {
  const first_guess_in_top = !!guesses_chrono[0].percentile;
  if (!first_guess_in_top) {
    for (const entry of guesses_chrono) {
      const percentile = entry.percentile;
      if (percentile) {
        const guess_number = entry.index;
        return `Mein erstes Wort in den Top-1000 war bei Versuch #${guess_number}. `;
      }
    }
  }
  return "";
}
