import {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from "react";
import { getSecrets } from "./data";

const EVERY_45_MINUTES = 45 * 60 * 1000;

const useSecrets: (
  setError: Dispatch<SetStateAction<string | undefined>>
) => [Array<string> | undefined, number | undefined, () => void] = (
  setError
) => {
  const [secrets, setSecrets] = useState<Array<string>>();
  const [puzzleNumber, setPuzzleNumber] = useState<number>();

  useEffect(
    () => {
      getSecrets()
        .then(({ puzzleNumber: newPuzzleNumber, secrets: newSecrets }) => {
          if (newSecrets.length < 9) {
            handleSecretsError(setError)("Secrets array is too short");
          } else {
            setSecrets(newSecrets);
            setPuzzleNumber(newPuzzleNumber);
          }
        })
        .catch(handleSecretsError(setError));
    },
    // Stryker disable next-line ArrayDeclaration
    [setError]
  );

  useEffect(
    () => {
      const interval = setInterval(() => {
        const newLocal_1 = new Date();
        const newLocal = newLocal_1.getUTCHours();
        if (newLocal === 0) {
          getSecrets()
            .then(({ puzzleNumber: newPuzzleNumber, secrets: newSecrets }) => {
              // Stryker disable next-line ConditionalExpression : This is just an optimization
              if (newPuzzleNumber !== puzzleNumber) {
                setPuzzleNumber(newPuzzleNumber);
                setSecrets(newSecrets);
              }
            })
            .catch(handleSecretsError(setError));
        }
      }, EVERY_45_MINUTES);

      return () => clearInterval(interval);
    },
    // Stryker disable next-line ArrayDeclaration
    [puzzleNumber, setError]
  );

  const reset = useCallback(
    () => {
      setSecrets(undefined);
      setPuzzleNumber(undefined);
    },
    // Stryker disable next-line ArrayDeclaration
    []
  );

  return [secrets, puzzleNumber, reset];
};

function handleSecretsError(
  setError: Dispatch<SetStateAction<string | undefined>>
): (reason: any) => void {
  return (e) => {
    console.error(e);
    setError(
      "Fehler beim Laden des Lösungsworts. " +
        "Bitte Netzwerkverbindung überprüfen und die Seite neu laden. " +
        "Sollte der Fehler weiterhin bestehen, bitte melden."
    );
  };
}

export default useSecrets;
