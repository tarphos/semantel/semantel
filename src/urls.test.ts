/**
 * @jest-environment jsdom
 */

import "jest-location-mock";
import { top1kUrl } from "@/urls";

describe("urls", () => {
  describe("top1kUrl", () => {
    it("should return the correct URL from live URL", () => {
      window.location.assign("https://semantel.tarphos.de/");
      const url = top1kUrl("abc");

      expect(url).toBe("top1k.html?w=abc");
    });

    it("should return the correct URL from localhost", () => {
      window.location.assign("http://localhost:8080/");
      const url = top1kUrl("abc");

      expect(url).toBe("top1k?w=abc");
    });
  });
});
