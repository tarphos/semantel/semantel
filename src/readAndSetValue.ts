import { Dispatch, MutableRefObject, SetStateAction } from "react";

export function readAndSetValue<T>(
  storageKey: string,
  defaultValue: T,
  setter: Dispatch<SetStateAction<T>>,
  mapper?: (rawValue: any) => T,
  updater?: (mappedValue: T) => T | undefined,
  ref?: MutableRefObject<T>
) {
  const rawValueFromStorage = window.localStorage.getItem(storageKey);
  let valueFromStorage: T;
  if (rawValueFromStorage === null) {
    valueFromStorage = defaultValue;
  } else {
    const parsedRawValueFromStorage =
      rawValueFromStorage === "undefined"
        ? undefined
        : JSON.parse(rawValueFromStorage);
    if (mapper !== undefined) {
      valueFromStorage = mapper(parsedRawValueFromStorage);
    } else {
      valueFromStorage = parsedRawValueFromStorage;
    }
  }
  valueFromStorage = updater?.(valueFromStorage) ?? valueFromStorage;
  if (updater !== undefined) {
    const newValue = JSON.stringify(valueFromStorage);
    // Stryker disable next-line ConditionalExpression : only an optimization
    if (newValue !== rawValueFromStorage) {
      window.localStorage.setItem(storageKey, newValue);
    }
  }

  setter(valueFromStorage);
  if (ref !== undefined) ref.current = valueFromStorage;
  return valueFromStorage;
}
