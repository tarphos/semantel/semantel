import formatNumber from "@/formatNumber";

describe("formatSimilarity", () => {
  describe("#formatSimilarity()", () => {
    it("should format 0.23456 correctly", () => {
      const result = formatNumber(0.23456);
      expect(result).toBe("0,23");
    });

    it("should format 0.34567 correctly", () => {
      const result = formatNumber(0.34567);
      expect(result).toBe("0,35");
    });

    it("should format 0.4 correctly", () => {
      const result = formatNumber(0.4);
      expect(result).toBe("0,40");
    });
  });
});
