import { saveGame } from "./saveGame";
import { SaveGameParams } from "./types";

describe("saveGame", () => {
  describe("#saveGame()", function () {
    it("should not save if puzzle keys don't match", function () {
      const setWinState = jest.fn();
      const setGuesses = jest.fn();

      const params: SaveGameParams = {
        puzzleKey: 23,
        newWinState: 0,
        newGuesses: [],
        setGuesses,
        setWinState,
        puzzleNumberFromStorage: 42,
      };

      saveGame(params);

      shouldNotSave(setGuesses, setWinState);
    });

    it("should not save if there's no stored puzzle key", function () {
      const setWinState = jest.fn();
      const setGuesses = jest.fn();

      const params: SaveGameParams = {
        puzzleKey: 23,
        newWinState: 0,
        newGuesses: [],
        setGuesses,
        setWinState,
        puzzleNumberFromStorage: 0,
      };

      saveGame(params);

      shouldNotSave(setGuesses, setWinState);
    });

    it("should save game", function () {
      const setWinState = jest.fn();
      const setGuesses = jest.fn();

      const params: SaveGameParams = {
        puzzleKey: 23,
        newWinState: 0,
        newGuesses: [{ similarity: 1, word: "foo", percentile: 3, index: 5 }],
        setGuesses,
        setWinState,
        puzzleNumberFromStorage: 23,
      };

      saveGame(params);

      shouldSave(params, setGuesses, setWinState);
    });
  });
});

function shouldSave(
  params: SaveGameParams,
  setGuesses: jest.Mock,
  setWinState: jest.Mock
) {
  expect(setWinState).toHaveBeenCalledWith(0);
  expect(setGuesses).toHaveBeenCalledWith(params.newGuesses);
}

function shouldNotSave(setGuesses: jest.Mock, setWinState: jest.Mock) {
  expect(setWinState).not.toHaveBeenCalled();
  expect(setGuesses).not.toHaveBeenCalled();
}
