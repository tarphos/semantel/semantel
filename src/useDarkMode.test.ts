/**
 * @jest-environment jsdom
 */

import useDarkMode from "@/useDarkMode";
import { renderHook, waitFor } from "@testing-library/react";
import { act } from "react";

const browserSetToDarkMode = true;
jest.mock("react-responsive", () => ({
  useMediaQuery: () => browserSetToDarkMode,
}));

describe("useDarkMode", () => {
  it.each([[true], [false]])(
    "should set dark mode from localStorage setting (%s)",
    (darkModeFromLocalStorage) => {
      localStorage.setItem(
        "prefersDarkColorScheme",
        `${darkModeFromLocalStorage}`
      );

      const { result } = renderHook(() => useDarkMode());

      expect(result.current.darkMode).toBe(darkModeFromLocalStorage);
      expect(result.current.storedDarkMode).toBe(darkModeFromLocalStorage);
    }
  );

  it("should set stored dark mode from undefined localStorage setting", () => {
    localStorage.setItem("prefersDarkColorScheme", "undefined");

    const { result } = renderHook(() => useDarkMode());

    expect(result.current.darkMode).toBe(browserSetToDarkMode);
    expect(result.current.storedDarkMode).toBeUndefined();
  });

  it("should set and persist dark mode", async () => {
    const { result } = renderHook(() => useDarkMode());

    expect(result.current.darkMode).toBe(browserSetToDarkMode);

    result.current.setDarkMode(true);

    await waitFor(() => expect(result.current.darkMode).toBe(true));
    expect(localStorage.getItem("prefersDarkColorScheme")).toBe("true");
  });

  it("should add and remove dark class to/from body", async () => {
    const { result } = renderHook(() => useDarkMode());

    act(() => result.current.setDarkMode(true));
    expect(document.body.classList.contains("dark")).toBe(true);

    act(() => result.current.setDarkMode(false));
    expect(document.body.classList.contains("dark")).toBe(false);
  });
});
