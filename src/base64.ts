export const encodeBase64 = (str: string) =>
  Buffer.from(str, "utf8").toString("base64");

export const decodeBase64 = (str: string) =>
  Buffer.from(str, "base64").toString("utf8");
