/**
 * @jest-environment jsdom
 */

import assert from "assert";

import { EMPTY_STATS } from "@/constants";
import { getUpdatedStats } from "./getUpdatedStats";
import { Stats } from "./types";

describe("getUpdatedStats", function () {
  describe("#getUpdatedStats ()", function () {
    it("should return and store blank stats if no previous stats present", function () {
      const puzzleNumber = 23;

      const stats = getUpdatedStats(puzzleNumber, EMPTY_STATS);

      assert.deepEqual(stats, {
        ...EMPTY_STATS,
        firstPlay: puzzleNumber,
        lastPlay: puzzleNumber,
        lastEnd: puzzleNumber - 1,
      });
    });

    it("should return undefined if stored stats are current", function () {
      const storedStats: Stats = {
        firstPlay: 1,
        lastEnd: 3,
        lastPlay: 5,
        winStreak: 7,
        playStreak: 11,
        totalGuesses: 13,
        wins: 17,
        giveups: 23,
        abandons: 29,
        totalPlays: 31,
        hints: 37,
      };
      const puzzleNumber = 5;

      const stats = getUpdatedStats(puzzleNumber, storedStats);

      assert.equal(stats, undefined);
    });

    it.each`
      streakContinued | previouslyAbandoned
      ${false}        | ${false}
      ${false}        | ${true}
      ${true}         | ${false}
      ${true}         | ${true}
    `(
      "should return updated if streak continued ($strakContinued) but previously abandoned ($previouslyAbandoned)",
      ({ streakContinued, previouslyAbandoned }) => {
        const storedStats: Stats = {
          firstPlay: 1,
          lastEnd: previouslyAbandoned ? 3 : 5,
          lastPlay: 5,
          winStreak: 7,
          playStreak: 11,
          totalGuesses: 13,
          wins: 17,
          giveups: 23,
          abandons: 29,
          totalPlays: 31,
          hints: 37,
        };
        const puzzleNumber = storedStats.lastPlay + (streakContinued ? 1 : 2);

        const stats = getUpdatedStats(puzzleNumber, storedStats);

        assert.deepEqual(stats, {
          ...storedStats,
          lastPlay: puzzleNumber,
          playStreak: storedStats.playStreak + (streakContinued ? 1 : 0),
          totalPlays: storedStats.totalPlays + 1,
          abandons: storedStats.abandons + (previouslyAbandoned ? 1 : 0),
        });
      }
    );
  });
});
