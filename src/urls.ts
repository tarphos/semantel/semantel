export const baseUrl = "https://semantel.tarphos.de/api";

export const top1kUrl = (secretBase64: string) =>
  `top1k${isLocalhost() ? ".html" : ""}?w=${secretBase64}`;

function isLocalhost() {
  return window.location.host.indexOf("localhost") === -1;
}
