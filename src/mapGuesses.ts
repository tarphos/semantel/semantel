import { GuessesEntry, GuessesEntryFromStorage } from "./types";

export const mapGuessesToNestedArrayForStorage = (guesses: GuessesEntry[]) =>
  guesses.map(
    (guess) =>
      [
        guess.similarity,
        guess.word,
        guess.percentile,
        guess.index,
      ] as GuessesEntryFromStorage
  );

export const mapNestedArrayFromStorageToGuesses = (
  rawValue: Array<GuessesEntryFromStorage>
) => {
  return rawValue.map(
    (entry: GuessesEntryFromStorage) =>
      ({
        similarity: entry[0],
        word: entry[1],
        percentile: entry[2] === null ? undefined : entry[2],
        index: entry[3],
      } as GuessesEntry)
  );
};
