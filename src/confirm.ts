export async function confirmIfNotGameOver(
  gameOver: boolean,
  message: string,
  action: () => Promise<void>
) {
  if (!gameOver) {
    if (global.confirm(message)) {
      await action();
    }
  }
}
