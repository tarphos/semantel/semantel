/**
 * @jest-environment jsdom
 */

import { usePersistedState } from "@/usePersistedState";
import { act, renderHook } from "@testing-library/react";

describe("usePersistedState", () => {
  afterEach(() => {
    window.localStorage.clear();
  });

  describe("value (0)", () => {
    it("should return the initial value when no value is found in local storage", () => {
      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 4 })
      );

      expect(result.current.value).toEqual(4);
    });

    it("should return the initial value even if other valueexists in local storage", () => {
      window.localStorage.setItem("test", JSON.stringify(7));

      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 13 })
      );

      expect(result.current.value).toEqual(13);
    });
  });

  describe("set and persist value (1)", () => {
    it("should update the value in local storage when it changes", () => {
      window.localStorage.setItem("test", JSON.stringify(4));

      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 13 })
      );

      act(() => {
        result.current.setAndPersistValue(17);
      });

      expect(window.localStorage.getItem("test")).toEqual(JSON.stringify(17));
    });

    it("should map the value when a mapper function is provided", () => {
      window.localStorage.setItem("test", JSON.stringify(7));

      const mapper = (value: number) => value + 1;
      const { result } = renderHook(() =>
        usePersistedState<number, number>({
          storageKey: "test",
          defaultValue: 13,
          mapper: mapper,
        })
      );

      act(() => {
        result.current.setAndPersistValue(17);
      });

      expect(result.current.value).toEqual(17);
      expect(window.localStorage.getItem("test")).toEqual(JSON.stringify(18));
    });
  });

  describe("init from storage (2)", () => {
    it("should return the default value when no value is found in local storage", () => {
      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 4 })
      );

      act(() => {
        result.current.initValueFromStorage();
      });

      expect(result.current.value).toEqual(4);
    });

    it("should return the init default value when no value is found in local storage", () => {
      const { result } = renderHook(() =>
        usePersistedState<number>({
          storageKey: "test",
          defaultValue: 4,
          defaultValueForInit: 23,
        })
      );

      act(() => {
        result.current.initValueFromStorage();
      });

      expect(result.current.value).toEqual(23);
    });

    it("should return the value from local storage", () => {
      window.localStorage.setItem("test", JSON.stringify(7));

      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 13 })
      );

      act(() => {
        result.current.initValueFromStorage();
      });

      expect(result.current.value).toEqual(7);
    });

    it("should return the mapped value from local storage and leave stored value as it is", () => {
      window.localStorage.setItem("test", JSON.stringify(7));

      const { result } = renderHook(() =>
        usePersistedState<number, number>({
          storageKey: "test",
          defaultValue: 13,
          backMapper: (value: number) => value + 1,
        })
      );

      act(() => {
        result.current.initValueFromStorage();
      });

      expect(result.current.value).toEqual(8);
      expect(window.localStorage.getItem("test")).toEqual(JSON.stringify(7));
    });

    it("should return the updated value from local storage and store it there", () => {
      window.localStorage.setItem("test", JSON.stringify(7));

      const { result } = renderHook(() =>
        usePersistedState<number, number>({
          storageKey: "test",
          defaultValue: 13,
          updater: (value: number) => value + 1,
        })
      );

      act(() => {
        result.current.initValueFromStorage();
      });

      expect(result.current.value).toEqual(8);
      expect(window.localStorage.getItem("test")).toEqual(JSON.stringify(8));
    });
  });

  describe("reset value (3)", () => {
    it("should reset the value in local storage", () => {
      window.localStorage.setItem("test", JSON.stringify(4));

      const { result } = renderHook(() =>
        usePersistedState<number>({ storageKey: "test", defaultValue: 13 })
      );

      act(() => {
        result.current.setAndPersistValue(17);
        result.current.resetValue();
      });

      expect(result.current.value).toEqual(13);
      expect(window.localStorage.getItem("test")).toBeNull();
    });
  });
});
