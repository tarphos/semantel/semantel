import { DEFAULT_WIN_STATE } from "@/constants";
import { getModel } from "./data";
import { saveGame } from "./saveGame";
import { DoGuessParams, GuessesEntry, Model2Type } from "./types";
import { endGame, getCosSim } from "./utils";

export async function doGuess(params: DoGuessParams) {
  const newGuesses = [...params.guesses];

  if (notInGuesses(params.guess, params.guesses)) {
    const guessData: Model2Type | undefined = await getGuessData(
      params.guess,
      params.secret,
      params.setError
    );
    if (guessData === undefined) {
      return;
    }

    const percentile = guessData.percentile;

    const guessVec = guessData.vec;

    const similarity = getCosSim(guessVec, params.secretVec) * 100.0;

    const newEntry: GuessesEntry = {
      similarity,
      word: params.guess,
      percentile,
      index: newGuesses.length + 1,
    };
    newGuesses.push(newEntry);

    if (params.winState === DEFAULT_WIN_STATE && !params.is_hint) {
      params.setStats((oldStats) => ({
        ...oldStats,
        totalGuesses: oldStats.totalGuesses + 1,
      }));
    }
  }

  newGuesses.sort(bySimilarity);
  params.setChronoForward(1);

  params.setGuesses(newGuesses);

  if (params.winState === DEFAULT_WIN_STATE) {
    saveGame({
      newGuesses,
      puzzleKey: params.puzzleKey,
      newWinState: -1,
      puzzleNumberFromStorage: params.puzzleNumberFromStorage,
      setWinState: params.setWinState,
      setGuesses: params.setGuesses,
    });
  }

  params.setLatestGuess(params.guess);

  if (
    params.guess.toLowerCase() === params.secret &&
    params.winState === DEFAULT_WIN_STATE
  ) {
    endGame({
      newWinState: 1,
      puzzleNumber: params.puzzleKey,
      guesses: newGuesses,
      setStats: params.setStats,
      puzzleNumberFromStorage: params.puzzleNumberFromStorage,
      winState: params.winState,
      setWinState: params.setWinState,
      setGuesses: params.setGuesses,
      hintsUsed: params.hintsUsed,
      setNumberOfGuessesToEnd: params.setNumberOfGuessesToEnd,
    });
  }
}

const getGuessData = async (
  guess: string,
  secret: string,
  setError: (message: string) => void
) => {
  try {
    const guessData = await getModel(guess, secret);
    if (!guessData) {
      setError(`Ich kenne das Wort ${guess} nicht.`);
    }
    return guessData;
  } catch (e) {
    setError(
      `Konnte Ähnlichkeitsdaten für ${guess} nicht laden. Bitte probiere es später noch einmal.`
    );
  }
};

function notInGuesses(candidate: string, guesses: GuessesEntry[]) {
  return guesses.findIndex((guess) => guess.word === candidate) === -1;
}

function bySimilarity(a: GuessesEntry, b: GuessesEntry): number {
  return b.similarity - a.similarity;
}
