import { optionallyHandleStats } from "./optionallyHandleStats";

import { Stats } from "./types";

const originalStats: Stats = {
  abandons: 1,
  firstPlay: 2,
  giveups: 3,
  hints: 4,
  lastEnd: 5,
  lastPlay: 6,
  playStreak: 7,
  totalGuesses: 8,
  totalPlays: 9,
  wins: 10,
  winStreak: 11,
};

describe("optionallyHandleStats", () => {
  describe("#optionallyHandleStats()", function () {
    it.each`
      won      | continueStreak
      ${true}  | ${true}
      ${true}  | ${false}
      ${false} | ${true}
      ${false} | ${false}
    `(
      "should return updated stats if we're on a streak ($continueStreak), and game was won ($won)",
      ({ won, continueStreak }: { won: boolean; continueStreak: boolean }) => {
        const storeStats = jest.fn();
        const puzzleNumber = continueStreak ? originalStats.lastEnd + 1 : 52;
        const hintsUsed = 2;
        optionallyHandleStats({
          won,
          puzzleNumber,
          hintsUsed,
          setStats: storeStats,
        });

        const expectedStats: Stats = {
          ...originalStats,
          giveups: originalStats.giveups + (won ? 0 : 1),
          hints: originalStats.hints + hintsUsed,
          lastEnd: puzzleNumber,
          winStreak: won ? winStreakIfWon(continueStreak, originalStats) : 0,
          wins: originalStats.wins + (won ? 1 : 0),
        };

        const d: (prevState: Stats) => Stats = storeStats.mock.calls[0][0];
        expect(d(originalStats)).toEqual(expectedStats);
      }
    );
  });
});

function winStreakIfWon(continueStreak: boolean, originalStats: Stats): number {
  return continueStreak ? originalStats.winStreak + 1 : 1;
}
