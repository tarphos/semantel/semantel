import { DEFAULT_GUESSES, EMPTY_STATS } from "@/constants";
import { doGuess } from "./guess";
import {
  DoGuessParams,
  EndGameParams,
  GuessesEntry,
  Model2Type,
} from "./types";

const equal = require("deep-equal");

const dataModule = require("../src/data");
const utilsModule = require("../src/utils");
const saveGameModule = require("../src/saveGame");

const defaultGuess = "world";

const defaultDoGuessParams: DoGuessParams = {
  guess: defaultGuess,
  secret: "hello",
  setError: jest.fn(),
  is_hint: false,
  puzzleKey: 1,
  setChronoForward: () => {},
  setLatestGuess: () => {},
  secretVec: [3, 4, 0],
  guesses: DEFAULT_GUESSES,
  setGuesses: () => {},
  hintsUsed: 0,
  puzzleNumberFromStorage: 1,
  stats: EMPTY_STATS,
  setStats: () => {},
  winState: -1,
  setWinState: () => {},
  setNumberOfGuessesToEnd: () => {},
};

const defaultSimilarity = 48; /*- calculated from secretVec and result of getModel (see beforeEach) -*/

describe("guess", function () {
  describe("#doGuess", function () {
    let getModelStub: jest.SpyInstance;
    beforeEach(function () {
      getModelStub = jest
        .spyOn(dataModule, "getModel")
        .mockReturnValue(Promise.resolve({ vec: [0, 3, 4] } as Model2Type));
    });

    describe.each([false, true])(
      "notification (after won: %p)",
      function (afterWon: boolean) {
        it("should not notify when word unknown for old guess", async function () {
          getModelStub.mockReturnValue(undefined);

          const guesses: Array<GuessesEntry> = [
            { similarity: 1, word: defaultGuess, index: 1 },
          ];

          const params: DoGuessParams = {
            ...defaultDoGuessParams,
            guesses,
            winState: afterWon ? 1 : defaultDoGuessParams.winState,
          };

          await doGuess(params);

          expect(params.setError).not.toHaveBeenCalled();
        });

        it("should notify when word unknown for new guess", async function () {
          getModelStub.mockReturnValue(undefined);

          const guesses: Array<GuessesEntry> = [];

          const params: DoGuessParams = {
            ...defaultDoGuessParams,
            guesses,
            winState: afterWon ? 1 : defaultDoGuessParams.winState,
          };

          await doGuess(params);

          expect(params.setError).toHaveBeenCalledWith(
            `Ich kenne das Wort ${params.guess} nicht.`
          );
        });

        it("should notify when error occurs while fetching model data", async function () {
          getModelStub.mockRejectedValue(new Error("some error"));

          const guesses: Array<GuessesEntry> = [];

          const params: DoGuessParams = {
            ...defaultDoGuessParams,
            guesses,
          };

          await doGuess(params);

          expect(params.setError).toHaveBeenCalledWith(
            `Konnte Ähnlichkeitsdaten für ${params.guess} nicht laden. Bitte probiere es später noch einmal.`
          );
        });
      }
    );

    describe.each([false, true])(
      "calling API (after won: %p)",
      function (afterWon: boolean) {
        it("should not call API again for old guess", async function () {
          const guesses: Array<GuessesEntry> = [
            { similarity: 1, word: defaultGuess, index: 1 },
          ];

          const params: DoGuessParams = {
            ...defaultDoGuessParams,
            guesses,
            winState: afterWon ? 1 : defaultDoGuessParams.winState,
          };

          await doGuess(params);

          expect(getModelStub).not.toHaveBeenCalled();
          expect(params.setError).not.toHaveBeenCalled();
        });

        it("should call API again for new guess", async function () {
          const guesses: Array<GuessesEntry> = [];

          const params: DoGuessParams = {
            ...defaultDoGuessParams,
            guesses,
            winState: afterWon ? 1 : defaultDoGuessParams.winState,
          };

          await doGuess(params);

          expect(getModelStub).toHaveBeenCalledWith(
            params.guess,
            params.secret
          );

          expect(params.setError).not.toHaveBeenCalled();
        });
      }
    );

    describe("reset sorting", function () {
      // TODO add tests for guess after won
      it("should reset sorting for old guess", async function () {
        const guesses: Array<GuessesEntry> = [
          { similarity: 3, word: defaultGuess, index: 3 },
          { similarity: 1, word: "foo", index: 2 },
          { similarity: 2, word: "bar", index: 1 },
        ];

        const setGuesses = jest.fn();

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
          setGuesses,
        };

        await doGuess(params);

        const guessesX: Array<GuessesEntry> = setGuesses.mock.calls[0][0];

        expect(hasSimilarities(guessesX, [3, 2, 1])).toBeTruthy();
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should reset sorting for new guess", async function () {
        const guesses: Array<GuessesEntry> = [
          { similarity: 3, word: "bla", index: 3 },
          { similarity: 1, word: "foo", index: 2 },
          { similarity: 2, word: "bar", index: 1 },
        ];

        const setGuesses = jest.fn();

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
          setGuesses,
        };

        await doGuess(params);

        const guessesX: Array<GuessesEntry> = setGuesses.mock.calls[0][0];

        expect(
          hasSimilarities(guessesX, [defaultSimilarity, 3, 2, 1])
        ).toBeTruthy();
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should not update guesses when error occurs while fetching model data", async function () {
        getModelStub.mockRejectedValue(new Error("some error"));

        const setGuesses = jest.fn();

        const guesses: Array<GuessesEntry> = [];

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          setGuesses,
          guesses,
        };

        await doGuess(params);

        expect(setGuesses).not.toHaveBeenCalled();
        expect(params.setError).toHaveBeenCalledWith(
          `Konnte Ähnlichkeitsdaten für ${params.guess} nicht laden. Bitte probiere es später noch einmal.`
        );
      });
    });

    describe("save game", function () {
      // TODO add tests for guess after won
      it("should not save game if game over for old guess", async function () {
        const guesses: Array<GuessesEntry> = [
          { similarity: 1, word: defaultGuess, index: 1 },
        ];
        const saveGameStub = jest.spyOn(saveGameModule, "saveGame");

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
          winState: 1,
          setWinState: () => {},
        };

        await doGuess(params);

        expect(saveGameStub).not.toHaveBeenCalled();
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should not save game if game over for new guess ", async function () {
        const guesses: Array<GuessesEntry> = [];
        const saveGameStub = jest.spyOn(saveGameModule, "saveGame");

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
          winState: 1,
          setWinState: () => {},
        };

        await doGuess(params);

        expect(saveGameStub).not.toHaveBeenCalled();
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should save game if not game over for old guess", async function () {
        const guesses: Array<GuessesEntry> = [
          { similarity: 1, word: defaultGuess, index: 1 },
        ];
        const saveGameStub = jest.spyOn(saveGameModule, "saveGame");

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
        };

        await doGuess(params);

        expect(saveGameStub).toHaveBeenCalledWith(
          expect.objectContaining({
            newGuesses: params.guesses,
            puzzleKey: params.puzzleKey,
            newWinState: params.winState,
            puzzleNumberFromStorage: params.puzzleNumberFromStorage,
            setWinState: params.setWinState,
            setGuesses: params.setGuesses,
          })
        );
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should save game if not game over for new guess", async function () {
        const guesses: Array<GuessesEntry> = [];
        const saveGameStub = jest.spyOn(saveGameModule, "saveGame");

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guesses,
        };

        await doGuess(params);

        expect(saveGameStub).toHaveBeenCalledWith(
          expect.objectContaining({
            newGuesses: [
              expect.objectContaining({ word: params.guess, index: 1 }),
            ],
            puzzleKey: params.puzzleKey,
            newWinState: params.winState,
            puzzleNumberFromStorage: params.puzzleNumberFromStorage,
            setWinState: params.setWinState,
            setGuesses: params.setGuesses,
          })
        );
        expect(params.setError).not.toHaveBeenCalled();
      });
    });

    it.each`
      winState | guessCorrect
      ${-1}    | ${false}
      ${-1}    | ${true}
      ${1}     | ${false}
      ${1}     | ${true}
    `(
      "should only end game if guess correct ($guessCorrect) and game not already over (game over: $winState > -1)",
      async ({ winState, guessCorrect }) => {
        const gameOver = winState > -1;
        const shouldEndGame = guessCorrect && !gameOver;

        const guess = guessCorrect ? defaultDoGuessParams.secret : "bla";

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          guess,
          winState,
          setWinState: () => {},
        };

        const endGameStub = jest.spyOn(utilsModule, "endGame");

        await doGuess(params);

        if (shouldEndGame) {
          const endGameParams: Omit<
            EndGameParams,
            | "winState"
            | "setWinState"
            | "hintsUsed"
            | "setHintsUsed"
            | "setGuesses"
            | "puzzleNumberFromStorage"
            | "stats"
            | "setStats"
            | "setNumberOfGuessesToEnd"
          > = {
            puzzleNumber: params.puzzleKey,
            guesses: [{ similarity: defaultSimilarity, word: guess, index: 1 }],
          };
          expect(endGameStub).toHaveBeenCalledWith(
            expect.objectContaining(endGameParams)
          );
        } else {
          expect(endGameStub).not.toHaveBeenCalled();
        }
        expect(params.setError).not.toHaveBeenCalled();
      }
    );

    describe("stats", function () {
      it("should update stats", async () => {
        const setStats = jest.fn();

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          setStats,
        };

        await doGuess(params);

        expect(setStats.mock.calls[0][0](EMPTY_STATS)).toStrictEqual({
          ...EMPTY_STATS,
          totalGuesses: EMPTY_STATS.totalGuesses + 1,
        });
        expect(params.setError).not.toHaveBeenCalled();
      });

      it("should not update stats if already won", async () => {
        const setStats = jest.fn();

        const params: DoGuessParams = {
          ...defaultDoGuessParams,
          setStats,
          winState: 1,
        };

        await doGuess(params);

        expect(setStats).not.toHaveBeenCalled();
        expect(params.setError).not.toHaveBeenCalled();
      });
    });

    describe("error case", function () {
      it("should report error if guess unknown", async function () {
        getModelStub.mockReturnValue(Promise.resolve(undefined));

        const params = defaultDoGuessParams;

        await doGuess(params);

        expect(params.setError).toHaveBeenCalledWith(
          `Ich kenne das Wort ${defaultGuess} nicht.`
        );
      });
    });
  });
});

function hasSimilarities(
  guesses: Array<GuessesEntry>,
  expectedArray: number[]
): boolean {
  return equal(
    guesses.map((guess) => guess.similarity),
    expectedArray
  );
}
