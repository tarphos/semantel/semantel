import { DEFAULT_GUESSES, DEFAULT_WIN_STATE } from "@/constants";
import { Dispatch, SetStateAction } from "react";
import { optionallyHandleStats } from "./optionallyHandleStats";
import { saveGame } from "./saveGame";
import {
  EndGameParams,
  GuessesEntry,
  GuessesEntryWithPercentile,
} from "./types";

function mag(a: Array<number>) {
  return Math.sqrt(
    a.reduce(function (sum, val) {
      return sum + val * val;
    }, 0)
  );
}

function dot(f1: Array<number>, f2: Array<number>) {
  return f1.reduce(function (sum, a, idx) {
    return sum + a * f2[idx];
  }, 0);
}

export function getCosSim(f1: Array<number>, f2: Array<number>) {
  return dot(f1, f2) / (mag(f1) * mag(f2));
}

function highest_unguessed(guessesLocal: Array<GuessesEntryWithPercentile>) {
  for (let i = 1; i < guessesLocal.length; i++) {
    if (guessesLocal[i].percentile !== 999 - i) {
      return 999 - i;
    }
  }
  if (guessesLocal.length === 999) {
    // user has guessed all of the top 1k except the actual word.
    return -1;
  } else {
    return 999 - guessesLocal.length;
  }
}

export function hintNumber(guessesLocal: Array<GuessesEntry>) {
  // Stryker disable next-line ConditionalExpression,BlockStatement : Only an optimization
  if (guessesLocal.length === 0) {
    return 1;
  }
  const top1k_guesses = guessesLocal.filter(
    (guessLocal) => guessLocal.percentile
  ) as Array<GuessesEntryWithPercentile>;

  if (top1k_guesses.length === 0) {
    return 1;
  }

  const nearest_top1k = top1k_guesses[0].percentile;
  if (nearest_top1k === 999) {
    return highest_unguessed(top1k_guesses);
  }

  let guess = Math.floor((nearest_top1k * 3 + 1000) / 4);
  if (guess === nearest_top1k) {
    guess += 1;
  }
  return guess;
}

export function clearStorage(
  puzzleKey: number,
  puzzleNumberFromStorage: number,
  setPuzzleNumber: Dispatch<SetStateAction<number>>,
  setWinState: Dispatch<SetStateAction<number>>,
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>
) {
  if (puzzleNumberFromStorage !== puzzleKey) {
    setGuesses(DEFAULT_GUESSES);
    setWinState(DEFAULT_WIN_STATE);
    setPuzzleNumber(puzzleKey);
  }
}

export function getToday(now: number) {
  const today = Math.floor(now / 86400000);
  return today;
}

export function endGame(params: EndGameParams) {
  if (params.newWinState !== undefined) {
    params.setWinState(params.newWinState);
  }

  optionallyHandleStats({
    puzzleNumber: params.puzzleNumber,
    won: (params.newWinState ?? params.winState) > DEFAULT_WIN_STATE,
    setStats: params.setStats,
    hintsUsed: params.hintsUsed,
  });

  saveGame({
    puzzleKey: params.puzzleNumber,
    puzzleNumberFromStorage: params.puzzleNumberFromStorage,
    setWinState: params.setWinState,
    setGuesses: params.setGuesses,
  });

  params.setNumberOfGuessesToEnd(params.guesses.length);
}
