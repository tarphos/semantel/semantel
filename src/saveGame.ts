import { SaveGameParams } from "./types";

export function saveGame({ puzzleKey, ...params }: SaveGameParams) {
  if (params.puzzleNumberFromStorage !== puzzleKey) {
    // If we are in a tab still open from yesterday, we're done here.
    // Don't save anything because we may overwrite today's game!
    return;
  }

  saveGameInternal(params);
}

function saveGameInternal(params: Omit<SaveGameParams, "puzzleKey">) {
  if (params.newWinState !== undefined) {
    params.setWinState(params.newWinState);
  }
  if (params.newGuesses !== undefined) {
    params.setGuesses(params.newGuesses);
  }
}
