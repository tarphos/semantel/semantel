import assert from "assert";
import {
  clearStorage,
  endGame,
  getCosSim,
  getToday,
  hintNumber,
} from "./utils";
const optionallyHandleStatsModule = require("../src/optionallyHandleStats");
const saveGameModule = require("../src/saveGame");

import { DEFAULT_GUESSES, DEFAULT_WIN_STATE } from "@/constants";
import {
  EndGameParams,
  GuessesEntry,
  OptionallyHandleStatsParams,
} from "./types";

describe("utils", function () {
  describe("#getCosSim()", function () {
    it("should compute correct cosine similarity", function () {
      const cosSim2 = getCosSim([9, 3], [3, 9]);
      assert.equal(cosSim2, 0.6);
    });
  });

  describe("#hintNumber()", function () {
    it("should return 1 when no guesses", async function () {
      const guesses: Array<GuessesEntry> = [];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 1);
    });

    it("should return 1 when no guesses in top1k", async function () {
      const guesses: Array<GuessesEntry> = [
        { similarity: -3.3428645860748714, word: "kurz", index: 1 },
      ];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 1);
    });

    it("should return next lowest unguessed when at 999", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 999,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 998,
          index: 8,
        },
        {
          similarity: 34.29144630553826,
          word: "fliehkraft",
          percentile: 997,
          index: 6,
        },
        {
          similarity: 28.931756034760536,
          word: "aufgleiten",
          percentile: 60,
          index: 5,
        },
        {
          similarity: 28.898201717589245,
          word: "hydrant",
          percentile: 53,
          index: 4,
        },
        { similarity: 27.206721044929523, word: "ballon", index: 3 },
        { similarity: 9.304509871190934, word: "lok", index: 2 },
        { similarity: -3.3428645860748714, word: "kurz", index: 1 },
      ];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 996);
    });

    it("should return next lowest unguessed when at 999 and only good guesses", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 999,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 998,
          index: 8,
        },
        {
          similarity: 34.29144630553826,
          word: "fliehkraft",
          percentile: 997,
          index: 6,
        },
      ];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 996);
    });

    it("should return -1 when all top1k words have been guessed", async function () {
      const guesses: Array<GuessesEntry> = [...Array(999).keys()].map((i) => ({
        similarity: 40 - i / 500,
        word: "" + i,
        percentile: 999 - i,
        index: 999 - i,
      }));

      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, -1);
    });

    it("should return hint somewhere between best guess and solution if there is a gap", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 400,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 300,
          index: 8,
        },
        {
          similarity: 34.29144630553826,
          word: "fliehkraft",
          percentile: 200,
          index: 6,
        },
      ];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 550);
    });

    it("should return hint somewhere between best guess and solution if there is a small gap", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 997,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 300,
          index: 8,
        },
        {
          similarity: 34.29144630553826,
          word: "fliehkraft",
          percentile: 200,
          index: 6,
        },
      ];
      const hintNumber2 = hintNumber(guesses);
      assert.equal(hintNumber2, 998);
    });
  });

  describe("#clearStorage()", function () {
    it("should clear storage if puzzle numbers don't match", function () {
      const newPuzzleNumber = 42;

      const setPuzzleNumber = jest.fn();
      const setWinState = jest.fn();
      const setGuesses = jest.fn();
      clearStorage(
        newPuzzleNumber,
        23,
        setPuzzleNumber,
        setWinState,
        setGuesses
      );

      expect(setPuzzleNumber).toHaveBeenCalledWith(newPuzzleNumber);

      expect(setGuesses).toHaveBeenCalledWith(DEFAULT_GUESSES);

      expect(setWinState).toHaveBeenCalledWith(DEFAULT_WIN_STATE);
    });

    it("should not clear storage if puzzle numbers match", function () {
      const setPuzzleNumber = jest.fn();
      const setWinState = jest.fn();
      const setGuesses = jest.fn();

      clearStorage(23, 23, setPuzzleNumber, setWinState, setGuesses);

      expect(setPuzzleNumber).not.toHaveBeenCalled();
      expect(setGuesses).not.toHaveBeenCalled();
      expect(setWinState).not.toHaveBeenCalled();
    });

    it("should clear storage if puzzle number isn't present", function () {
      const newPuzzleNumber = 42;

      const setPuzzleNumber = jest.fn();
      const setWinState = jest.fn();
      const setGuesses = jest.fn();
      clearStorage(
        newPuzzleNumber,
        0,
        setPuzzleNumber,
        setWinState,
        setGuesses
      );

      expect(setPuzzleNumber).toHaveBeenCalledWith(newPuzzleNumber);

      expect(setGuesses).toHaveBeenCalledWith(DEFAULT_GUESSES);

      expect(setWinState).toHaveBeenCalledWith(DEFAULT_WIN_STATE);
    });
  });

  describe("#getToday()", function () {
    it("should get correct day (1)", function () {
      const today = getToday(Date.parse("2022-04-27"));
      assert.equal(today, 19109);
    });

    it("should get correct day (2)", function () {
      const today = getToday(Date.parse("2021-12-10"));
      assert.equal(today, 18971);
    });
  });

  describe("#endGame()", function () {
    beforeEach(() => {
      jest.spyOn(optionallyHandleStatsModule, "optionallyHandleStats");
      jest.spyOn(saveGameModule, "saveGame");
    });

    const puzzleNumbers = [1, -1];
    const hintsUseds = [0, 1];
    const winStates = [-1, 0, 1];

    puzzleNumbers.forEach((puzzleNumber) =>
      hintsUseds.forEach((hintsUsed) =>
        winStates.forEach((winState) => {
          const paramsString = JSON.stringify({
            puzzleNumber,
            hintsUsed,
            winState,
          });
          const guesses: GuessesEntry[] = [
            { similarity: -3.3428645860748714, word: "kurz", index: 1 },
          ];
          const params: EndGameParams = {
            puzzleNumber,
            guesses,
            hintsUsed: hintsUsed,
            winState: winState,
            setWinState: jest.fn(),
            setGuesses: jest.fn(),
            puzzleNumberFromStorage: 1,
            setStats: () => {},
            newWinState: winState,
            setNumberOfGuessesToEnd: jest.fn(),
          };

          it(`should set winState (${paramsString})`, async function () {
            endGame(params);

            expect(params.setWinState).toHaveBeenCalledWith(winState);
          });

          it(`should call optionallyHandleStats (${paramsString})`, async function () {
            endGame(params);

            const optionallyHandleStatsParams: Omit<
              OptionallyHandleStatsParams,
              "hintsUsed" | "setStats"
            > = {
              puzzleNumber: params.puzzleNumber,
              won: winState > -1,
            };

            expect(
              optionallyHandleStatsModule.optionallyHandleStats
            ).toHaveBeenCalledWith(
              expect.objectContaining(optionallyHandleStatsParams)
            );
          });

          it(`should call optionallyHandleStats with newWinState value (${paramsString})`, async function () {
            endGame({ ...params, newWinState: 1 });

            const optionallyHandleStatsParams: Omit<
              OptionallyHandleStatsParams,
              "hintsUsed" | "setStats"
            > = {
              puzzleNumber: params.puzzleNumber,
              won: true,
            };

            expect(
              optionallyHandleStatsModule.optionallyHandleStats
            ).toHaveBeenCalledWith(
              expect.objectContaining(optionallyHandleStatsParams)
            );
          });

          it("should save game", async function () {
            endGame(params);

            expect(saveGameModule.saveGame).toHaveBeenCalled();
          });

          it(`should set number of guesses to end (${paramsString})`, async function () {
            endGame(params);

            expect(params.setNumberOfGuessesToEnd).toHaveBeenCalledWith(
              guesses.length
            );
          });
        })
      )
    );
  });
});
