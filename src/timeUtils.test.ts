import { getLocalTimeString } from "@/timeUtils";

describe("getLocalTimeString", () => {
  it("should return the current string", () => {
    const timezoneHours = 4;
    const FakeDate = require("timezoned-date").makeConstructor(
      timezoneHours * 60
    );
    global.Date = FakeDate;
    const localTimeString = getLocalTimeString();
    expect(localTimeString).toBe(`oder ${timezoneHours}:00 in deiner Zeit`);
  });
});
