import {
  mapGuessesToNestedArrayForStorage,
  mapNestedArrayFromStorageToGuesses,
} from "@/mapGuesses";
import { GuessesEntryFromStorage } from "@/types";

describe("mapGuesses", function () {
  describe("#mapGuessesToNestedArrayForStorage", function () {
    it("should map to nested array correctly", () => {
      expect(
        mapGuessesToNestedArrayForStorage([
          { similarity: 1, word: "foo", index: 1 },
          { similarity: 2, word: "bar", index: 2 },
          { similarity: 3, word: "baz", index: 3, percentile: 0.5 },
        ])
      ).toEqual([
        [1, "foo", undefined, 1],
        [2, "bar", undefined, 2],
        [3, "baz", 0.5, 3],
      ]);
    });

    it("should map to nested array and back correctly", () => {
      const guesses = [
        { similarity: 1, word: "foo", index: 1 },
        { similarity: 2, word: "bar", index: 2 },
        { similarity: 3, word: "baz", index: 3, percentile: 0.5 },
      ];

      expect(
        mapNestedArrayFromStorageToGuesses(
          mapGuessesToNestedArrayForStorage(guesses)
        )
      ).toEqual(guesses);
    });
  });

  describe("#mapNestedArrayFromStorageToGuesses", function () {
    it("should map to guesses correctly", () => {
      expect(
        mapNestedArrayFromStorageToGuesses([
          [1, "foo", undefined, 1],
          [2, "bar", undefined, 2],
          [3, "baz", 0.5, 3],
        ])
      ).toEqual([
        { similarity: 1, word: "foo", index: 1 },
        { similarity: 2, word: "bar", index: 2 },
        { similarity: 3, word: "baz", index: 3, percentile: 0.5 },
      ]);
    });

    it("should map from nested array and back correctly", () => {
      const nestedArray = [
        [1, "foo", undefined, 1],
        [2, "bar", undefined, 2],
        [3, "baz", 0.5, 3],
      ] as Array<GuessesEntryFromStorage>;

      expect(
        mapGuessesToNestedArrayForStorage(
          mapNestedArrayFromStorageToGuesses(nestedArray)
        )
      ).toEqual(nestedArray);
    });
  });
});
