import { useCallback, useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";
import { usePersistedState } from "./usePersistedState";
import { storageKey_prefersDarkColorScheme } from "./constants/localStorageKeys";

export default function useDarkMode(): {
  darkMode: boolean;
  setDarkMode: (darkMode?: boolean) => void;
  storedDarkMode: boolean | undefined;
} {
  // Stryker disable next-line BooleanLiteral : This immediately gets overwritten. Couldn't find a way to test this. // TODO test this
  const [darkMode, setDarkMode] = useState(false);

  const isBrowserDarkMode = useMediaQuery({
    query: "(prefers-color-scheme: dark)",
  });

  const {
    value: prefersDarkColorScheme,
    setAndPersistValue: setAndPersistPrefersDarkColorScheme,
    initValueFromStorage: initPrefersDarkColorSchemeFromStorage,
  } = usePersistedState<boolean | undefined>({
    storageKey: storageKey_prefersDarkColorScheme,
    defaultValue: undefined,
  });

  useEffect(
    () => {
      // Stryker disable ConditionalExpression : We're not testing if window is defined
      /* istanbul ignore else */
      if (typeof window !== "undefined") {
        initPrefersDarkColorSchemeFromStorage();
      }
    },
    // Stryker disable next-line ArrayDeclaration
    [initPrefersDarkColorSchemeFromStorage]
  );
  // Stryker restore ConditionalExpression

  useEffect(() => {
    setDarkMode(prefersDarkColorScheme ?? isBrowserDarkMode);
  }, [isBrowserDarkMode, prefersDarkColorScheme]);

  useEffect(
    () => document.body.classList[darkMode ? "add" : "remove"]("dark"),
    // Stryker disable next-line ArrayDeclaration
    [darkMode]
  );

  const setAndPersistDarkMode = useCallback(
    (darkMode?: boolean) => {
      setDarkMode(darkMode ?? isBrowserDarkMode);
      setAndPersistPrefersDarkColorScheme(darkMode);
    },
    // Stryker disable next-line ArrayDeclaration
    [isBrowserDarkMode, setAndPersistPrefersDarkColorScheme]
  );

  return {
    darkMode,
    setDarkMode: setAndPersistDarkMode,
    storedDarkMode: prefersDarkColorScheme,
  };
}
