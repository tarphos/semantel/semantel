import { Dispatch, SetStateAction } from "react";

export type Stats = {
  firstPlay: number;
  lastEnd: number;
  lastPlay: number;
  winStreak: number;
  playStreak: number;
  totalGuesses: number;
  wins: number;
  giveups: number;
  abandons: number;
  totalPlays: number;
  hints: number;
};

export type GuessesEntryFromStorage = [
  number,
  string,
  number | undefined | null,
  number
];

export type GuessesEntry = {
  similarity: number;
  word: string;
  percentile?: number;
  index: number;
};
export type GuessesEntryWithPercentile = Required<GuessesEntry>;

export type Model2Type = {
  vec: Array<number>;
  percentile?: number;
};

export type SimilarityStoryType = { top: number; top10: number; rest: number };

export type SpecificDoGuessParams = {
  guess: string;
  is_hint: boolean;
  stats: Stats;
  setStats: Dispatch<SetStateAction<Stats>>;
  puzzleNumberFromStorage: number;
};

export type CommonDoGuessAndHintParams = {
  secret: string;
  puzzleKey: number;
  setError: (message: string) => void;
  setChronoForward: Dispatch<number>;
  setLatestGuess: Dispatch<string>;
  secretVec: Array<number>;
  winState: number;
  setWinState: Dispatch<SetStateAction<number>>;
  guesses: Array<GuessesEntry>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
  setNumberOfGuessesToEnd: Dispatch<SetStateAction<number>>;
  hintsUsed: number;
};

export type DoGuessParams = SpecificDoGuessParams & CommonDoGuessAndHintParams;

export type HintParams = {
  alert: (message?: any) => void;
  secret: string;
  puzzleKey: number;
  setError: (message: string) => void;
  setChronoForward: Dispatch<number>;
  setLatestGuess: Dispatch<string>;
  secretVec: Array<number>;
  stats: Stats;
  setStats: Dispatch<SetStateAction<Stats>>;
  puzzleNumberFromStorage: number;
  winState: number;
  setWinState: Dispatch<SetStateAction<number>>;
  guesses: Array<GuessesEntry>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
  hintsUsed: number;
  setHintsUsed: Dispatch<SetStateAction<number>>;
  setNumberOfGuessesToEnd: Dispatch<SetStateAction<number>>;
};

export type EndGameParams = {
  puzzleNumber: number;
  newWinState?: number;
  setStats: Dispatch<SetStateAction<Stats>>;
  puzzleNumberFromStorage: number;
  winState: number;
  setWinState: Dispatch<SetStateAction<number>>;
  guesses: Array<GuessesEntry>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
  hintsUsed: number;
  setNumberOfGuessesToEnd: Dispatch<SetStateAction<number>>;
};

export type OptionallyHandleStatsParams = {
  won: boolean;
  puzzleNumber: number;
  setStats: Dispatch<SetStateAction<Stats>>;
  hintsUsed: number;
};

export type SaveGameParams = {
  newGuesses?: Array<GuessesEntry>;
  newWinState?: number;
  puzzleKey: number;
  puzzleNumberFromStorage: number;
  setWinState: Dispatch<SetStateAction<number>>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
};

export type IValueStorage<S, T extends S | undefined> = {
  value: T;
  setValue: (value: SetStateAction<S>, persist: boolean) => void;
  clearValue: () => void;
};
export type SafeValueStorage<T> = IValueStorage<T, T>;
export type ValueStorage<T> = IValueStorage<T, T | undefined>;

export type Top1kEntry = {
  neighbor: string;
  percentile: number;
  similarity: number;
};
