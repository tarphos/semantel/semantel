"use client";

import { EMPTY_STATS } from "./constants";
import { Stats } from "./types";

const equal = require("deep-equal");

export function getUpdatedStats(
  puzzleNumber: number,
  stats: Stats
): Stats | undefined {
  if (equal(stats, EMPTY_STATS)) {
    const stats: Stats = {
      firstPlay: puzzleNumber,
      lastEnd: puzzleNumber - 1,
      lastPlay: puzzleNumber,
      winStreak: 0,
      playStreak: 0,
      totalGuesses: 0,
      wins: 0,
      giveups: 0,
      abandons: 0,
      totalPlays: 0,
      hints: 0,
    };
    return stats;
  } else {
    const newStats = { ...stats };
    if (newStats.lastPlay !== puzzleNumber) {
      const onStreak = newStats.lastPlay === puzzleNumber - 1;
      if (onStreak) {
        newStats.playStreak += 1;
      }
      newStats.totalPlays += 1;
      if (newStats.lastEnd !== newStats.lastPlay) {
        newStats.abandons += 1;
      }
      newStats.lastPlay = puzzleNumber;
      return newStats;
    } // else return undefined
  }
}
