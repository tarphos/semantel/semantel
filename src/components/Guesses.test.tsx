/**
 * @jest-environment jsdom
 */

import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import assert from "assert";
import { useState } from "react";
import Guesses from "@/components/Guesses";
import { GuessesEntry, SimilarityStoryType } from "@/types";

describe("Guesses", function () {
  afterEach(() => {
    window.localStorage.clear();
  });

  it("should sort by guess number on click", async () => {
    render(<GuessesContainer />);

    await act(() => userEvent.click(screen.getByText("#")));

    assert.deepEqual(
      screen
        .getAllByTestId("guessRow")
        .map((row) => row.firstChild?.textContent),
      [1, 2, 3]
    );

    await act(() => userEvent.click(screen.getByText("#")));

    assert.deepEqual(
      screen
        .getAllByTestId("guessRow")
        .map((row) => row.firstChild?.textContent),
      [3, 2, 1]
    );
  });

  it("should sort alphabetically on click", async () => {
    render(<GuessesContainer />);

    await act(() => userEvent.click(screen.getByText("Tipp")));

    assert.deepEqual(
      screen
        .getAllByTestId("guessRow")
        .map((row) => row.firstChild?.textContent),
      [2, 3, 1]
    );
  });

  it("should sort by similarity on click", async () => {
    render(<GuessesContainer />);

    await act(() => userEvent.click(screen.getByText("Ähnlichkeit")));

    assert.deepEqual(
      screen
        .getAllByTestId("guessRow")
        .map((row) => row.firstChild?.textContent),
      [3, 1, 2]
    );
  });

  it("should show latest guess first", async () => {
    render(<GuessesContainer latestGuess="b" />);

    await act(() => userEvent.click(screen.getByText("Ähnlichkeit")));

    assert.deepEqual(
      screen
        .getAllByTestId("guessRow")
        .map((row) => row.firstChild?.textContent),
      [3, 1, 2]
    );
  });
});

const GuessesContainer = (props: { latestGuess?: string }) => {
  const [chronoForward, setChronoForward] = useState(1);
  const [guesses, setGuesses] = useState<Array<GuessesEntry>>([
    { similarity: 100, word: "a", index: 2 },
    { similarity: 200, word: "c", index: 1 },
    { similarity: 300, word: "b", index: 3 },
  ]);

  return (
    <Guesses
      chrono_forward={chronoForward}
      setChrono_forward={setChronoForward}
      guesses={guesses}
      setGuesses={setGuesses}
      darkMode={false}
      latestGuess={props.latestGuess}
      similarityStory={{} as unknown as SimilarityStoryType}
    />
  );
};
