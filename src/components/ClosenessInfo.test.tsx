/**
 * @jest-environment jsdom
 */

import { render, screen } from "@testing-library/react";
import assert from "assert";
import React from "react";
import ClosenessInfo from "@/components/ClosenessInfo";
import "@testing-library/jest-dom";

describe("ClosenessInfo", function () {
  it("should display found message", () => {
    renderInsideTable(
      <ClosenessInfo
        percentile={1000}
        similarity={2}
        similarityStory={{ rest: 3, top: 4, top10: 5 }}
      />
    );

    assert.ok(screen.queryByText("GEFUNDEN!") !== null);
  });

  it("should display closeness", () => {
    renderInsideTable(
      <ClosenessInfo
        percentile={300}
        similarity={2}
        similarityStory={{ rest: 3, top: 4, top10: 5 }}
      />
    );

    assert.ok(screen.queryByText("300/1000") !== null);
  });

  it.each([[300], [400]])(
    "should display weirdness info (similarity %i)",
    (similarity) => {
      renderInsideTable(
        <ClosenessInfo
          percentile={undefined}
          similarity={similarity /* at least rest*100 */}
          similarityStory={{ rest: 3, top: 4, top10: 5 }}
        />
      );

      assert.ok(screen.queryByText("????") !== null);
    }
  );

  it("should display cold info", () => {
    renderInsideTable(
      <ClosenessInfo
        percentile={undefined}
        similarity={100 /* less than rest*100 */}
        similarityStory={{ rest: 3, top: 4, top10: 5 }}
      />
    );

    assert.ok(screen.queryByText("(kalt)") !== null);
  });

  it("should display progress bar", () => {
    renderInsideTable(
      <ClosenessInfo
        percentile={300}
        similarity={2}
        similarityStory={{ rest: 3, top: 4, top10: 5 }}
      />
    );

    expect(screen.getByTestId("progressbar")).toHaveStyle({ width: "30%" });
  });
});

const renderInsideTable = (component: JSX.Element) => {
  render(
    <table>
      <tbody>
        <tr>{component}</tr>
      </tbody>
    </table>
  );
};
