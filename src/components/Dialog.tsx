import React, {
  KeyboardEventHandler,
  memo,
  useCallback,
  useEffect,
  useRef,
} from "react";
import CornerActionButton from "./CornerActionButton";

export type DialogProps = {
  underlayId: string;
  ariaLabelledby: string;
  closeButtonId: string;
  close: () => void;
  children: React.ReactNode;
};

const Dialog = memo(function Dialog(props: Readonly<DialogProps>) {
  const dialogRef = useRef<HTMLDivElement>(null);

  useEffect(
    () => {
      document.body.classList.add("dialog-open");
      dialogRef.current?.focus();
      return () => {
        document.body.classList.remove("dialog-open");
      };
    },
    // Stryker disable next-line ArrayDeclaration
    []
  );

  const handleKeyDown: KeyboardEventHandler = useCallback(
    (event) => {
      if (event.key === "Escape") {
        props.close();
      }
    },
    // Stryker disable next-line ArrayDeclaration
    [props]
  );

  return (
    <div
      // Stryker disable next-line StringLiteral
      className={"dialog-underlay"}
      id={props.underlayId}
      onClick={props.close}
      onKeyDown={handleKeyDown}
      tabIndex={0}
    >
      <div
        className="dialog"
        aria-labelledby={props.ariaLabelledby}
        aria-modal="true"
        role="dialog"
        onClick={(event) => event.stopPropagation()}
        onKeyDown={handleKeyDown}
        ref={dialogRef}
        // Stryker disable next-line UnaryOperator
        tabIndex={-1}
      >
        <CornerActionButton
          close={props.close}
          id={props.closeButtonId}
          type="close"
        />

        <div className="dialog-content">{props.children}</div>
      </div>
    </div>
  );
});

export default Dialog;
