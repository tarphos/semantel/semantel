/**
 * @jest-environment jsdom
 */

import RulesDialog from "@/components/RulesDialog";
import { readRules } from "@/constants/localStorageKeys";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { useState } from "react";

const yesterday = "baum";

describe("RulesDialog", function () {
  afterEach(() => {
    localStorage.clear();
  });

  it("should store that rules have been read if dialog is open", () => {
    render(<RulesDialogContainer initiallyOpen={true} />);

    expect(localStorage.getItem(readRules)).toBe("true");
  });

  it("should not store that rules have been read if dialog is closed", () => {
    render(<RulesDialogContainer initiallyOpen={false} />);

    expect(localStorage.getItem(readRules)).toBeNull();
  });

  it("should not display anything if dialog is closed", () => {
    render(<RulesDialogContainer initiallyOpen={false} />);

    expect(screen.queryByRole("dialog")).not.toBeInTheDocument();
  });

  it("should display dialog if marled as open", () => {
    render(<RulesDialogContainer initiallyOpen={true} />);

    expect(screen.getByRole("dialog")).toBeVisible();
    expect(screen.getByText("Errate das Lösungswort")).toBeVisible();
  });

  it("should display yesterday's word", () => {
    render(<RulesDialogContainer initiallyOpen={true} />);

    expect(screen.getByRole("dialog")).toHaveTextContent(
      `Das Wort von gestern war „${yesterday}“.`
    );
  });
});

const RulesDialogContainer = (props: { initiallyOpen: boolean }) => {
  const [open, setOpen] = useState(props.initiallyOpen);

  return (
    <RulesDialog
      close={() => setOpen(false)}
      open={open}
      yesterday={yesterday}
    />
  );
};
