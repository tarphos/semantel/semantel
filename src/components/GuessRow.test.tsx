/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import GuessRow from "@/components/GuessRow";
import { ClosenessInfoProps } from "@/components/ClosenessInfo";

const mockClosenessInfo = jest.fn();
jest.mock(
  "@/components/ClosenessInfo",
  // eslint-disable-next-line react/display-name
  () => (props: ClosenessInfoProps) => {
    mockClosenessInfo(props);
    return <td data-testid="closenessInfo">Mocked ClosenessInfo</td>;
  }
);

describe("GuessRow", function () {
  it.each`
    darkMode | oldGuessIsLatestGuess | expectedColor
    ${false} | ${false}              | ${"0, 0, 0"}
    ${false} | ${true}               | ${"204, 0, 204"}
    ${true}  | ${false}              | ${"250, 250, 250"}
    ${true}  | ${true}               | ${"204, 0, 204"}
  `(
    "should choose correct color for old guess (darkMode: $darkMode, oldGuessIsLatestGuess: $oldGuessIsLatestGuess)",
    ({ darkMode, oldGuessIsLatestGuess, expectedColor }) => {
      renderInsideTable(
        <GuessRow
          darkMode={darkMode}
          guessNumber={1}
          oldGuess={"foo"}
          isLatestGuess={oldGuessIsLatestGuess}
          percentile={0.234}
          similarity={20}
          similarityStory={{ rest: 1, top: 2, top10: 3 }}
        />
      );

      expect(screen.getByTestId("guessRow").children[1]).toHaveStyle(
        `color: rgb(${expectedColor})`
      );
    }
  );

  it.each`
    darkMode
    ${false}
    ${true}
  `(
    "should choose correct color for similarity (darkMode: $darkMode)",
    ({ darkMode }) => {
      renderInsideTable(
        <GuessRow
          darkMode={darkMode}
          guessNumber={1}
          oldGuess="bar"
          isLatestGuess={false}
          percentile={0.234}
          similarity={20}
          similarityStory={{ rest: 1, top: 2, top10: 3 }}
        />
      );

      const expectedColor = darkMode ? "255, 204, 204" : "51, 0, 0"; // based on Math.round(props.similarity * 2.55)

      expect(screen.getByTestId("guessRow").children[2]).toHaveStyle(
        `color: rgb(${expectedColor})`
      );
    }
  );

  it("should print correct values", () => {
    renderInsideTable(
      <GuessRow
        darkMode={false}
        guessNumber={1}
        oldGuess="bar"
        isLatestGuess={false}
        percentile={0.234}
        similarity={20}
        similarityStory={{ rest: 1, top: 2, top10: 3 }}
      />
    );

    const row = screen.getByTestId("guessRow");
    expect(row.children[0]).toHaveTextContent("1");
    expect(row.children[1]).toHaveTextContent("bar");
    expect(row.children[2]).toHaveTextContent("20,00");
  });

  it("should render ClosenessInfo", () => {
    renderInsideTable(
      <GuessRow
        darkMode={false}
        guessNumber={1}
        oldGuess="bar"
        isLatestGuess={false}
        percentile={0.234}
        similarity={20}
        similarityStory={{ rest: 1, top: 2, top10: 3 }}
      />
    );

    expect(screen.getByTestId("closenessInfo")).toBeInTheDocument();
    expect(mockClosenessInfo).toHaveBeenCalledWith<[ClosenessInfoProps]>({
      percentile: 0.234,
      similarity: 20,
      similarityStory: { rest: 1, top: 2, top10: 3 },
    });
  });
});

const renderInsideTable = (component: JSX.Element) => {
  render(
    <table>
      <tbody>{component}</tbody>
    </table>
  );
};
