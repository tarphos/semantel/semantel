import { memo } from "react";
import { SVG_COLLAPSE, SVG_EXPAND, SVG_X } from "../svg/svgs";

export type CornerActionButtonProps = {
  id?: string;
  close: () => void;
  type: "close" | "collapse" | "expand";
};

const typeToButtonData = (type: CornerActionButtonProps["type"]) => {
  switch (type) {
    case "collapse":
      return { svg: SVG_COLLAPSE, ariaLabel: "verkleinern" };
    case "expand":
      return { svg: SVG_EXPAND, ariaLabel: "vergrößern" };
    case "close":
      return { svg: SVG_X, ariaLabel: "schließen" };
  }
};

const CornerActionButton = memo(function CornerActionButton(
  props: Readonly<CornerActionButtonProps>
) {
  const { svg, ariaLabel } = typeToButtonData(props.type);

  return (
    <button
      className="dialog-close"
      id={props.id}
      onClick={props.close}
      aria-label={ariaLabel}
    >
      {svg}
    </button>
  );
});

export default CornerActionButton;
