import React, { memo, useId } from "react";

const RadioInput = memo(function RadioInput(
  props: Readonly<{
    name: string;
    checked: boolean;
    onChange: () => void;
    children: React.ReactNode;
  }>
) {
  const id = useId();

  return (
    <>
      <input
        type="radio"
        name={props.name}
        id={id}
        defaultChecked={props.checked}
        onChange={props.onChange}
      />
      <label htmlFor={id}>{props.children}</label>
    </>
  );
});

export default RadioInput;
