import React, { memo } from "react";

const PayPal = memo(function PayPal() {
  // Stryker disable ObjectLiteral : TODO use some CSS framework
  const formStyle = { flexGrow: 2 };
  const inputStyle = { borderWidth: 0 };
  // Stryker restore ObjectLiteral

  return (
    <form
      action="https://www.paypal.com/donate"
      method="post"
      target="_top"
      style={formStyle}
    >
      <input type="hidden" name="hosted_button_id" value="U47WZSCC634RJ" />
      <input
        type="image"
        src="https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donateCC_LG.gif"
        style={inputStyle}
        name="submit"
        title="PayPal - The safer, easier way to pay online!"
        alt="Spenden mit dem PayPal-Button"
      />
    </form>
  );
});

export default PayPal;
