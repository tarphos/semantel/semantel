import Link from "next/link";
import { memo } from "react";

const Backlink = memo(function Backlink() {
  return (
    <>
      <br />
      <Link href={"/"}>Zurück zu Semantel</Link>
    </>
  );
});

export default Backlink;
