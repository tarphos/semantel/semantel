"use client";

import React, { memo, useEffect, useState } from "react";
import Semantel from "./Semantel";

const SemantelRoot = memo(function SemantelRoot(
  props: Readonly<{
    today: number;
    puzzleNumber: number;
    secrets: Array<string>;
    newPuzzleNumber?: number;
    openNewPuzzle: () => void;
  }>
) {
  const { today, puzzleNumber } = props;

  const [showNewDayMessage, setShowNewDayMessage] = useState(false);

  useEffect(() => {
    setShowNewDayMessage(
      props.newPuzzleNumber !== undefined &&
        props.newPuzzleNumber !== props.puzzleNumber
    );
  }, [props.newPuzzleNumber, props.puzzleNumber]);

  if (showNewDayMessage) {
    return (
      <div>
        Das neue Semantel (Nummer {props.newPuzzleNumber}) ist da!{" "}
        <a href="#" onClick={props.openNewPuzzle}>
          Zum neuen Rätsel
        </a>
      </div>
    );
  }

  return (
    <Semantel
      today={today}
      puzzleNumberForStorage={puzzleNumber}
      secrets={props.secrets}
    />
  );
});

export default SemantelRoot;
