import React, { ReactNode, memo } from "react";

type MenuButtonProps = {
  ariaLabel: string;
  openDialog: () => void;
  icon: ReactNode;
};

const MenuButton = memo(function MenuButton(props: Readonly<MenuButtonProps>) {
  return (
    <button
      aria-label={props.ariaLabel}
      onClick={props.openDialog}
      title={props.ariaLabel}
    >
      {props.icon}
    </button>
  );
});

export default MenuButton;
