import React, { memo } from "react";

export type ErrorMessageProps = {
  message: string;
};

const ErrorMessage = memo(function ErrorMessage_(
  props: Readonly<ErrorMessageProps>
) {
  return (
    <div id="error" aria-live="assertive">
      {props.message}
    </div>
  );
});

export default ErrorMessage;
