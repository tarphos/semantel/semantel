/**
 * @jest-environment jsdom
 */

import { ErrorMessageProps } from "@/components/ErrorMessage";
import { GuessFormProps } from "@/components/GuessForm";
import { GuessesProps } from "@/components/Guesses";
import { HintGiveUpProps } from "@/components/HintGiveUp";
import { LoadingSpinnerOverlayProps } from "@/components/LoadingSpinnerOverlay";
import { ResponseProps } from "@/components/Response";
import Semantel from "@/components/Semantel";
import { EMPTY_STATS } from "@/constants";
import {
  storageKey_guesses,
  storageKey_puzzleNumber,
  storageKey_stats,
  storageKey_winState,
} from "@/constants/localStorageKeys";
import { Stats } from "@/types";
import "@testing-library/jest-dom";
import { act, render, screen, waitFor } from "@testing-library/react";
import { MutableRefObject, useEffect } from "react";
import { mockComponent } from "../testutils/mockComponent";

jest.mock("fetch-retry"); // can't import data module otherwise
const dataModule = require("@/data");
const guessModule = require("@/guess");

const headerModule = require("@/components/Header");
const similarityStoryModule = require("@/components/SimilarityStory");
const errorMessageModule = require("@/components/ErrorMessage");
const loadingSpinnerOverlayModule = require("@/components/LoadingSpinnerOverlay");
const guessFormModule = require("@/components/GuessForm");
const responseModule = require("@/components/Response");
const guessesModule = require("@/components/Guesses");
const hintGiveUpModule = require("@/components/HintGiveUp");
const introTextModule = require("@/components/IntroText");

const todaysWord = "banane";

const secrets = [
  "word1",
  "word2",
  "word3",
  "word4",
  "word5",
  "word6",
  "word7",
  "word8",
  todaysWord,
];

describe("Semantel", function () {
  let testIdHeader: string;
  let testIdSimilarityStory: string;
  let testIdErrorMessage: string;
  let testIdLoadingSpinnerOverlay: string;
  let testIdGuessForm: string;
  let testIdResponse: string;
  let testIdGuesses: string;
  let testIdHintGiveUp: string;
  let testIdIntroText: string;

  let mockedSimilarityStory: jest.Mock;
  let mockedGuessForm: jest.Mock;
  let mockedResponse: jest.Mock;
  let mockedGuesses: jest.Mock;
  let mockedHintGiveUp: jest.Mock;

  beforeAll(() => {
    ({ testId: testIdHeader } = mockComponent(headerModule));
    ({ testId: testIdSimilarityStory, mockedComponent: mockedSimilarityStory } =
      mockComponent(similarityStoryModule));
    ({ testId: testIdErrorMessage } = mockComponent(
      errorMessageModule,
      (props: ErrorMessageProps) => props.message
    ));
    ({ testId: testIdLoadingSpinnerOverlay } = mockComponent(
      loadingSpinnerOverlayModule,
      (props: LoadingSpinnerOverlayProps) => (
        <>
          Mocked LoadingSpinnerOverlay {props.active ? "active" : "inactive"}
          {props.children}
        </>
      )
    ));
    ({ testId: testIdGuessForm, mockedComponent: mockedGuessForm } =
      mockComponent(guessFormModule));
    ({ testId: testIdResponse, mockedComponent: mockedResponse } =
      mockComponent(responseModule));
    ({ testId: testIdGuesses, mockedComponent: mockedGuesses } =
      mockComponent(guessesModule));
    ({ testId: testIdHintGiveUp, mockedComponent: mockedHintGiveUp } =
      mockComponent(hintGiveUpModule));
    ({ testId: testIdIntroText } = mockComponent(introTextModule));
  });

  beforeEach(() => {
    jest.spyOn(dataModule, "getModel").mockReturnValue(Promise.resolve());
    jest
      .spyOn(dataModule, "getSimilarityStory")
      .mockReturnValue(Promise.resolve());
    jest.spyOn(dataModule, "getNearby").mockReturnValue(Promise.resolve([]));
    jest.spyOn(dataModule, "hint").mockReturnValue(Promise.resolve());
    jest.spyOn(guessModule, "doGuess").mockReturnValue(Promise.resolve());

    window.matchMedia = jest.fn().mockReturnValue({});

    console.error = jest.fn();

    window.localStorage.clear();
  });

  describe("regular", function () {
    it("should display header", () => {
      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      expect(screen.getByTestId(testIdHeader)).toBeVisible();
    });

    it("should display similarity story", () => {
      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      expect(screen.getByTestId(testIdSimilarityStory)).toBeVisible();
      expect(mockedSimilarityStory).toHaveBeenCalledWith({
        puzzleNumber: 23,
        similarityStory: undefined,
      });
    });

    describe("ErrorMessage", () => {
      it("should not display error message", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );

        expect(screen.getByTestId(testIdErrorMessage)).toHaveTextContent("");
      });

      it("should display error message", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );
        mockedGuessForm.mockImplementation((props: GuessFormProps) => {
          useEffect(() => props.setError("error"));
        });

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );

        await waitFor(() =>
          expect(screen.getByTestId(testIdErrorMessage)).toHaveTextContent(
            "error"
          )
        );
      });
    });

    it("should display loading spinner overlay", async () => {
      dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));
      mockedGuessForm.mockImplementation((props: GuessFormProps) => {
        useEffect(() => {
          props.setLoadingSpinnerActive(true);
        });
      });

      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      await waitFor(() =>
        expect(
          screen.getByTestId(testIdLoadingSpinnerOverlay)
        ).toHaveTextContent(/^Mocked LoadingSpinnerOverlay active/)
      );
    });

    it("should display inactive loading spinner overlay", () => {
      dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));

      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      expect(screen.getByTestId(testIdLoadingSpinnerOverlay)).toHaveTextContent(
        /^Mocked LoadingSpinnerOverlay inactive/
      );
    });

    describe("GuessForm", () => {
      afterEach(() => {
        localStorage.clear();
      });

      it("should display guess form", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );
        await waitFor(() =>
          expect(screen.getByTestId(testIdGuessForm)).toBeVisible()
        );

        await waitFor(() =>
          expect(mockedGuessForm).toHaveBeenCalledWith(
            expect.objectContaining<Partial<GuessFormProps>>({
              stats: expect.any(Object), // TODO fill
            })
          )
        );
      });

      it("should pass default stats if not present", async () => {
        const puzzleNumber = 23;

        await shouldPassDefaultValue<GuessFormProps, Stats>(
          mockedGuessForm,
          "stats",
          {
            ...EMPTY_STATS,
            firstPlay: puzzleNumber,
            lastPlay: puzzleNumber,
            lastEnd: puzzleNumber - 1,
          }
        );
      });

      it("should pass stored stats if stats is up to date", async () => {
        const puzzleNumber = 23;

        await shouldPassStoredValue<GuessFormProps, Stats>(
          mockedGuessForm,
          "stats",
          { ...otherStats, lastPlay: puzzleNumber },
          puzzleNumber,
          storageKey_stats
        );
      });

      it("should pass updated stats storage if stats is outdated", async () => {
        await shouldPassUpdatedStatsIfStatsIsOutdated(mockedGuessForm);
      });

      it("should pass default win state if there is nothing stored", async () => {
        await shouldPassDefaultValue<GuessFormProps, number>(
          mockedGuessForm,
          "winState",
          -1
        );
      });

      it("should pass stored win state", async () => {
        const puzzleNumber = 23;

        window.localStorage.setItem(
          storageKey_puzzleNumber,
          JSON.stringify(puzzleNumber)
        );

        await shouldPassStoredValue<GuessFormProps, number>(
          mockedGuessForm,
          "winState",
          0,
          puzzleNumber,
          storageKey_winState("")
        );
      });

      it.skip("should pass default win state storage if stored state is outdated", async () => {
        // TODO fix and re-enable
        const puzzleNumber = 23;

        window.localStorage.setItem(
          storageKey_puzzleNumber,
          JSON.stringify(puzzleNumber - 3)
        );

        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        window.localStorage.setItem(
          storageKey_winState(""),
          JSON.stringify(100)
        );

        render(
          <Semantel
            puzzleNumberForStorage={puzzleNumber}
            today={19100}
            secrets={secrets}
          />
        );

        await waitFor(() =>
          expect(
            mockedGuessForm.mock.calls[0][0]["winStateStorage"].value
          ).toEqual(-1)
        );
      });
    });

    it("should display response", async () => {
      const puzzleNumber = 23;

      window.localStorage.setItem(
        storageKey_puzzleNumber,
        JSON.stringify(puzzleNumber)
      );

      window.localStorage.setItem(storageKey_winState(""), JSON.stringify(0));

      dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));

      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      expect(screen.getByTestId(testIdResponse)).toBeVisible();

      expect(mockedResponse).toHaveBeenCalledWith(
        expect.objectContaining<Partial<ResponseProps>>({
          puzzleNumberForDisplay: puzzleNumber,
          secret: todaysWord,
          won: false, // TODO: test change
          stats: {
            firstPlay: puzzleNumber,
            lastEnd: puzzleNumber - 1,
            lastPlay: puzzleNumber,
            winStreak: 0,
            playStreak: 0,
            totalGuesses: 0,
            wins: 0,
            giveups: 0,
            abandons: 0,
            totalPlays: 0,
            hints: 0,
          },
        })
      );
    });

    describe("Guesses", () => {
      it("should display empty guesses", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );
        await waitFor(() =>
          expect(screen.getByTestId(testIdGuesses)).toBeVisible()
        );

        expect(mockedGuesses).toHaveBeenCalledWith(
          expect.objectContaining<Partial<GuessesProps>>({
            chrono_forward: 1,
            darkMode: false,
            latestGuess: undefined,
            similarityStory: undefined,
            guesses: [],
          })
        );
      });

      it("should display stored guesses", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        window.localStorage.setItem(
          storageKey_puzzleNumber,
          JSON.stringify(23)
        );
        window.localStorage.setItem(
          storageKey_guesses(""),
          '[[25.868549777585,"eins",509,1],[22.718808310588006,"drei",null,3],[20.319184009871435,"zwei",null,2]]'
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );
        await waitFor(() =>
          expect(screen.getByTestId(testIdGuesses)).toBeVisible()
        );

        expect(mockedGuesses).toHaveBeenLastCalledWith(
          expect.objectContaining<Partial<GuessesProps>>({
            chrono_forward: 1,
            latestGuess: undefined,
            similarityStory: undefined,
            guesses: [
              {
                similarity: 25.868549777585,
                word: "eins",
                percentile: 509,
                index: 1,
              },
              {
                similarity: 22.718808310588006,
                word: "drei",
                percentile: undefined,
                index: 3,
              },
              {
                similarity: 20.319184009871435,
                word: "zwei",
                percentile: undefined,
                index: 2,
              },
            ],
          })
        );
      });

      it("should display empty guesses if stored guesses are outdated", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        window.localStorage.setItem(
          storageKey_puzzleNumber,
          JSON.stringify(23 - 1)
        );
        window.localStorage.setItem(
          storageKey_guesses(""),
          '[[25.868549777585,"eins",509,1],[22.718808310588006,"drei",null,3],[20.319184009871435,"zwei",null,2]]'
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );
        await waitFor(() =>
          expect(screen.getByTestId(testIdGuesses)).toBeVisible()
        );

        expect(mockedGuesses).toHaveBeenLastCalledWith(
          expect.objectContaining<Partial<GuessesProps>>({
            chrono_forward: 1,
            latestGuess: undefined,
            similarityStory: undefined,
            guesses: [],
          })
        );
      });
    });

    describe("HintGiveUp", () => {
      it("should display hint/giveup", async () => {
        dataModule.getModel.mockReturnValue(
          Promise.resolve({ vec: [1, 2, 3] })
        );

        render(
          <Semantel
            puzzleNumberForStorage={23}
            today={19100}
            secrets={secrets}
          />
        );
        await waitFor(() =>
          expect(screen.getByTestId(testIdHintGiveUp)).toBeVisible()
        );

        expect(mockedHintGiveUp).toHaveBeenCalledWith(
          expect.objectContaining<Partial<HintGiveUpProps>>({
            puzzleKey: 23,
            secret: todaysWord,
            secretVec: [1, 2, 3],
          })
        );
      });

      it("should pass default stats storage", async () => {
        await shouldPassDefaultValue<HintGiveUpProps, Stats>(
          mockedHintGiveUp,
          "stats",
          defaultStats
        );
      });

      it("should pass stored stats storage if stats is up to date", async () => {
        const puzzleNumber = 23;

        await shouldPassStoredValue<HintGiveUpProps, Stats>(
          mockedHintGiveUp,
          "stats",
          { ...otherStats, lastPlay: puzzleNumber },
          puzzleNumber,
          storageKey_stats
        );
      });

      it("should pass updated stats storage if stats is outdated", async () => {
        await shouldPassUpdatedStatsIfStatsIsOutdated(mockedHintGiveUp);
      });
    });

    it("should display intro text", () => {
      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );
      expect(screen.getByTestId(testIdIntroText)).toBeVisible();
    });
  });

  describe("Error handling", () => {
    it("should handle error while fetching secret vector correctly", async () => {
      dataModule.getModel.mockImplementation(() => {
        throw new Error("error");
      });

      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      await screen.findByText(/^Konnte Lösungswort nicht laden.*/);

      await waitFor(() =>
        expect(screen.queryByTestId(testIdGuessForm)).not.toBeInTheDocument()
      );
    });

    it("should handle error while fetching similarity story correctly", async () => {
      dataModule.getSimilarityStory.mockImplementation(() => {
        throw new Error("error");
      });

      render(
        <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
      );

      await screen.findByText(
        /^Konnte Informationen über heutiges Spiel nicht laden.*/
      );
    });
  });
});

async function shouldPassUpdatedStatsIfStatsIsOutdated(
  mockedComponent: jest.Mock<any, any, any>
) {
  dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));

  const puzzleNumber = 23;

  const storedStats: Stats = {
    firstPlay: 1,
    lastEnd: 2,
    lastPlay: puzzleNumber - 5,
    winStreak: 4,
    playStreak: 5,
    totalGuesses: 6,
    wins: 7,
    giveups: 8,
    abandons: 9,
    totalPlays: 10,
    hints: 11,
  };
  window.localStorage.setItem(storageKey_stats, JSON.stringify(storedStats));

  render(
    <Semantel
      puzzleNumberForStorage={puzzleNumber}
      today={19100}
      secrets={secrets}
    />
  );

  await waitFor(() => expect(mockedComponent).toHaveBeenCalled());

  const rawValue: Stats | MutableRefObject<Stats> =
    mockedComponent.mock.calls[0][0].stats;
  const value: Stats = isMutableRefObject(rawValue)
    ? rawValue.current
    : rawValue;

  await waitFor(() =>
    expect(value).toEqual({
      ...storedStats,
      totalPlays: storedStats.totalPlays + 1,
      abandons: storedStats.abandons + 1,
      lastPlay: puzzleNumber,
    })
  );
}

async function shouldPassStoredValue<T, U>(
  mockedComponent: jest.Mock<any, any, any>,
  valueName: keyof T,
  storedValue: U,
  puzzleNumber: number,
  storageKey: string,
  expectedValue?: T[keyof T]
) {
  dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));

  window.localStorage.setItem(
    storageKey_puzzleNumber,
    JSON.stringify(puzzleNumber)
  );
  window.localStorage.setItem(storageKey, JSON.stringify(storedValue));

  render(
    <Semantel
      puzzleNumberForStorage={puzzleNumber}
      today={19100}
      secrets={secrets}
    />
  );

  await waitFor(() => expect(mockedComponent).toHaveBeenCalled());

  const rawValue: T | MutableRefObject<T> =
    mockedComponent.mock.calls[0][0][valueName];
  const value: T = isMutableRefObject<T>(rawValue)
    ? rawValue.current
    : rawValue;

  expect(value).toEqual(expectedValue ?? storedValue);
}

async function shouldPassDefaultValue<T, U>(
  mockedComponent: jest.Mock<any, any, any>,
  valueName: keyof T,
  defaultValue: U
) {
  dataModule.getModel.mockReturnValue(Promise.resolve({ vec: [1, 2, 3] }));

  render(
    <Semantel puzzleNumberForStorage={23} today={19100} secrets={secrets} />
  );

  await waitFor(() => expect(mockedComponent).toHaveBeenCalled());

  const rawValue: T | MutableRefObject<T> =
    mockedComponent.mock.calls[0][0][valueName];
  const value: T = isMutableRefObject<T>(rawValue)
    ? rawValue.current
    : rawValue;

  expect(value).toEqual(defaultValue);
}

const defaultStats: Stats = {
  firstPlay: 23,
  lastEnd: 22,
  lastPlay: 23,
  winStreak: 0,
  playStreak: 0,
  totalGuesses: 0,
  wins: 0,
  giveups: 0,
  abandons: 0,
  totalPlays: 0,
  hints: 0,
};

const otherStats: Stats = {
  firstPlay: 1,
  lastEnd: 2,
  lastPlay: 3,
  winStreak: 4,
  playStreak: 5,
  totalGuesses: 6,
  wins: 7,
  giveups: 8,
  abandons: 9,
  totalPlays: 10,
  hints: 11,
};

function isMutableRefObject<T>(obj: any): obj is MutableRefObject<T> {
  return typeof obj === "object" && "current" in obj;
}
