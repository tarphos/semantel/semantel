/**
 * @jest-environment jsdom
 */

import { encodeBase64 } from "@/base64";
import Top1kLink from "@/components/Top1kLink";
import { top1kUrl } from "@/urls";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

describe("Top1kLink", function () {
  it("should show link", () => {
    const word = "hund";
    const content = "foo";

    render(<Top1kLink word={word}>{content}</Top1kLink>);

    const link = screen.getByText(content);
    expect(link).toBeVisible();
    expect(decodeURIComponent(link.getAttribute("href") ?? "")).toBe(
      top1kUrl(encodeBase64(word))
    );
  });
});
