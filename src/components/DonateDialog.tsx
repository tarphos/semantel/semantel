import { memo } from "react";
import Dialog from "./Dialog";
import PayPal from "./PayPal";
import { __ } from "@/constants/tsxConstants";

export type DonateDialogProps = {
  open: boolean;
  close: () => void;
};

const DonateDialog = memo(function DonateDialog_(
  props: Readonly<DonateDialogProps>
) {
  // Stryker disable StringLiteral : internal heading for aria
  const donateHeading = "donate-heading";
  // Stryker restore StringLiteral

  return (
    props.open && (
      <Dialog
        underlayId="donate-underlay"
        ariaLabelledby={donateHeading}
        closeButtonId="donate-close"
        close={props.close}
      >
        <h3 id={donateHeading}>Unterstützen</h3>
        <p>
          Semantel ist und bleibt kostenlos. Wenn du die Entwicklung und den
          Betrieb des Spiels aber unterstützen möchtest, kannst du das entweder
          {__}
          <a href="https://www.paypal.com/donate/?hosted_button_id=U47WZSCC634RJ">
            hier über PayPal tun
          </a>
          : <PayPal />
          <br />
          Oder du überweist direkt auf unser Geschäftskonto:
          <dl className="bankDetails">
            <dt>Inhaber:</dt> <dd>Tarphos Apps</dd>
            <dt>IBAN:</dt> <dd>DE40110101015507524943</dd>
            <dt>BIC:</dt> <dd>SOBKDEB2XXX</dd>
            <dt>Bank:</dt> <dd>Solaris</dd>
          </dl>
        </p>
      </Dialog>
    )
  );
});

export default DonateDialog;
