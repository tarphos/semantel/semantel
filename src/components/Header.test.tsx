/**
 * @jest-environment jsdom
 */

import Header_ from "@/components/Header";
import { readRules } from "@/constants/localStorageKeys";
import "@testing-library/jest-dom";
import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { mockComponent } from "../testutils/mockComponent";
import { FaqDialogProps } from "@/components/FaqDialog";

const faqDialogModule = require("@/components/FaqDialog");

jest.mock("fetch-retry"); // can't import data module otherwise
const dataModule = require("@/data");
let getNearbySpy: jest.SpyInstance<any, unknown[], any>;
beforeEach(() => {
  getNearbySpy = jest.spyOn(dataModule, "getNearby");
});

const secrets = [
  "word1",
  "word2",
  "word3",
  "word4",
  "word5",
  "word6",
  "word7",
  "word8",
  "word9",
];

describe("Header", function () {
  let testIdFaqDialog: string;
  let mockedFaqDialog: jest.Mock;

  beforeAll(() => {
    ({ mockedComponent: mockedFaqDialog } = mockComponent(
      faqDialogModule,
      (props: FaqDialogProps) => (props.open ? "FAQ" : "")
    ));
  });

  beforeEach(() => {
    getNearbySpy.mockImplementation((...args: unknown[]) => {
      return Promise.resolve(["a", "b", "c"]);
    });
  });

  afterEach(function () {
    localStorage.clear();
  });

  it("clicking rules icon should open dialog", async () => {
    localStorage.setItem(readRules, "true");

    await renderHeaderAndExpectClickingHeaderIconToOpenDialog(
      "Errate das Lösungswort",
      "Regeln"
    );
  });

  it("clicking settings icon should open dialog", async () => {
    await renderHeaderAndExpectClickingHeaderIconToOpenDialog(
      "Einstellungen",
      "Einstellungen"
    );
  });

  it("clicking faq icon should open dialog", async () => {
    await renderHeaderAndExpectClickingHeaderIconToOpenDialog(
      "FAQ",
      "FAQ",
      mockedFaqDialog
    );
  });

  it("should show rules dialog on first render", async () => {
    render(header());

    expect(screen.getByText("Errate das Lösungswort")).toBeVisible();
  });
});
async function renderHeaderAndExpectClickingHeaderIconToOpenDialog(
  dialogHeaderText: string,
  headerIconText: string,
  mockedDialog?: jest.Mock
) {
  render(header());

  mockedDialog !== undefined &&
    (await waitFor(() => expect(mockedDialog).toHaveBeenCalled()));

  expect(screen.queryByText(dialogHeaderText)).not.toBeInTheDocument();

  await act(() => userEvent.click(screen.getByLabelText(headerIconText)));

  await waitFor(() => expect(screen.getByText(dialogHeaderText)).toBeVisible());
}

const header = () => (
  <Header_
    darkMode={false}
    setDarkMode={jest.fn()}
    today={19100}
    secrets={secrets}
    openDonateDialog={jest.fn()}
  />
);
