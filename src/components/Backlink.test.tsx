/**
 * @jest-environment jsdom
 */

import Backlink from "@/components/Backlink";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

describe("Backlink", function () {
  it("should show link back", () => {
    render(<Backlink />);

    const link = screen.getByText("Zurück zu Semantel");
    expect(link).toBeVisible();
    expect(link).toHaveAttribute("href", "/");
  });
});
