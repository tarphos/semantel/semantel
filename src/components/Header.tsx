import { readRules } from "@/constants/localStorageKeys";
import { SVG_FAQ, SVG_RULES, SVG_SETTINGS } from "@/svg/svgs";
import useDialogState from "@/useDialogState";
import { Dispatch, memo, useEffect } from "react";
import FaqDialog, { FaqDialogProps } from "./FaqDialog";
import MenuButton from "./MenuButton";
import RulesDialog, { RulesDialogProps } from "./RulesDialog";
import Settings, { SettingsProps } from "./Settings";

export type HeaderProps = {
  today: number;
  darkMode?: boolean;
  setDarkMode: Dispatch<boolean | undefined>;
  openDonateDialog: () => void;
  secrets: Array<string>;
};

const Header = memo(function Header_(props: Readonly<HeaderProps>) {
  const [rulesOpen, openRules, closeRules] = useDialogState();
  const [settingsOpen, openSettings, closeSettings] = useDialogState();
  const [faqOpen, openFaq, closeFaq] = useDialogState();

  useEffect(() => {
    if (!localStorage.getItem(readRules)) {
      openRules();
    }
  }, [openRules]);

  const yesterday = props.secrets[props.secrets.length - 2];

  const rulesDialogProps: RulesDialogProps = {
    close: closeRules,
    open: rulesOpen,
    yesterday,
  };
  const faqDialogProps: FaqDialogProps = {
    close: closeFaq,
    open: faqOpen,
    today: props.today,
    yesterday,
    openDonateDialog: props.openDonateDialog,
    secrets: props.secrets,
  };
  const settingsProps: SettingsProps = {
    open: settingsOpen,
    close: closeSettings,
    darkMode: props.darkMode,
    setDarkMode: props.setDarkMode,
  };

  return (
    <>
      <header>
        <h2>Semantel</h2>
        <nav id="menu">
          <MenuButton
            ariaLabel="Regeln"
            icon={SVG_RULES}
            openDialog={openRules}
          />
          <MenuButton
            ariaLabel="Einstellungen"
            icon={SVG_SETTINGS}
            openDialog={openSettings}
          />
          <MenuButton ariaLabel="FAQ" icon={SVG_FAQ} openDialog={openFaq} />
        </nav>
      </header>
      <RulesDialog {...rulesDialogProps} />
      <FaqDialog {...faqDialogProps} />
      <Settings {...settingsProps} />
    </>
  );
});

export default Header;
