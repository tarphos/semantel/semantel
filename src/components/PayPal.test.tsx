/**
 * @jest-environment jsdom
 */

import PayPal from "@/components/PayPal";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

describe("PayPal", function () {
  it("should display button", () => {
    render(<PayPal />);
    expect(screen.getByRole("button")).toHaveAttribute(
      "alt",
      "Spenden mit dem PayPal-Button"
    );
  });

  it("should include button id", () => {
    render(<PayPal />);
    expect(screen.getByDisplayValue("U47WZSCC634RJ")).toHaveAttribute(
      "name",
      "hosted_button_id"
    );
  });
});
