import {
  Dispatch,
  MutableRefObject,
  SetStateAction,
  memo,
  useRef,
  useState,
} from "react";
import { doGuess } from "../guess";
import { CommonDoGuessAndHintParams, GuessesEntry, Stats } from "../types";

export type GuessFormProps = {
  secret: string;
  puzzleKey: number;
  setError: (message: string) => void;
  setChronoForward: Dispatch<number>;
  setLatestGuess: Dispatch<string>;
  secretVec: Array<number>;
  setLoadingSpinnerActive: (newValue: boolean) => void;
  stats: MutableRefObject<Stats>;
  setStats: Dispatch<SetStateAction<Stats>>;
  puzzleNumberFromStorage: number;
  winState: MutableRefObject<number>;
  setWinState: Dispatch<SetStateAction<number>>;
  guesses: Array<GuessesEntry>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
  hintsUsed: number;
  setNumberOfGuessesToEnd: Dispatch<SetStateAction<number>>;
};

const GuessForm = memo(function GuessForm_(props: Readonly<GuessFormProps>) {
  const [guessValue, setGuessValue] = useState("");

  const guessInputRef = useRef<HTMLInputElement>(null);

  return (
    <form
      autoCorrect="off"
      autoCapitalize="none"
      autoComplete="off"
      onSubmit={async (event) => {
        event.preventDefault();
        guessInputRef.current?.focus();
        props.setError("");
        let guess = guessValue.trim().replace("!", "").replace("*", "");
        if (guess) {
          guess = guess.toLowerCase();
          setGuessValue("");
          const commonDoGuessParams: CommonDoGuessAndHintParams = {
            secret: props.secret,
            puzzleKey: props.puzzleKey,
            setError: props.setError,
            setChronoForward: props.setChronoForward,
            setLatestGuess: props.setLatestGuess,
            secretVec: props.secretVec,
            winState: props.winState.current,
            setWinState: props.setWinState,
            guesses: props.guesses,
            setGuesses: props.setGuesses,
            hintsUsed: props.hintsUsed,
            setNumberOfGuessesToEnd: props.setNumberOfGuessesToEnd,
          };
          props.setLoadingSpinnerActive(true);
          try {
            await doGuess({
              guess,
              is_hint: false,
              stats: props.stats.current,
              setStats: props.setStats,
              puzzleNumberFromStorage: props.puzzleNumberFromStorage,
              ...commonDoGuessParams,
            });
          } catch (e) {
            props.setError(
              `An error ooccurred when submitting your guess: ${
                e instanceof Error ? e.message : e
              }`
            );
          } finally {
            props.setLoadingSpinnerActive(false);
          }
        }
        return false;
      }}
    >
      <div id="guess-wrapper">
        <input
          placeholder="Raten"
          autoCorrect="off"
          autoCapitalize="none"
          autoComplete="off"
          type="text"
          id="guess"
          value={guessValue}
          onChange={(event) => setGuessValue(event.target.value)}
          ref={guessInputRef}
        />
        <input type="submit" value="Raten" />
      </div>
    </form>
  );
});

export default GuessForm;
