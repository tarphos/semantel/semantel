import { getNearby1k } from "@/data";
import formatNumber from "@/formatNumber";
import { Top1kEntry } from "@/types";
import { memo, useEffect, useState } from "react";

const NearestTable = memo(function NearestTable(
  props: Readonly<{ word: string }>
) {
  const [rows, setRows] = useState<Array<Top1kEntry>>([]);

  useEffect(() => {
    const fetchData = async () => {
      const newRows = await getNearby1k(props.word);
      if (newRows !== undefined) setRows(newRows); // TODO error handling here
    };
    fetchData();
  }, [props.word]);

  return (
    <div>
      <table className="nearestTable">
        <caption>
          Die Wörter, die <strong>{props.word}</strong> am nächsten sind, sind:
        </caption>
        <thead>
          <tr>
            <th>Nähe (höher ist näher)</th>
            <th>Wort</th>
            <th>Ähnlichlkeit</th>
          </tr>
        </thead>
        <tbody>
          {rows.map((row) => (
            <tr key={row.neighbor}>
              <td>{row.percentile}</td>
              <td>{row.neighbor}</td>
              <td>{formatNumber(100 * row.similarity)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
});

export default NearestTable;
