/**
 * @jest-environment jsdom
 */

import AccordionElement from "@/components/AccordionElement";
import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import assert from "assert";
import { useState } from "react";

const title = "Some title";
const content = "some content";

describe("AccordionElement", function () {
  describe("title", () => {
    const activeValues = [true, false];

    activeValues.forEach((active) =>
      it(`should be displayed if element ${
        active ? "" : "not "
      }active`, function () {
        render(<AccordionElementContainer initiallyActive={active} />);

        assert.ok(screen.queryByText(title) !== null, "Title not present");
      })
    );
  });

  describe("content", () => {
    const activeValues = [true, false];

    activeValues.forEach((active) =>
      it(`should only be displayed if element is active (${active})`, function () {
        render(<AccordionElementContainer initiallyActive={active} />);

        assert.equal(screen.queryByText(content) !== null, active);
      })
    );
  });

  describe("toggling", () => {
    it(`should toggle active state on click`, async () => {
      render(<AccordionElementContainer initiallyActive={false} />);

      assert.equal(screen.queryByText(content) !== null, false);

      await act(() => userEvent.click(screen.getByText(title)));

      assert.equal(screen.queryByText(content) !== null, true);

      await act(() => userEvent.click(screen.getByText(title)));

      assert.equal(screen.queryByText(content) !== null, false);
    });
  });
});

const AccordionElementContainer = (props: { initiallyActive: boolean }) => {
  const [active, setActive] = useState(props.initiallyActive);

  return (
    <AccordionElement
      title={title}
      active={active}
      toggleActive={() => setActive((oldValue) => !oldValue)}
    >
      {content}
    </AccordionElement>
  );
};
