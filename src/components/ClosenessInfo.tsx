import React, { memo } from "react";
import { SimilarityStoryType } from "../types";
import { __ } from "@/constants/tsxConstants";

export type ClosenessInfoProps = {
  similarityStory: SimilarityStoryType | undefined;
  similarity: number;
  percentile: number | undefined;
};

const ClosenessInfo = memo(function ClosenessInfo(
  props: Readonly<ClosenessInfoProps>
) {
  const { cls, content } = determineContent(props);

  return <td className={cls}>{content}</td>;
});

function determineContent(props: ClosenessInfoProps) {
  if (props.percentile) {
    if (props.percentile == 1000) {
      return {
        content: "GEFUNDEN!",
      };
    } else {
      // Stryker disable StringLiteral, ObjectLiteral : TODO use some CSS framework
      const cls = "close";
      const progressBarStyle = {
        width: `${props.percentile / 10}%`,
      };
      // Stryker restore StringLiteral, ObjectLiteral

      return {
        cls,
        content: (
          <>
            <span className="percentile">{props.percentile}/1000</span>
            {__}
            <span className="progress-container">
              <span
                className="progress-bar"
                style={progressBarStyle}
                data-testid="progressbar"
              >
                &nbsp;
              </span>
            </span>
          </>
        ),
      };
    }
  } else if (
    props.similarityStory !== undefined &&
    props.similarity >= props.similarityStory.rest * 100
  ) {
    return {
      content: (
        <span className="weirdWord">
          ????
          {/*
           */}
          <span className="tooltiptext">
            Ungewöhnliches Wort gefunden! Dieses Wort ist nicht in der Liste
            &quot;normaler&quot; Wörter, die wir für die Top-1000-Liste
            verwenden, aber es ist dennoch ähnlich!
          </span>
        </span>
      ),
    };
  } else {
    return {
      content: "(kalt)",
    };
  }
}

export default ClosenessInfo;
