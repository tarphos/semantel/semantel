import { memo } from "react";
import { share } from "../share";
import { GuessesEntry } from "../types";
import { __ } from "@/constants/tsxConstants";

export type ShareInvitationProps = {
  won: boolean;
  puzzleNumberForDisplay: number;
  guesses: Array<GuessesEntry>;
  hintsUsed: number;
  numberOfGuessesToEnd: number;
};

const ShareInvitation = memo(function ShareInvitation_(
  props: Readonly<ShareInvitationProps>
) {
  return (
    <p>
      <a
        href="#"
        onClick={() =>
          share(
            props.puzzleNumberForDisplay,
            props.won,
            props.guesses,
            props.hintsUsed,
            props.numberOfGuessesToEnd
          )
        }
      >
        Teile deinen Erfolg
      </a>
      {__} und spiele morgen wieder.
    </p>
  );
});

export default ShareInvitation;
