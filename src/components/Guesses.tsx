import { Dispatch, SetStateAction, memo } from "react";
import { GuessesEntry, SimilarityStoryType } from "../types";
import GuessRow from "./GuessRow";

export type GuessesProps = {
  latestGuess: string | undefined;
  chrono_forward: number;
  setChrono_forward: Dispatch<SetStateAction<number>>;
  darkMode: boolean;
  similarityStory: SimilarityStoryType | undefined;
  guesses: Array<GuessesEntry>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
};

const Guesses = memo(function Guesses_(props: Readonly<GuessesProps>) {
  return (
    <figure style={{ margin: 0 }}>
      <figcaption>Deine heutigen Versuche:</figcaption>
      <table aria-live="polite" id="guesses" role="log">
        <thead>
          <tr>
            <th
              id="chronoOrder"
              onClick={() => {
                props.setGuesses((oldValue) =>
                  [...oldValue].sort(
                    (a, b) => props.chrono_forward * (a.index - b.index)
                  )
                );
                props.setChrono_forward((oldValue) => oldValue * -1);
              }}
            >
              #
            </th>
            <th
              id="alphaOrder"
              onClick={() => {
                props.setGuesses((oldValue) =>
                  [...oldValue].sort(function (a, b) {
                    return a.word.localeCompare(b.word);
                  })
                );
                props.setChrono_forward(1);
              }}
            >
              Tipp
            </th>
            <th
              id="similarityOrder"
              onClick={() => {
                props.setGuesses((oldValue) =>
                  [...oldValue].sort(function (a, b) {
                    return b.similarity - a.similarity;
                  })
                );
                props.setChrono_forward(1);
              }}
            >
              Ähnlichkeit
            </th>
            <th>Nah dran?</th>
          </tr>
        </thead>
        <tbody>
          {props.guesses.map((guess) => {
            /* This is dumb: first we find the most-recent word, and put
            it at the top.  Then we do the rest. */
            const isLatestGuess = guess.word === props.latestGuess;
            if (isLatestGuess) {
              return (
                <GuessRow
                  similarityStory={props.similarityStory}
                  similarity={guess.similarity}
                  oldGuess={guess.word}
                  isLatestGuess={isLatestGuess}
                  percentile={guess.percentile}
                  guessNumber={guess.index}
                  darkMode={props.darkMode}
                  key={guess.index}
                />
              );
            }
            return null;
          })}
          <tr>
            <td colSpan={4}>
              <hr />
            </td>
          </tr>
          {props.guesses.map((guess) => {
            const isLatestGuess = guess.word === props.latestGuess;
            if (!isLatestGuess) {
              return (
                <GuessRow
                  similarityStory={props.similarityStory}
                  similarity={guess.similarity}
                  oldGuess={guess.word}
                  isLatestGuess={isLatestGuess}
                  percentile={guess.percentile}
                  guessNumber={guess.index}
                  darkMode={props.darkMode}
                  key={guess.index}
                />
              );
            }
            return null;
          })}
        </tbody>
      </table>
    </figure>
  );
});

export default Guesses;
