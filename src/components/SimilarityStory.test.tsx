/**
 * @jest-environment jsdom
 */

import SimilarityStory from "@/components/SimilarityStory";
import { SimilarityStoryType } from "@/types";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

describe("SimilarityStory", function () {
  it("shouldDisplayCorrectStory", () => {
    const puzzleNumber = 1;
    const similarityStory: SimilarityStoryType = {
      rest: 0.07,
      top: 0.13,
      top10: 0.17,
    };
    render(
      <SimilarityStory
        puzzleNumber={puzzleNumber}
        similarityStory={similarityStory}
      />
    );

    expect(screen.getByText(/Das/)).toHaveTextContent(
      `Das heutige Rätsel ist Semantel Nummer ${puzzleNumber}. Das nächste Wort hat eine Ähnlichkeit von ${toGermanNumber(
        similarityStory.top * 100
      )}, das zehnt-nächste hat eine Ähnlichkeit von ${toGermanNumber(
        similarityStory.top10 * 100
      )} und das tausendst-nächste hat eine Ähnlichkeit von ${toGermanNumber(
        similarityStory.rest * 100
      )}.`
    );
  });

  it("shouldDisplayNothingIfThereIsNoStoryData", () => {
    const puzzleNumber = 1;
    const similarityStory = undefined;
    render(
      <SimilarityStory
        puzzleNumber={puzzleNumber}
        similarityStory={similarityStory}
      />
    );

    expect(screen.queryByText(/Das/)).toBeNull();
  });
});

const toGermanNumber = (n: number) => n.toFixed(2).replace(".", ",");
