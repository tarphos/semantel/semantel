/**
 * @jest-environment jsdom
 */

import FaqDialog, { FaqDialogProps } from "@/components/FaqDialog";
import "@testing-library/jest-dom";
import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import assert from "assert";

jest.mock("fetch-retry"); // can't import data module otherwise
const dataModule = require("@/data");

const secrets = [
  "interessieren",
  "titel",
  "stimme",
  "schätzen",
  "soldat",
  "helfen",
  "rund",
  "Käse",
  "word9",
];

describe("Faq", function () {
  let getNearbySpy: jest.SpyInstance<any, unknown[], any>;

  const yesterday = "Käse";
  const encodedYesterday = "S8Okc2U%3D";
  const yesterdayNearbyArray = ["a", "b", "c"];

  const props: FaqDialogProps = {
    today: 19108,
    yesterday: yesterday,
    open: true,
    close: jest.fn(),
    openDonateDialog: jest.fn(),
    secrets: secrets,
  };

  beforeEach(() => {
    getNearbySpy = jest.spyOn(dataModule, "getNearby");
  });

  describe("regular behaviour", () => {
    beforeEach(() => {
      getNearbySpy.mockImplementation((...args: unknown[]) => {
        if (args[0] === yesterday) {
          return Promise.resolve(yesterdayNearbyArray);
        } else {
          console.error("Called getNearby with wrong argument", args);
        }
      });
    });

    it("should show FAQ", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      expect(screen.queryByText("FAQ")).toBeVisible();
    });

    it("should not show FAQ", async () => {
      await renderWithAct(<FaqDialog {...props} open={false} />);

      expect(screen.queryByText("FAQ")).not.toBeInTheDocument();
    });

    it("should update yesterday text", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      await act(() =>
        userEvent.click(
          screen.getByText(
            "Welche Wörter waren dem gestrigen Wort am ähnlichsten?"
          )
        )
      );

      assert.equal(
        screen.getByTestId("yesterdayText").innerHTML,
        `${yesterdayNearbyArray.join(
          ", "
        )}, nach Nähe absteigend. <a href="top1k?w=${encodedYesterday}">Mehr?</a>`
      );
    });

    it("clicking on accordion items should work as expected", async () => {
      jest
        .spyOn(dataModule, "getNearby")
        .mockImplementation((...args: unknown[]) => {
          if (args[0] === yesterday) {
            return Promise.resolve(yesterdayNearbyArray);
          } else {
            console.error("Called getNearby with wrong argument", args);
          }
        });

      await renderWithAct(<FaqDialog {...props} />);

      const header1 = "Wie funktioniert das?";
      const text1 =
        "Hier ist ein Artikel über Word2Vec, was dem ganzen zugrunde liegt";

      const header2 = "Was bedeutet „????“?";
      const text2 =
        "Dieses Wort ist nicht in der Wortliste von gebräuchlichen Wörtern, die wir für die Top-1000-Liste hernehmen, aber es ist dem Lösungswort trotzdem sehr ähnlich.";

      assert.ok(screen.queryByText(text1) === null);
      assert.ok(screen.queryByText(text2) === null);

      await act(async () => userEvent.click(screen.getByText(header1)));

      assert.ok(screen.queryByText(text1) !== null);
      assert.ok(screen.queryByText(text2) === null);

      await act(async () => userEvent.click(screen.getByText(header2)));

      assert.ok(screen.queryByText(text2) !== null);
      assert.ok(screen.queryByText(text1) !== null);

      await act(async () => userEvent.click(screen.getByText(header1)));

      assert.ok(screen.queryByText(text1) === null);
      assert.ok(screen.queryByText(text2) !== null);
    });

    it("should update yesterday's word", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      await act(() =>
        userEvent.click(screen.getByText("Was war das Wort von gestern?"))
      );

      assert.equal(
        screen.getByTestId("yesterdaysWord").innerHTML,
        `Gestern war das Lösungswort „Käse“ Die Wörter davor waren: „rund“, „helfen“, „soldat“, „schätzen“, „stimme“, „titel“, „interessieren“.`
      );
    });

    it("should switch to donate dialog", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      expect(screen.queryByText("FAQ")).toBeVisible();

      await userEvent.click(screen.getByText("Unterstützen?"));
      await userEvent.click(
        screen.getByText(
          "Erfahre hier, wie du uns finanziell unterstützen kannst,"
        )
      );

      expect(props.openDonateDialog).toHaveBeenCalled();
      expect(props.close).toHaveBeenCalled();
    });

    it("should not fetch yesterday's words directly on load", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      expect(getNearbySpy).not.toHaveBeenCalled();
    });
  });

  describe("error handling", () => {
    let errorMock: jest.SpyInstance;
    beforeEach(() => {
      getNearbySpy.mockImplementation(() => {
        throw new Error("Some error");
      });
      errorMock = jest.spyOn(console, "error");
      errorMock.mockImplementation(() => {});
    });

    it("should log error when fetching yesterday's information", async () => {
      await renderWithAct(<FaqDialog {...props} />);

      await userEvent.click(
        screen.getByText(
          "Welche Wörter waren dem gestrigen Wort am ähnlichsten?"
        )
      );

      await waitFor(() => {
        expect(errorMock).toHaveBeenCalled();
      });
      expect(errorMock.mock.calls[0][0].message).toBe("Some error");
    });
  });
});
function renderWithAct(component: JSX.Element) {
  return act(async () => {
    render(component);
  });
}
