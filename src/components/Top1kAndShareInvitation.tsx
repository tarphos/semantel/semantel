import { memo } from "react";
import Top1kLink from "./Top1kLink";

export type Top1kAndShareInvitationProps = {
  secret: string;
  openDonateDialog: () => void;
};

//TODO rename to Top1kAndDonateInvitation
const Top1kAndShareInvitation = memo(function Top1kAndShareInvitation_(
  props: Readonly<Top1kAndShareInvitationProps>
) {
  return (
    <p>
      Du kannst dir{" "}
      <Top1kLink word={props.secret}>
        hier die tausend ähnlichsten Wörter anschauen
      </Top1kLink>
      .<br />
      Erfahre{" "}
      <a href="#" onClick={props.openDonateDialog}>
        hier, wie du uns finanziell unterstützen kannst.
      </a>
    </p>
  );
});

export default Top1kAndShareInvitation;
