import React, { memo } from "react";
import { SimilarityStoryType } from "../types";
import ClosenessInfo from "./ClosenessInfo";
import formatNumber from "@/formatNumber";

type GuessRowProps = {
  similarity: number;
  oldGuess: string;
  isLatestGuess: boolean;
  percentile: number | undefined;
  guessNumber: number;
  darkMode: boolean;
  similarityStory: SimilarityStoryType | undefined;
};

const GuessRow = memo(function GuessRow(props: Readonly<GuessRowProps>) {
  let color;
  if (props.isLatestGuess) {
    color = "#c0c";
  } else if (props.darkMode) {
    color = "#fafafa";
  } else {
    color = "#000";
  }
  const similarityLevel = Math.round(props.similarity * 2.55);
  let similarityColor;
  if (props.darkMode) {
    similarityColor = `255,${255 - similarityLevel},${255 - similarityLevel}`;
  } else {
    similarityColor = `${similarityLevel},0,0`;
  }
  return (
    <tr data-testid="guessRow">
      <td>{props.guessNumber}</td>
      <td style={{ color: color }}>{props.oldGuess}</td>
      <td style={{ color: `rgb(${similarityColor})` }}>
        {formatNumber(props.similarity)}
      </td>
      <ClosenessInfo
        percentile={props.percentile}
        similarity={props.similarity}
        similarityStory={props.similarityStory}
      />
    </tr>
  );
});

export default GuessRow;
