import React, { memo } from "react";
import { SimilarityStoryType } from "../types";
import formatNumber from "@/formatNumber";

export type SimilarityStoryProps = {
  puzzleNumber: number;
  similarityStory: SimilarityStoryType | undefined;
};

const SimilarityStory = memo(function SimilarityStory_(
  props: Readonly<SimilarityStoryProps>
) {
  return props.similarityStory ? (
    <p id="similarityStory">
      Das heutige Rätsel ist Semantel Nummer <b>{props.puzzleNumber}</b>. Das
      nächste Wort hat eine Ähnlichkeit von{" "}
      <b>{formatNumber(props.similarityStory.top * 100)}</b>, das zehnt-nächste
      hat eine Ähnlichkeit von {formatNumber(props.similarityStory.top10 * 100)}{" "}
      und das tausendst-nächste hat eine Ähnlichkeit von{" "}
      {formatNumber(props.similarityStory.rest * 100)}.
    </p>
  ) : null;
});

export default SimilarityStory;
