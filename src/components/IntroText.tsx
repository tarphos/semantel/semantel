import { __ } from "@/constants/tsxConstants";
import { memo } from "react";

const IntroText = memo(function IntroText_(props: { openDonate: () => void }) {
  return (
    <p>
      Vielen Dank an all die, die die Entwicklung und den Betrieb dieses Spiels
      finanziell unterstützen!{__}
      <a href="#" id="openDonate" onClick={props.openDonate}>
        Erfahre hier, wie du das auch tun kannst.
      </a>
    </p>
  );
});

export default IntroText;
