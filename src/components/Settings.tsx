import { Dispatch, memo } from "react";
import Dialog from "./Dialog";
import RadioInput from "./RadioInput";

export type SettingsProps = {
  open: boolean;
  close: () => void;
  setDarkMode: Dispatch<boolean | undefined>;
  darkMode?: boolean;
};

const Settings = memo(function Settings_(props: Readonly<SettingsProps>) {
  // Stryker disable StringLiteral : internal ID for aria
  const headingId = "settings-heading";

  return (
    props.open && (
      <Dialog
        underlayId="settings-underlay"
        ariaLabelledby={headingId}
        closeButtonId="settings-close"
        close={props.close}
      >
        <h3 id={headingId}>Einstellungen</h3>
        <div>
          <RadioInput
            name="dark-mode"
            checked={props.darkMode === true}
            onChange={() => props.setDarkMode(true)}
          >
            Dunkler Modus
          </RadioInput>
          <RadioInput
            name="dark-mode"
            checked={props.darkMode === undefined}
            onChange={() => props.setDarkMode(undefined)}
          >
            wie System
          </RadioInput>
          <RadioInput
            name="dark-mode"
            checked={props.darkMode === false}
            onChange={() => props.setDarkMode(false)}
          >
            Heller Modus
          </RadioInput>
        </div>
      </Dialog>
    )
  );
});

export default Settings;
