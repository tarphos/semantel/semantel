/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import LoadingSpinnerOverlay from "./LoadingSpinnerOverlay";

describe("LoadingSpinnerOverlay", function () {
  it.each([true, false])(
    "spinner should show content (active: %p)",
    (active: boolean) => {
      renderSpinner(active);
      expect(screen.getByText("foo")).toBeVisible();
    }
  );

  it("active spinner should show spinner", () => {
    renderSpinner(true);
    expect(screen.getByTestId("wrapper").innerHTML).not.toBe("<p>foo</p>");
  });

  it("inactive spinner should not show spinner", () => {
    renderSpinner(false);
    expect(screen.getByTestId("wrapper").innerHTML).toBe("<p>foo</p>");
  });
});
function renderSpinner(active: boolean) {
  render(
    <LoadingSpinnerOverlay active={active}>
      <p>foo</p>
    </LoadingSpinnerOverlay>
  );
}
