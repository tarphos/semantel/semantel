/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import ResponseContentGivenUp from "./ResponseContentGivenUp";

describe("ResponseContentGivenUp", function () {
  it("should print correct message", () => {
    const secret = "secret";
    render(<ResponseContentGivenUp secret={secret} />);

    expect(screen.getByTestId("givenup")).toHaveTextContent(
      `\
Du hast aufgegeben! \
Das Lösungswort ist: ${secret}. \
Gib ruhig weitere Wörter ein, wenn du neugierig bist, wie die Ähnlichkeiten aussehen.`
    );
  });
});
