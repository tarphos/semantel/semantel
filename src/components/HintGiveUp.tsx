import { DEFAULT_WIN_STATE } from "@/constants";
import React, {
  Dispatch,
  MutableRefObject,
  SetStateAction,
  memo,
  useCallback,
} from "react";
import { confirmIfNotGameOver } from "../confirm";
import { hint } from "../data";
import { GuessesEntry, Stats } from "../types";
import { endGame } from "../utils";

export type HintGiveUpProps = {
  secret: string;
  puzzleKey: number;
  setChronoForward: React.Dispatch<number>;
  setError: (message: string) => void;
  setLatestGuess: React.Dispatch<string>;
  secretVec: number[];
  puzzleNumberFromStorage: number;
  stats: MutableRefObject<Stats>;
  setStats: Dispatch<SetStateAction<Stats>>;
  winState: number;
  setWinState: Dispatch<SetStateAction<number>>;
  guesses: MutableRefObject<Array<GuessesEntry>>;
  setGuesses: Dispatch<SetStateAction<Array<GuessesEntry>>>;
  hintsUsed: number;
  setHintsUsed: Dispatch<SetStateAction<number>>;
  setNumberOfGuessesToEnd: Dispatch<SetStateAction<number>>;
};

const HintGiveUp = memo(function HintGiveUp_(props: Readonly<HintGiveUpProps>) {
  const hintAction = useCallback(
    () =>
      hint({
        alert,
        puzzleKey: props.puzzleKey,
        secret: props.secret,
        secretVec: props.secretVec,
        setChronoForward: props.setChronoForward,
        setError: props.setError,
        setLatestGuess: props.setLatestGuess,
        puzzleNumberFromStorage: props.puzzleNumberFromStorage,
        stats: props.stats.current,
        setStats: props.setStats,
        winState: props.winState,
        setWinState: props.setWinState,
        guesses: props.guesses.current,
        setGuesses: props.setGuesses,
        hintsUsed: props.hintsUsed,
        setHintsUsed: props.setHintsUsed,
        setNumberOfGuessesToEnd: props.setNumberOfGuessesToEnd,
      }),
    [
      props.puzzleKey,
      props.secret,
      props.secretVec,
      props.setChronoForward,
      props.setError,
      props.setLatestGuess,
      props.puzzleNumberFromStorage,
      props.stats,
      props.setStats,
      props.winState,
      props.setWinState,
      props.guesses,
      props.setGuesses,
      props.hintsUsed,
      props.setHintsUsed,
      props.setNumberOfGuessesToEnd,
    ]
  );

  return (
    <>
      <input
        type="button"
        value="Hinweis"
        id="hint-btn"
        onClick={() => {
          confirmIfNotGameOver(
            props.winState > DEFAULT_WIN_STATE,
            "Willst du wirklich einen Hinweis?",
            hintAction
          );
        }}
      />
      {props.winState > DEFAULT_WIN_STATE || (
        <input
          type="button"
          value="Aufgeben"
          id="give-up-btn"
          onClick={() =>
            confirmIfNotGameOver(
              props.winState > DEFAULT_WIN_STATE,
              "Willst du wirklich aufgeben?",
              async () =>
                endGame({
                  newWinState: 0,
                  puzzleNumber: props.puzzleKey,
                  puzzleNumberFromStorage: props.puzzleNumberFromStorage,
                  setStats: props.setStats,
                  winState: props.winState,
                  setWinState: props.setWinState,
                  guesses: props.guesses.current,
                  setGuesses: props.setGuesses,
                  hintsUsed: props.hintsUsed,
                  setNumberOfGuessesToEnd: props.setNumberOfGuessesToEnd,
                })
            )
          }
        />
      )}
    </>
  );
});

export default HintGiveUp;
