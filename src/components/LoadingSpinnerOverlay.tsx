import React, { memo } from "react";
import LoadingOverlay from "react-loading-overlay-ts";
import PacmanLoader from "react-spinners/PacmanLoader";

export type LoadingSpinnerOverlayProps = {
  active: boolean;
  children: React.ReactNode;
};

const LoadingSpinnerOverlay = memo(function LoadingSpinnerOverlay_({
  active,
  children,
}: Readonly<LoadingSpinnerOverlayProps>) {
  return (
    <LoadingOverlay
      active={active}
      spinner={<PacmanLoader color="white" />}
      fadeSpeed={50}
    >
      {children}
    </LoadingOverlay>
  );
});

export default LoadingSpinnerOverlay;
