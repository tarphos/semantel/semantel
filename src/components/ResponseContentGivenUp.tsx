import { memo } from "react";

export type ResponseContentGivenUpProps = {
  secret: string;
};

const ResponseContentGivenUp = memo(function ResponseContentGivenUp_(
  props: Readonly<ResponseContentGivenUpProps>
) {
  return (
    <p data-testid="givenup">
      <b>Du hast aufgegeben! Das Lösungswort ist: {props.secret}</b>. Gib ruhig
      weitere Wörter ein, wenn du neugierig bist, wie die Ähnlichkeiten
      aussehen.
    </p>
  );
});

export default ResponseContentGivenUp;
