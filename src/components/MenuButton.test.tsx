/**
 * @jest-environment jsdom
 */

import MenuButton from "@/components/MenuButton";
import { render, screen } from "@testing-library/react";

import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";

describe("MenuButton", function () {
  it("should call callback on click", async () => {
    const openDialog = jest.fn();
    render(
      <MenuButton ariaLabel="bar" icon={<>foo</>} openDialog={openDialog} />
    );

    expect(openDialog).not.toHaveBeenCalled();
    await userEvent.click(screen.getByText("foo"));
    expect(openDialog).toHaveBeenCalled();
  });
});
