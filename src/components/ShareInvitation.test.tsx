/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import ShareInvitation from "./ShareInvitation";
import userEvent from "@testing-library/user-event";
import { DEFAULT_numberOfGuessesToEnd } from "@/constants";

const shareModule = require("../../src/share");

describe("ShareInvitation", function () {
  beforeEach(() => {
    window.alert = jest.fn();
  });

  describe.each([true, false])("", (won) =>
    describe.each([0, 10])("", (hintsUsed) =>
      it.each([DEFAULT_numberOfGuessesToEnd, 11])(
        `should share correct data on click (won: ${won}, hintsUsed: ${hintsUsed}, numberOfGuessesToEnd: %d)`,
        async (numberOfGuessesToEnd) => {
          const shareStub = jest.spyOn(shareModule, "share");

          const puzzleNumberForDisplay = 2;

          render(
            <ShareInvitation
              won={won}
              puzzleNumberForDisplay={puzzleNumberForDisplay}
              guesses={[]}
              hintsUsed={hintsUsed}
              numberOfGuessesToEnd={numberOfGuessesToEnd}
            />
          );

          await userEvent.click(screen.getByText("Teile deinen Erfolg"));

          expect(shareStub).toHaveBeenCalledWith(
            puzzleNumberForDisplay,
            won,
            [],
            hintsUsed,
            numberOfGuessesToEnd
          );
        }
      )
    )
  );
});
