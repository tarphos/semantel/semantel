import { encodeBase64 } from "@/base64";
import { top1kUrl } from "@/urls";
import Link from "next/link";
import React, { ReactNode, memo, useMemo } from "react";

type Top1kLinkProps = {
  children: ReactNode;
  word: string;
};

const Top1kLink = memo(function Top1kLink(props: Readonly<Top1kLinkProps>) {
  const top1k = useMemo(
    () => {
      const secretBase64 = encodeURIComponent(encodeBase64(props.word));
      return top1kUrl(secretBase64);
    },
    // Stryker disable next-line ArrayDeclaration
    [props.word]
  );

  return <Link href={top1k}>{props.children}</Link>;
});

export default Top1kLink;
