/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { DEFAULT_numberOfGuessesToEnd } from "@/constants";
import { render, screen } from "@testing-library/react";
import ResponseContentWon from "./ResponseContentWon";
import { GuessesEntry } from "@/types";

describe("ResponseContentWon", function () {
  it.each([DEFAULT_numberOfGuessesToEnd, 1, 2])(
    "should print correct message if won (numberOfGuessesToEnd: %d)",
    (numberOfGuessesToEnd) => {
      const numberOfGuesses = 3;
      const secret = "secret";
      render(
        <ResponseContentWon
          guesses={guessesArray(numberOfGuesses)}
          secret={secret}
          numberOfGuessesToEnd={numberOfGuessesToEnd}
        />
      );

      expect(
        screen.getByTestId("won" /* TODO do we need testId here? */)
      ).toHaveTextContent(
        `\
Du hast es mit ${
          numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
            ? numberOfGuesses
            : numberOfGuessesToEnd
        } Versuch${numberOfGuessesToEnd === 1 ? "" : "en"} gefunden! \
Das Lösungswort ist ${secret}. \
Gib ruhig weitere Wörter ein, wenn du neugierig bist, wie die Ähnlichkeiten aussehen.`
      );
    }
  );
});

function guessesArray(numberOfGuesses?: number): GuessesEntry[] {
  const guesses: GuessesEntry[] = [];
  if (numberOfGuesses !== undefined) {
    for (let i = 0; i < numberOfGuesses; i++) {
      guesses.push({ index: i, word: `${i}`, similarity: i });
    }
  }
  return guesses;
}
