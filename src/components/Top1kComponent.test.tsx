/**
 * @jest-environment jsdom
 */

import Top1kComponent from "@/components/Top1kComponent";
import { mockComponent } from "@/testutils/mockComponent";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

jest.mock("fetch-retry"); // can't import data module otherwise
const nearestTableModule = require("@/components/NearestTable");

const otherBase64 = "c2Nob2tvbGFkZQ%3D%3D";

const secrets = [
  "word1",
  "word2",
  "word3",
  "word4",
  "word5",
  "word6",
  "word7",
  "word8",
  "word9",
];

describe("Top1k component", function () {
  let testIdNearestTable: string;

  let mockedNearestTable: jest.Mock;

  beforeAll(() => {
    ({ testId: testIdNearestTable, mockedComponent: mockedNearestTable } =
      mockComponent(nearestTableModule));
  });
  beforeEach(() => {
    global.Date.now = jest.fn(() => new Date("2023-04-27T10:20:30Z").getTime());
  });

  it("should show nearest table if word is not secret", () => {
    Object.defineProperty(window, "location", {
      writable: true,
      value: { search: `?w=${otherBase64}` },
    });
    render(<Top1kComponent secrets={secrets} />);

    expect(screen.getByTestId(testIdNearestTable)).toBeVisible();
    expect(mockedNearestTable).toHaveBeenCalledWith({
      word: "schokolade",
    });
  });

  it("should not show nearest words table when parameter is missing but only error message", () => {
    Object.defineProperty(window, "location", {
      writable: true,
      value: { search: "" },
    });
    render(<Top1kComponent secrets={secrets} />);

    expect(screen.queryByTestId(testIdNearestTable)).not.toBeInTheDocument();
    expect(mockedNearestTable).not.toHaveBeenCalled();

    expect(screen.queryByText(/^Kein Wort angegeben/)).toBeVisible();
  });
});
