/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Stats } from "@/types";
import ResponseContentStatistics from "./ResponseContentStatistics";

describe("ResponseContentStatistics", function () {
  it("should print correct statistics", () => {
    const stats: Stats = {
      abandons: 2,
      firstPlay: 3,
      giveups: 5,
      hints: 7,
      lastEnd: 11,
      lastPlay: 13,
      playStreak: 17,
      totalGuesses: 23,
      totalPlays: 29,
      wins: 31,
      winStreak: 37,
    };

    render(<ResponseContentStatistics stats={stats} />);

    const totalGames = stats.wins + stats.giveups + stats.abandons;
    const avgGuesses = (stats.totalGuesses / totalGames)
      .toFixed(2)
      .replace(".", ",");

    expect(screen.getByTestId("responseStatistics")).toHaveTextContent(
      `Erstes Spiel:${stats.firstPlay}` +
        `Gespielte Tage:${totalGames}` +
        `Siege:${stats.wins}` +
        `Siegesserie:${stats.winStreak}` +
        `Aufgegeben:${stats.giveups}` +
        `Nicht beendet:${stats.abandons}` +
        `Gesamtzahl an Tipps:${stats.totalGuesses}` +
        `Durchschnittliche Zahl von Tipps:${avgGuesses}` +
        `Benutzte Hinweise:${stats.hints}`
    );
  });
});
