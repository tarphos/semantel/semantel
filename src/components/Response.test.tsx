/**
 * @jest-environment jsdom
 */

import { DEFAULT_numberOfGuessesToEnd } from "@/constants";
import { mockComponent } from "@/testutils/mockComponent";
import { GuessesEntry, Stats } from "@/types";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Response from "./Response";
import userEvent from "@testing-library/user-event";

const responseContentWonDialogModule = require("@/components/ResponseContentWon");
const responseContentGivenUpDialogModule = require("@/components/ResponseContentGivenUp");
const top1kAndShareInvitationModule = require("@/components/Top1kAndShareInvitation");

describe("Response", function () {
  let testIdResponseContentWon: string;

  let mockedResponseContentWon: jest.Mock;

  let testIdResponseContentGivenUp: string;

  let mockedResponseContentGivenUp: jest.Mock;

  let testIdShareInvitation: string;

  let mockedShareInvitation: jest.Mock;

  let testIdTop1kAndShareInvitation: string;

  let mockedTop1kAndShareInvitation: jest.Mock;

  let testIdResponseContentStatistics: string;

  let mockedResponseContentStatistics: jest.Mock;

  beforeAll(() => {
    ({
      testId: testIdResponseContentWon,
      mockedComponent: mockedResponseContentWon,
    } = mockComponent(responseContentWonDialogModule));

    ({
      testId: testIdResponseContentGivenUp,
      mockedComponent: mockedResponseContentGivenUp,
    } = mockComponent(responseContentGivenUpDialogModule));

    ({ testId: testIdShareInvitation, mockedComponent: mockedShareInvitation } =
      mockComponent(require("@/components/ShareInvitation")));

    ({
      testId: testIdTop1kAndShareInvitation,
      mockedComponent: mockedTop1kAndShareInvitation,
    } = mockComponent(top1kAndShareInvitationModule));

    ({
      testId: testIdResponseContentStatistics,
      mockedComponent: mockedResponseContentStatistics,
    } = mockComponent(require("@/components/ResponseContentStatistics")));
  });

  beforeEach(() => {
    window.prompt = jest.fn();
    window.alert = jest.fn();
  });

  afterEach(() => {
    window.localStorage.clear();
  });

  it.each([DEFAULT_numberOfGuessesToEnd, 1, 2])(
    "should render correct component if won (numberOfGuessesToEnd: %d)",
    (numberOfGuessesToEnd) => {
      const numberOfGuesses = 3;
      const secret = "secret";
      render(
        <ResponseContainer
          won={true}
          numberOfGuesses={numberOfGuesses}
          secret={secret}
          numberOfGuessesToEnd={numberOfGuessesToEnd}
        />
      );

      expect(screen.getByTestId(testIdResponseContentWon)).toBeVisible();
      expect(mockedResponseContentWon).toHaveBeenCalledWith({
        guesses: guessesArray(numberOfGuesses),
        secret,
        numberOfGuessesToEnd,
      });

      expect(
        screen.queryByTestId(testIdResponseContentGivenUp)
      ).not.toBeInTheDocument();
    }
  );

  it("should render correct component if given up", () => {
    const secret = "secret";
    render(<ResponseContainer won={false} secret={secret} />);

    expect(screen.getByTestId(testIdResponseContentGivenUp)).toBeVisible();
    expect(mockedResponseContentGivenUp).toHaveBeenCalledWith({ secret });

    expect(
      screen.queryByTestId(testIdResponseContentWon)
    ).not.toBeInTheDocument();
  });

  describe.each([0, 123])("with %i hints used:", (hintsUsed) => {
    it.each([true, false])("shoud render share component (won: %p)", (won) => {
      const secret = "secret";
      const puzzleNumberForDisplay = 3;
      const numberOfGuesses = 5;
      const numberOfGuessesToEnd = 7;
      render(
        <ResponseContainer
          won={won}
          secret={secret}
          puzzleNumberForDisplay={puzzleNumberForDisplay}
          numberOfGuesses={numberOfGuesses}
          numberOfGuessesToEnd={numberOfGuessesToEnd}
          hintsUsed={hintsUsed}
        />
      );

      expect(screen.getByTestId(testIdShareInvitation)).toBeVisible();
      expect(mockedShareInvitation).toHaveBeenCalledWith({
        won,
        puzzleNumberForDisplay,
        guesses: guessesArray(numberOfGuesses),
        hintsUsed,
        numberOfGuessesToEnd,
      });
    });
  });

  it.each([true, false])("should render top1k invitation correctly", (won) => {
    render(<ResponseContainer won={won} />);

    expect(screen.getByTestId(testIdTop1kAndShareInvitation)).toBeVisible();
    expect(mockedTop1kAndShareInvitation).toHaveBeenCalledWith({
      secret: "hund",
      openDonateDialog: expect.any(Function),
    });
  });

  it.each([true, false])(
    "should print correct statistics (won: %p)",
    (won: boolean) => {
      const stats: Stats = {
        abandons: 2,
        firstPlay: 3,
        giveups: 5,
        hints: 7,
        lastEnd: 11,
        lastPlay: 13,
        playStreak: 17,
        totalGuesses: 23,
        totalPlays: 29,
        wins: 31,
        winStreak: 37,
      };

      render(<ResponseContainer won={won} numberOfGuesses={7} stats={stats} />);

      expect(screen.getByTestId(testIdResponseContentStatistics)).toBeVisible();
      expect(mockedResponseContentStatistics).toHaveBeenCalledWith({ stats });
    }
  );

  it("should collapse response on click", async () => {
    render(<ResponseContainer won={true} />);

    await userEvent.click(screen.getByRole("button"));

    const elementsToBeHidden = [
      testIdShareInvitation,
      testIdTop1kAndShareInvitation,
      testIdResponseContentStatistics,
    ];

    for (const element of elementsToBeHidden) {
      expect(screen.queryByTestId(element)).not.toBeInTheDocument();
    }

    expect(screen.getByTestId(testIdResponseContentWon)).toBeVisible();

    await userEvent.click(screen.getByRole("button"));

    for (const element of elementsToBeHidden) {
      expect(screen.getByTestId(element)).toBeVisible();
    }

    expect(screen.getByTestId(testIdResponseContentWon)).toBeVisible();
  });
});

const ResponseContainer = (props: {
  won: boolean;
  numberOfGuesses?: number;
  secret?: string;
  puzzleNumberForDisplay?: number;
  stats?: Stats;
  numberOfGuessesToEnd?: number;
  hintsUsed?: number;
}) => {
  const hintsUsed = props.hintsUsed ?? 0;

  const stats: Stats = props.stats ?? {
    abandons: 12,
    firstPlay: 13,
    giveups: 15,
    hints: 17,
    lastEnd: 111,
    lastPlay: 113,
    playStreak: 117,
    totalGuesses: 123,
    totalPlays: 129,
    wins: 131,
    winStreak: 137,
  };

  return (
    <Response
      won={props.won}
      guesses={guessesArray(props.numberOfGuesses)}
      hintsUsed={hintsUsed}
      puzzleNumberForDisplay={props.puzzleNumberForDisplay ?? 2}
      secret={props.secret ?? "hund"}
      stats={stats}
      numberOfGuessesToEnd={
        props.numberOfGuessesToEnd ?? DEFAULT_numberOfGuessesToEnd
      }
      openDonateDialog={() => {}}
    />
  );
};

function guessesArray(numberOfGuesses?: number): GuessesEntry[] {
  const guesses: GuessesEntry[] = [];
  if (numberOfGuesses !== undefined) {
    for (let i = 0; i < numberOfGuesses; i++) {
      guesses.push({ index: i, word: `${i}`, similarity: i });
    }
  }
  return guesses;
}
