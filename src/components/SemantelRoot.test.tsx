/**
 * @jest-environment jsdom
 * @jest-environment-options {"url": "https://semantel.tarphos.de/"}
 */

import { SemantelProps } from "@/components/Semantel";
import SemantelRoot from "@/components/SemantelRoot";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { mockComponent } from "../testutils/mockComponent";

jest.mock("fetch-retry"); // can't import data module otherwise
const semantelDialogModule = require("@/components/Semantel");

describe("SemantelRoot", function () {
  let testIdSemantel: string;

  let mockedSemantel: jest.Mock;

  beforeAll(() => {
    ({ testId: testIdSemantel, mockedComponent: mockedSemantel } =
      mockComponent(semantelDialogModule));
  });

  afterEach(() => {
    window.location.hash = "";
  });
  const secrets = [
    "word1",
    "word2",
    "word3",
    "word4",
    "word5",
    "word6",
    "word7",
    "word8",
    "word9",
  ];

  it("should display Semantel", () => {
    render(
      <SemantelRoot
        today={7}
        puzzleNumber={13}
        secrets={secrets}
        openNewPuzzle={() => {}}
      />
    );

    expect(screen.getByTestId(testIdSemantel)).toBeVisible();
    expect(mockedSemantel).toHaveBeenCalledWith<Array<SemantelProps>>({
      today: 7,
      puzzleNumberForStorage: 13,
      secrets,
    });
  });

  it("should display message for new puzzle", async () => {
    const openNewPuzzle = jest.fn();

    const { rerender } = render(
      <SemantelRoot
        today={7}
        puzzleNumber={13}
        secrets={secrets}
        openNewPuzzle={openNewPuzzle}
      />
    );

    expect(screen.queryByText(/Das neue Semantel/i)).not.toBeInTheDocument();
    expect(openNewPuzzle).not.toHaveBeenCalled();

    rerender(
      <SemantelRoot
        today={7}
        puzzleNumber={13}
        secrets={secrets}
        openNewPuzzle={openNewPuzzle}
        newPuzzleNumber={14}
      />
    );

    expect(
      screen.getByText(`Das neue Semantel (Nummer 14) ist da!`)
    ).toBeVisible();
    expect(openNewPuzzle).not.toHaveBeenCalled();

    await userEvent.click(screen.getByText("Zum neuen Rätsel"));

    expect(openNewPuzzle).toHaveBeenCalledTimes(1);
  });
});
