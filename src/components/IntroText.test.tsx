/**
 * @jest-environment jsdom
 */

import IntroText from "@/components/IntroText";
import useDialogState from "@/useDialogState";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const defaultDialogState: ReturnType<typeof useDialogState> = [
  false,
  () => {},
  () => {},
];

describe("IntroText", function () {
  it("should render support links", () => {
    render(<IntroText openDonate={() => {}} />);
    expect(screen.getByRole("link")).toHaveTextContent(
      "Erfahre hier, wie du das auch tun kannst."
    );
  });

  it("should open dialog on click", async () => {
    const openDialog = jest.fn();
    render(<IntroText openDonate={openDialog} />);
    expect(openDialog).not.toHaveBeenCalled();
    await userEvent.click(screen.getByRole("link"));
    expect(openDialog).toHaveBeenCalled();
  });
});
