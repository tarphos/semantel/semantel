/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import CornerActionButton, {
  CornerActionButtonProps,
} from "./CornerActionButton";

describe("CornerActionButton", function () {
  describe.each(["close", "collapse", "expand"] as Array<
    CornerActionButtonProps["type"]
  >)('with type "%s"', (type) => {
    it("should call callback when clicked", async () => {
      const onClick = jest.fn();
      render(<CornerActionButton close={onClick} id="someId" type={type} />);

      expect(onClick).not.toHaveBeenCalled();

      await userEvent.click(screen.getByRole("button"));

      expect(onClick).toHaveBeenCalled();
    });

    it("should have correct aria label", async () => {
      const onClick = jest.fn();
      render(<CornerActionButton close={onClick} id="someId" type={type} />);

      expect(screen.getByRole("button")).toHaveAttribute(
        "aria-label",
        typeToExpectedAriaLabel(type)
      );
    });
  });
});

function typeToExpectedAriaLabel(
  type: CornerActionButtonProps["type"]
): string {
  switch (type) {
    case "close":
      return "schließen";
    case "collapse":
      return "verkleinern";
    case "expand":
      return "vergrößern";
  }
}
