import { getNearby } from "@/data";
import React, { memo, useCallback, useEffect, useState } from "react";
import { getLocalTimeString } from "../timeUtils";
import AccordionElement from "./AccordionElement";
import Dialog from "./Dialog";
import Top1kLink from "./Top1kLink";
import withLoadingSpinnerAndFallback from "@/withLoadingSpinnerAndFallback";

export type FaqDialogProps = {
  yesterday: string;
  today: number;
  open: boolean;
  close: () => void;
  openDonateDialog: () => void;
  secrets: Array<string>;
};

enum FaqEntry {
  werbung,
  wortVonGestern,
  neuesWort,
  similarity,
  multiple,
  idee,
  wie,
  datensatz,
  nearbyYesterday,
  questionMarks,
  quellcode,
  sort,
  compare,
  sprachen,
  wortDesTages,
  hinweise,
  support,
  fehler,
  datenschutz,
}

const faqHeading = "faq-heading";

const FaqDialog = memo(function FaqDialog(props: Readonly<FaqDialogProps>) {
  const [faqEntriesState, setFaqEntriesState] = useState(new Set<FaqEntry>());
  const [yesterdayText, setYesterdayText] = useState<React.ReactNode>();

  const activeHandlingProps: (faqEntry: FaqEntry) => {
    active: boolean;
    toggleActive: () => void;
  } = useCallback(
    (faqEntry) => {
      const active = faqEntriesState.has(faqEntry);
      return {
        active,
        toggleActive: () => {
          setFaqEntriesState((oldState) => {
            const newState = new Set(oldState);
            if (active) {
              newState.delete(faqEntry);
            } else {
              newState.add(faqEntry);
            }
            return newState;
          });
        },
      };
    },
    [faqEntriesState]
  );

  useEffect(() => {
    const updateYesterdayText = async () => {
      const yesterdayNearby = await getNearby(props.yesterday);
      setYesterdayText(
        <>
          {yesterdayNearby.join(", ")}, nach Nähe absteigend.{" "}
          <Top1kLink word={props.yesterday}>Mehr?</Top1kLink>
        </>
      );
    };
    if (
      yesterdayText === undefined &&
      activeHandlingProps(FaqEntry.nearbyYesterday).active
    ) {
      updateYesterdayText().catch(console.error);
    }
  }, [activeHandlingProps, props.yesterday, yesterdayText]);

  const pastWeek: Array<string> = [];
  for (let i = props.secrets.length - 3; i >= 0; i--) {
    pastWeek.push(`„${props.secrets[i]}“`);
  }

  const switchToDonateDialog = useCallback(() => {
    props.openDonateDialog();
    props.close();
  }, [props]);

  return (
    props.open && (
      <Dialog
        underlayId="faq-underlay"
        ariaLabelledby={faqHeading}
        closeButtonId="faq-close"
        close={props.close}
      >
        <h3 id={faqHeading}>FAQ</h3>

        <AccordionElement
          title="Was war das Wort von gestern?"
          {...activeHandlingProps(FaqEntry.wortVonGestern)}
        >
          <p data-testid="yesterdaysWord">
            Gestern war das Lösungswort{" "}
            {`„${props.yesterday}“ Die Wörter davor waren: ${pastWeek.join(
              ", "
            )}`}
            .
          </p>
        </AccordionElement>

        <AccordionElement
          title="Wann kommt das neue Wort raus?"
          {...activeHandlingProps(FaqEntry.neuesWort)}
        >
          <p>Mitternacht nach GMT {getLocalTimeString()}</p>
        </AccordionElement>

        <AccordionElement
          title="Wieso ist die Ähnlichkeit so anders, als ich erwartet hätte?"
          {...activeHandlingProps(FaqEntry.similarity)}
        >
          <p>Da könnte es verschiedene Gründe geben. </p>
          <ol>
            <li>
              Dein Tipp oder das Lösungswort haben mehrere Bedeutungen, und die
              ähnliche Bedeutung wird nicht so oft benutzt. Manchmal kommt eine
              Bedeutung einfach öfter vor (in der deutschen Wikipedia, die als
              Textgrundlage verwendet wurde).
            </li>
            <li>
              Dein Wort und das Lösungswort sind verschiedene Wortarten.
              Manchmal macht das viel aus. Manchmal nur wenig.
            </li>
            <li>
              Mit „Ähnlichkeit“ ist eigentlich gemeint, dass das Wort „in
              ähnlichen Kontexten verwendet“ wird. Auch Gegenteile können in
              diesem Sinn semantisch ähnlich sein.
            </li>
          </ol>
          <p>
            Der zugrunde liegende Datensatz ist nicht perfekt. Aber ich arbeite
            daran. :-D
          </p>
        </AccordionElement>

        <AccordionElement
          title="Kann ich mehrere Wörter pro Tag spielen? Kann ich alte Wörter nochmal
        spielen?"
          {...activeHandlingProps(FaqEntry.multiple)}
        >
          <p>
            Nope. Was gibt es schöneres als morgens aufzuwachen und sich auf das
            neue Wort zu freuen?
          </p>
        </AccordionElement>

        <AccordionElement
          title="Wie bist du auf die Idee hierzu gekommen?"
          {...activeHandlingProps(FaqEntry.idee)}
        >
          <p>
            Gar nicht. Ich habe nur{" "}
            <a href="https://novalis.org/">David Turners</a> Spiel (das
            inzwischen verkauft wurde und auf{" "}
            <a href="https://semantle.com/">semantle.com</a> umgezogen ist) fürs
            Deutsche adaptiert.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Wie funktioniert das?"
          {...activeHandlingProps(FaqEntry.wie)}
        >
          <p>
            <a href="https://towardsdatascience.com/word2vec-explained-49c52b4ccb71">
              Hier ist ein Artikel über Word2Vec, was dem ganzen zugrunde liegt
            </a>
            {/* */}.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Welchen Datensatz hast du benutzt?"
          {...activeHandlingProps(FaqEntry.datensatz)}
        >
          <p>
            Ich habe mit den Texten der deutschen Wikipedia (Stand 20. März
            2022) ein Word2Vec-Modell trainiert.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Welche Wörter waren dem gestrigen Wort am ähnlichsten?"
          {...activeHandlingProps(FaqEntry.nearbyYesterday)}
        >
          {withLoadingSpinnerAndFallback<{ yesterdayText?: React.ReactNode }>(
            ({ yesterdayText }) => (
              <p>
                <span data-testid="yesterdayText">{yesterdayText}</span>
              </p>
            ),
            { yesterdayText }
          )}
        </AccordionElement>

        <AccordionElement
          title="Was bedeutet „????“?"
          {...activeHandlingProps(FaqEntry.questionMarks)}
        >
          Dieses Wort ist nicht in der Wortliste von gebräuchlichen Wörtern, die
          wir für die Top-1000-Liste hernehmen, aber es ist dem Lösungswort
          trotzdem sehr ähnlich.
        </AccordionElement>

        <AccordionElement
          title="Quellcode?"
          {...activeHandlingProps(FaqEntry.quellcode)}
        >
          <p>
            <a href="https://gitlab.com/tarphos/semantel/semantel">Ja!</a>
          </p>
        </AccordionElement>

        <AccordionElement
          title="Kann ich meine Tipps chronologisch sortieren?"
          {...activeHandlingProps(FaqEntry.sort)}
        >
          <p>
            Ja: Klick einfach auf die Zeilenüberschrift „#“. Klick nochmal
            drauf, um die Reihenfolge umzukehren. Wenn du einen neuen Tipp
            abgibst, wird wieder nach Ähnlichkeit sortiert. Ebenso wenn du auf
            die „Ähnlichkeit“-Überschrift klickst.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Wie gut bin ich im Vergleich zu anderen Spielern? Was ist die durchschnittliche Anzahl von Tipps?"
          {...activeHandlingProps(FaqEntry.compare)}
        >
          <p>Es werden keine Daten dafür gesammelt.</p>
        </AccordionElement>

        <AccordionElement
          title="Kann ich in anderen Sprachen spielen?"
          {...activeHandlingProps(FaqEntry.sprachen)}
        >
          <p>
            Ja, <a href="https://semantle.com/">Englisch</a>,{" "}
            <a href="https://swemantle.riddle.nu/">Schwedisch</a>,{" "}
            <a href="https://semantle-he.herokuapp.com">Hebräisch</a>,{" "}
            <a href="http://semantle-es.cgk.cl/">Spanisch</a>,{" "}
            <a href="https://contexto.me/">Portugiesisch</a>,{" "}
            <a href="https://cemantix.herokuapp.com/">Französisch</a>,{" "}
            <a href="https://semantle.ozanalpay.com/">Türkisch</a>,{" "}
            <a href="https://kcl.somecrap.ru/semantle.today/">Russisch</a>,{" "}
            <a href="https://semantle.be/">Niederländisch/Flämisch</a>, und{" "}
            <a href="https://semantle-ko.newsjel.ly/">Koreanisch</a>, bisher.{" "}
          </p>
        </AccordionElement>

        <AccordionElement
          title="Wie bestimmst du das Wort des Tages?"
          {...activeHandlingProps(FaqEntry.wortDesTages)}
        >
          <p>
            Ich habe irgendeine Liste der häufigsten deutschen Wörter genommen,
            seltsame Wörter und Eigennamen herausgenommen, die ersten ca. 1000
            genommen und in eine zufällige Reihenfolge gebracht.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Hinweise?"
          {...activeHandlingProps(FaqEntry.hinweise)}
        >
          <p>
            Ja: Klick auf den „Hinweis“-Knopf unter der Liste deiner Tipps
            (links vom „Aufgeben“-Knopf). Dann bekommst du ein Wort, das ein
            bisschen besser als deine bisherigen Tipps ist.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Unterstützen?"
          {...activeHandlingProps(FaqEntry.support)}
        >
          <p>
            Der Betrieb dieser Website kostet Geld. Die Entwicklung kostet Zeit.{" "}
            <a href="#" onClick={switchToDonateDialog}>
              Erfahre hier, wie du uns finanziell unterstützen kannst,
            </a>{" "}
            wenn du magst.
          </p>
        </AccordionElement>

        <AccordionElement
          title="Ich habe einen Fehler gefunden oder einen Verbesserungsvorschlag. Was soll ich tun?"
          {...activeHandlingProps(FaqEntry.fehler)}
        >
          <p>
            <a href="mailto:contact-project+tarphos-semantel-semantel-34604831-issue-@incoming.gitlab.com">
              Schreib mir am besten eine Mail
            </a>
            oder{" "}
            <a href="https://gitlab.com/tarphos/semantel/semantel/-/issues">
              erstelle ein Issue auf GitLab
            </a>
          </p>
        </AccordionElement>
      </Dialog>
    )
  );
});

export default FaqDialog;
