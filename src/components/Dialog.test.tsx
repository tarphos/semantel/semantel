/**
 * @jest-environment jsdom
 */

import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import assert from "assert";
import React, { useState } from "react";
import Dialog, { DialogProps } from "./Dialog";
import "@testing-library/jest-dom";

describe("Dialog", function () {
  it("should show dialog", () => {
    render(<Dialog {...defaultDialogPros}>foo</Dialog>);

    assert.ok(screen.queryByText("foo") !== null);
    expect(screen.queryByRole("dialog")).toBeVisible();
  });

  it("should focus dialog", () => {
    const mockClose = jest.fn;
    render(
      <Dialog {...defaultDialogPros} close={mockClose}>
        foo
      </Dialog>
    );

    expect(screen.getByRole("dialog")).toHaveFocus();
  });

  it("should close dialog when escape key is pressed", async () => {
    const mockClose = jest.fn();
    render(
      <Dialog {...defaultDialogPros} close={mockClose}>
        foo
      </Dialog>
    );

    await act(() => userEvent.keyboard("[Escape]"));

    expect(mockClose).toHaveBeenCalled();
  });

  it("should not close dialog when space key is pressed", async () => {
    const mockClose = jest.fn();
    render(
      <Dialog {...defaultDialogPros} close={mockClose}>
        foo
      </Dialog>
    );

    await act(() => userEvent.keyboard("[Space]"));

    expect(mockClose).not.toHaveBeenCalled();
  });

  it("should not close dialog when clicked in dialog", async () => {
    const mockClose = jest.fn();
    render(
      <Dialog {...defaultDialogPros} close={mockClose}>
        foo
      </Dialog>
    );

    await act(() => userEvent.click(screen.getByText("foo")));

    expect(mockClose).not.toHaveBeenCalled();
  });

  it("should set and unset diaog-open class on body", async () => {
    const { unmount } = render(<Dialog {...defaultDialogPros}>foo</Dialog>);

    await act(() => userEvent.click(screen.getByText("foo")));

    expect(document.body.classList).toContain("dialog-open");

    unmount();

    expect(document.body.classList).not.toContain("dialog-open");
  });
});

const defaultDialogPros: DialogProps = {
  ariaLabelledby: "",
  closeButtonId: "",
  underlayId: "",
  children: "foo",
  close: () => {},
};
