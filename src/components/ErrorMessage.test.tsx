/**
 * @jest-environment jsdom
 */

import { render, screen } from "@testing-library/react";
import assert from "assert";
import React from "react";
import ErrorMessage from "@/components/ErrorMessage";

describe("ErrorMessage", function () {
  it("should show message", () => {
    const message = "my message";
    render(<ErrorMessage message={message} />);

    assert.ok(screen.queryByText(message) !== null);
  });
});
