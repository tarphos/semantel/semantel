/**
 * @jest-environment jsdom
 */

import HintGiveUp from "@/components/HintGiveUp";
import { EMPTY_STATS } from "@/constants";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import assert from "assert";

jest.mock("fetch-retry"); // can't import data module otherwise
const dataModule = require("@/data");
const utilsModule = require("@/utils");

describe("HintGiveUp", function () {
  afterEach(() => {
    window.localStorage.clear();
  });

  describe("hint", function () {
    beforeEach(() => {
      jest.spyOn(dataModule, "hint");
    });

    it("should show confirm dialog and don't show hint if aborted", async () => {
      mockConfirm(false);

      render(<HintGiveUpContainer />);

      await userEvent.click(screen.getByText("Hinweis"));

      assert.equal(
        (global.confirm as jest.Mock).mock.calls[0],
        "Willst du wirklich einen Hinweis?"
      );
      expect(dataModule.hint).not.toHaveBeenCalled();
    });

    it("should show confirm dialog and show hint if confirmed", async () => {
      mockConfirm(true);

      render(<HintGiveUpContainer />);

      await userEvent.click(screen.getByText("Hinweis"));

      assert.equal(
        (global.confirm as jest.Mock).mock.calls[0],
        "Willst du wirklich einen Hinweis?"
      );
      expect(dataModule.hint).toHaveBeenCalled();
    });
  });

  describe("give up", function () {
    beforeEach(() => {
      jest.spyOn(utilsModule, "endGame");
    });

    it("should show abort button if game is still running", async () => {
      render(<HintGiveUpContainer />);

      expect(screen.getByText("Aufgeben")).toBeInTheDocument();
    });

    it("should not show abort button if game is over", async () => {
      render(<HintGiveUpContainer winState={0} />);

      expect(screen.queryByText("Aufgeben")).not.toBeInTheDocument();
    });

    it("should end game when clicked and confirmed", async () => {
      mockConfirm(true);

      render(<HintGiveUpContainer />);

      await userEvent.click(screen.getByText("Aufgeben"));

      expect(utilsModule.endGame).toHaveBeenCalled();
    });

    it("should not end game when clicked but canceled confirmed", async () => {
      mockConfirm(false);

      render(<HintGiveUpContainer />);

      await userEvent.click(screen.getByText("Aufgeben"));

      expect(utilsModule.endGame).not.toHaveBeenCalled();
    });
  });
});

const HintGiveUpContainer = (props: { winState?: number }) => {
  return (
    <HintGiveUp
      guesses={{ current: [] }}
      setGuesses={() => {}}
      hintsUsed={0}
      setHintsUsed={() => {}}
      secret=""
      winState={props.winState ?? -1}
      setWinState={() => {}}
      puzzleKey={1}
      setChronoForward={() => {}}
      setError={() => {}}
      setLatestGuess={() => {}}
      secretVec={[]}
      puzzleNumberFromStorage={1}
      stats={{ current: EMPTY_STATS }}
      setStats={() => {}}
      setNumberOfGuessesToEnd={() => {}}
    />
  );
};
function mockConfirm(result: boolean) {
  global.confirm = jest.fn(() => result);
}
