/**
 * @jest-environment jsdom
 */

import Settings from "@/components/Settings";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Dispatch } from "react";

describe("Settings", function () {
  it("should display settings", () => {
    render(<SettingsContainer open={true} />);

    expect(screen.getByRole("heading")).toHaveTextContent("Einstellungen");
    expect(screen.getByLabelText("Dunkler Modus")).toBeVisible();
  });

  it("should not display settings", () => {
    render(<SettingsContainer open={false} />);

    expect(screen.queryByRole("heading")).not.toBeInTheDocument();
  });

  it("should set and unset dark mode", async () => {
    const setDarkMode = jest.fn();
    render(<SettingsContainer open={true} setDarkMode={setDarkMode} />);

    await userEvent.click(screen.getByLabelText("Dunkler Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(true);

    await userEvent.click(screen.getByLabelText("Heller Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(false);
  });

  it("should set and unset dark mode with preset dark mode", async () => {
    const setDarkMode = jest.fn();
    render(
      <SettingsContainer
        open={true}
        setDarkMode={setDarkMode}
        darkMode={true}
      />
    );

    await userEvent.click(screen.getByLabelText("Heller Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(false);

    await userEvent.click(screen.getByLabelText("Dunkler Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(true);
  });

  it("should set and unset dark mode correctly after external darkMode change", async () => {
    const setDarkMode = jest.fn();
    const { rerender } = render(
      <SettingsContainer open={true} setDarkMode={setDarkMode} />
    );

    await userEvent.click(screen.getByLabelText("Dunkler Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(true);

    await userEvent.click(screen.getByLabelText("Heller Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(false);

    rerender(
      <SettingsContainer
        open={true}
        setDarkMode={setDarkMode}
        darkMode={true}
      />
    );

    await userEvent.click(screen.getByLabelText("Heller Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(false);

    await userEvent.click(screen.getByLabelText("Dunkler Modus"));

    expect(setDarkMode).toHaveBeenCalledWith(true);
  });

  test.each([[true], [undefined], [false]])(
    "correct radio should be checked when darkMode is %p",
    async (darkMode?: boolean) => {
      render(<SettingsContainer open={true} darkMode={darkMode} />);

      const [checked, unchecked1, unchecked2] = getLabels(darkMode);

      expect(screen.getByLabelText(checked)).toBeChecked();
      expect(screen.getByLabelText(unchecked1)).not.toBeChecked();
      expect(screen.getByLabelText(unchecked2)).not.toBeChecked();
    }
  );
});

const SettingsContainer = (props: {
  open: boolean;
  setDarkMode?: Dispatch<boolean | undefined>;
  darkMode?: boolean;
}) => {
  return (
    <Settings
      close={() => {}}
      darkMode={props.darkMode}
      open={props.open}
      setDarkMode={props.setDarkMode ?? (() => {})}
    />
  );
};
function getLabels(darkMode: boolean | undefined) {
  if (darkMode === undefined) {
    return ["wie System", "Heller Modus", "Dunkler Modus"];
  } else if (darkMode) {
    return ["Dunkler Modus", "wie System", "Heller Modus"];
  } else {
    return ["Heller Modus", "Dunkler Modus", "wie System"];
  }
}
