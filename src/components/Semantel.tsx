"use client";

import {
  DEFAULT_GUESSES,
  DEFAULT_WIN_STATE,
  DEFAULT_numberOfGuessesToEnd,
  EMPTY_STATS,
} from "@/constants";
import {
  storageKey_guesses,
  storageKey_hints_used,
  storageKey_numberOfGuessesToEnd,
  storageKey_puzzleNumber,
  storageKey_stats,
  storageKey_winState,
} from "@/constants/localStorageKeys";
import {
  mapGuessesToNestedArrayForStorage,
  mapNestedArrayFromStorageToGuesses,
} from "@/mapGuesses";
import useDarkMode from "@/useDarkMode";
import useDialogState from "@/useDialogState";
import {
  usePersistedState,
  usePersistedStateWithRef,
} from "@/usePersistedState";
import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { getModel, getSimilarityStory } from "../data";
import { getUpdatedStats } from "../getUpdatedStats";
import {
  GuessesEntry,
  GuessesEntryFromStorage,
  SimilarityStoryType,
  Stats,
} from "../types";
import { clearStorage } from "../utils";
import DonateDialog from "./DonateDialog";
import ErrorMessage from "./ErrorMessage";
import GuessForm, { GuessFormProps } from "./GuessForm";
import Guesses, { GuessesProps } from "./Guesses";
import Header, { HeaderProps } from "./Header";
import HintGiveUp, { HintGiveUpProps } from "./HintGiveUp";
import IntroText from "./IntroText";
import LoadingSpinnerOverlay from "./LoadingSpinnerOverlay";
import Response, { ResponseProps } from "./Response";
import SimilarityStory, { SimilarityStoryProps } from "./SimilarityStory";

export type SemantelProps = {
  today: number;
  puzzleNumberForStorage: number;
  secrets: Array<string>;
};

const Semantel = memo(function Semantel(props: Readonly<SemantelProps>) {
  const [error, setError] = useState("");
  const [chronoForward, setChronoForward] = useState(1);
  const [latestGuess, setLatestGuess] = useState<string>();
  const [secretVec, setSecretVec] = useState<Array<number>>();
  const [similarityStory, setSimilarityStory] = useState<SimilarityStoryType>();
  const [loadingSpinnerActive, setLoadingSpinnerActive] = useState(false);

  const suffix = useMemo(
    () =>
      props.puzzleNumberForStorage < 0 ? "" + props.puzzleNumberForStorage : "",
    // Stryker disable next-line ArrayDeclaration
    [props.puzzleNumberForStorage]
  );

  const [dataIsUpToDate, setDataIsUpToDate] = useState(false);
  const statsUpdater = useCallback(
    (stats: Stats) => getUpdatedStats(props.puzzleNumberForStorage, stats),
    // Stryker disable next-line ArrayDeclaration
    [props.puzzleNumberForStorage]
  );
  const {
    value: stats,
    setAndPersistValue: setAndPersistStats,
    initValueFromStorage: initStatsFromStorage,
    ref: statsRef,
  } = usePersistedStateWithRef({
    storageKey: storageKey_stats,
    defaultValue: EMPTY_STATS,
    updater: statsUpdater,
  });

  const {
    value: winState,
    setAndPersistValue: setAndPersistWinState,
    initValueFromStorage: initWinStateFromStorage,
    resetValue: resetWinState,
    ref: winStateRef,
  } = usePersistedStateWithRef<number>({
    storageKey: storageKey_winState(suffix),
    defaultValue: DEFAULT_WIN_STATE,
  });

  const {
    value: guesses,
    setAndPersistValue: setAndPersistGuesses,
    initValueFromStorage: initGuessesFromStorage,
    resetValue: resetGuesses,
    ref: guessesRef,
  } = usePersistedStateWithRef<
    Array<GuessesEntry>,
    Array<GuessesEntryFromStorage>
  >({
    storageKey: storageKey_guesses(suffix),
    defaultValue: DEFAULT_GUESSES,
    mapper: mapGuessesToNestedArrayForStorage,
    backMapper: mapNestedArrayFromStorageToGuesses,
  });

  const {
    value: hintsUsed,
    setAndPersistValue: setAndPersistHintsUsed,
    initValueFromStorage: initHintsUsedFromStorage,
    resetValue: resetHintsUsed,
  } = usePersistedState<number>({
    storageKey: storageKey_hints_used(suffix),
    defaultValue: 0,
  });

  const {
    value: puzzleNumber,
    setAndPersistValue: setAndPersistPuzzleNumber,
    initValueFromStorage: initPuzzleNumberFromStorage,
  } = usePersistedState<number>({
    storageKey: storageKey_puzzleNumber,
    defaultValue: 0,
    defaultValueForInit: props.puzzleNumberForStorage,
    /* initially set to this if there is no stored value; default value of 0
      is used while localStorage is not yet read and when value is reset */
  });

  const {
    value: numberOfGuessesToEnd,
    setAndPersistValue: setAndPersistNumberOfGuessesToEnd,
    initValueFromStorage: initNumberOfGuessesToEndFromStorage,
  } = usePersistedState<number>({
    storageKey: storageKey_numberOfGuessesToEnd,
    defaultValue: DEFAULT_numberOfGuessesToEnd,
  });

  useEffect(
    () => {
      // Stryker disable BlockStatement,ConditionalExpression : We're not testing if window is defined
      /* istanbul ignore if */ // TODO: remove this once we have a way to test this (also elsewhere)
      if (typeof window === "undefined") {
        // Stryker disable next-line BooleanLiteral : We're not testing if window is defined
        setDataIsUpToDate(false);
      } else {
        // Stryker restore BlockStatement,ConditionalExpression
        const storagePuzzleNumberIsCurrent = isStoragePuzzleNumberCurrent(
          props.puzzleNumberForStorage
        );

        initStatsFromStorage();

        if (storagePuzzleNumberIsCurrent) {
          initPuzzleNumberFromStorage();
          initWinStateFromStorage();
          initGuessesFromStorage();
          initHintsUsedFromStorage();
          initNumberOfGuessesToEndFromStorage();
        } else {
          resetWinState();
          resetGuesses();
          resetHintsUsed();
        }

        setDataIsUpToDate(true);
      }
    },
    // Stryker disable next-line ArrayDeclaration
    [
      initGuessesFromStorage,
      initHintsUsedFromStorage,
      initNumberOfGuessesToEndFromStorage,
      initPuzzleNumberFromStorage,
      initStatsFromStorage,
      initWinStateFromStorage,
      props.puzzleNumberForStorage,
      resetGuesses,
      resetHintsUsed,
      resetWinState,
      setAndPersistPuzzleNumber,
    ]
  );

  const storedPuzzleNumberIsCurrent = useMemo(
    () => {
      return props.puzzleNumberForStorage === puzzleNumber;
    },
    // Stryker disable next-line ArrayDeclaration
    [props.puzzleNumberForStorage, puzzleNumber]
  );

  const secret = props.secrets[props.secrets.length - 1];

  useEffect(
    () => {
      if (dataIsUpToDate) {
        clearStorage(
          props.puzzleNumberForStorage,
          puzzleNumber,
          setAndPersistPuzzleNumber,
          setAndPersistWinState,
          setAndPersistGuesses
        );
      }
    },
    // Stryker disable next-line ArrayDeclaration
    [
      dataIsUpToDate,
      props.puzzleNumberForStorage,
      puzzleNumber,
      setAndPersistGuesses,
      setAndPersistPuzzleNumber,
      setAndPersistWinState,
      suffix,
    ]
  );

  useEffect(
    () => {
      setLatestGuess(undefined);
    },
    // Stryker disable next-line ArrayDeclaration
    [suffix]
  );

  useEffect(
    () => {
      const getAndSetSimilarityStory = async () => {
        setSimilarityStory(await getSimilarityStory(secret));
      };

      getAndSetSimilarityStory().catch((reason) => {
        setError(
          "Konnte Informationen über heutiges Spiel nicht laden. Spielen könnte aber trotzdem möglich sein."
        );
        console.error(reason);
      });
    },
    // Stryker disable next-line ArrayDeclaration
    [secret]
  );

  useEffect(
    () => {
      if (secretVec === undefined) {
        const getAndSetSecretVec = async () => {
          const model = await getModel(secret, secret);
          if (model !== undefined) {
            setSecretVec(model.vec);
          } else {
            setSecretVec(undefined);
            console.error("Could not get secret vector.");
          }
        };
        getAndSetSecretVec().catch((reason) => {
          setError(
            "Konnte Lösungswort nicht laden. Spielen ist momentan leider nicht möglich. Bitte versuche es später noch einmal."
          );
          console.error(reason);
        });
      }
    },
    // Stryker disable next-line ArrayDeclaration
    [secretVec, secret]
  );

  const puzzleNumberForDisplay = props.puzzleNumberForStorage;

  const { darkMode, setDarkMode, storedDarkMode } = useDarkMode();

  const [donateOpen, openDonate, closeDonate] = useDialogState();

  const headerProps: HeaderProps = {
    today: props.today,
    darkMode: storedDarkMode,
    setDarkMode,
    openDonateDialog: openDonate,
    secrets: props.secrets,
  };
  const similarityStoryProps: SimilarityStoryProps = {
    puzzleNumber: puzzleNumberForDisplay,
    similarityStory,
  };
  const guessFormProps: GuessFormProps | undefined = secretVec
    ? {
        secret,
        puzzleKey: props.puzzleNumberForStorage,
        setError,
        setLatestGuess,
        setChronoForward,
        secretVec,
        setLoadingSpinnerActive,
        stats: statsRef,
        setStats: setAndPersistStats,
        puzzleNumberFromStorage: puzzleNumber,
        winState: winStateRef,
        setWinState: setAndPersistWinState,
        guesses,
        setGuesses: setAndPersistGuesses,
        hintsUsed,
        setNumberOfGuessesToEnd: setAndPersistNumberOfGuessesToEnd,
      }
    : undefined;
  const responseProps: ResponseProps = {
    puzzleNumberForDisplay,
    secret,
    won: storedPuzzleNumberIsCurrent && winState === 1,
    guesses: guesses,
    hintsUsed: hintsUsed,
    stats: stats,
    numberOfGuessesToEnd,
    openDonateDialog: openDonate,
  };
  const guessesProps: GuessesProps = {
    latestGuess,
    chrono_forward: chronoForward,
    setChrono_forward: setChronoForward,
    darkMode,
    similarityStory,
    guesses: guesses,
    setGuesses: setAndPersistGuesses,
  };
  const hintGiveUpProps: HintGiveUpProps | undefined = secretVec
    ? {
        secret,
        puzzleKey: props.puzzleNumberForStorage,
        setChronoForward,
        setError,
        setLatestGuess,
        secretVec,
        puzzleNumberFromStorage: puzzleNumber,
        stats: statsRef,
        setStats: setAndPersistStats,
        winState: winState,
        setWinState: setAndPersistWinState,
        guesses: guessesRef,
        setGuesses: setAndPersistGuesses,
        hintsUsed: hintsUsed,
        setHintsUsed: setAndPersistHintsUsed,
        setNumberOfGuessesToEnd: setAndPersistNumberOfGuessesToEnd,
      }
    : undefined;

  return (
    <>
      <Header {...headerProps} />
      <SimilarityStory {...similarityStoryProps} />
      <ErrorMessage message={error} />
      <LoadingSpinnerOverlay active={loadingSpinnerActive || !dataIsUpToDate}>
        {winState > -1 && <Response {...responseProps} />}
        {guessFormProps && <GuessForm {...guessFormProps} />}
        <Guesses {...guessesProps} />
        {hintGiveUpProps && <HintGiveUp {...hintGiveUpProps} />}
      </LoadingSpinnerOverlay>
      <IntroText openDonate={openDonate} />
      <DonateDialog open={donateOpen} close={closeDonate} />
    </>
  );
});

const isStoragePuzzleNumberCurrent = (puzzleNumberForStorage: number) => {
  const rawPuzzleNumberFromStorage = window.localStorage.getItem(
    storageKey_puzzleNumber
  );
  const puzzleNumberFromStorage = rawPuzzleNumberFromStorage
    ? JSON.parse(rawPuzzleNumberFromStorage)
    : undefined;

  return puzzleNumberForStorage === puzzleNumberFromStorage;
};

export default Semantel;
