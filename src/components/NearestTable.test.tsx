/**
 * @jest-environment jsdom
 */

import NearestTable from "@/components/NearestTable";
import { Top1kEntry } from "@/types";
import { baseUrl } from "@/urls";
import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";

const word = "wurst";

const fetchMock2 = async (input: any, init?: RequestInit | undefined) => {
  const url = input as string;
  if (url === `${baseUrl}/nearby_1k/${word}`) {
    return {
      status: 200,
      json: async () => {
        const result: Array<Top1kEntry> = [
          { neighbor: "käse", percentile: 100, similarity: 0.8 },
          { neighbor: "brot", percentile: 99, similarity: 0.7 },
        ];
        return result;
      },
    } as Response;
  } else {
    return {
      json: async () => {
        return undefined;
      },
    } as Response;
  }
};

jest.mock("fetch-retry", () => {
  return jest.fn(() => fetchMock2);
});

const fetchRetry = require("fetch-retry");
const fetchMock = fetchRetry(global.fetch);

describe("NearestTable", function () {
  beforeEach(() => {
    global.fetch = fetchMock;
  });

  it("should show top1k table", async () => {
    render(<NearestTable word={word} />);

    await waitFor(() => {
      expect(screen.queryByText("käse")).toBeVisible();
      expect(screen.queryByText("brot")).toBeVisible();
    });
  });
});
