import formatNumber from "@/formatNumber";
import { memo, useMemo } from "react";
import { Stats } from "../types";

export type ResponseContentStatisticsProps = {
  stats: Stats;
};

const ResponseContentStatistics = memo(function ResponseContentStatistics_(
  props: Readonly<ResponseContentStatisticsProps>
) {
  const totalGames = useMemo(
    () => props.stats.wins + props.stats.giveups + props.stats.abandons,
    // Stryker disable next-line ArrayDeclaration
    [props.stats.abandons, props.stats.giveups, props.stats.wins]
  );

  return (
    <p>
      Statistiken: <br />
      <table data-testid="responseStatistics">
        <tbody>
          <tr>
            <th>Erstes Spiel:</th>
            <td>{props.stats.firstPlay}</td>
          </tr>
          <tr>
            <th>Gespielte Tage:</th>
            <td>{totalGames}</td>
          </tr>
          <tr>
            <th>Siege:</th>
            <td>{props.stats.wins}</td>
          </tr>
          <tr>
            <th>Siegesserie:</th>
            <td>{props.stats.winStreak}</td>
          </tr>
          <tr>
            <th>Aufgegeben:</th>
            <td>{props.stats.giveups}</td>
          </tr>
          <tr>
            <th>Nicht beendet:</th>
            <td>{props.stats.abandons}</td>
          </tr>
          <tr>
            <th>Gesamtzahl an Tipps:</th>
            <td>{props.stats.totalGuesses}</td>
          </tr>
          <tr>
            <th>Durchschnittliche Zahl von Tipps:</th>
            <td>{formatNumber(props.stats.totalGuesses / totalGames)}</td>
          </tr>
          <tr>
            <th>Benutzte Hinweise:</th>
            <td>{props.stats.hints}</td>
          </tr>
        </tbody>
      </table>
    </p>
  );
});

export default ResponseContentStatistics;
