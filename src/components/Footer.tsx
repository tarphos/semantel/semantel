import { memo } from "react";

const Footer = memo(function Footer_() {
  return (
    <footer>
      <div>
        Erstellt von <a href="mailto:apps@tarphos.de">Tobias Wichtrey</a> auf
        Grundlage des Originalspiels von{" "}
        <a href="https://novalis.org/">David Turner</a>.
      </div>
      <div>
        Tarphos Apps | Tobias Wichtrey | Münchener Str. 7 | 82362 Weilheim i. OB
        | <a href="mailto:apps@tarphos.de">apps@tarphos.de</a>
      </div>
    </footer>
  );
});

export default Footer;
