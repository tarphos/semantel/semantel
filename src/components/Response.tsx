import { memo, useState } from "react";
import { GuessesEntry, Stats } from "../types";
import ResponseContentGivenUp from "./ResponseContentGivenUp";
import ResponseContentStatistics from "./ResponseContentStatistics";
import ResponseContentWon from "./ResponseContentWon";
import ShareInvitation from "./ShareInvitation";
import Top1kAndShareInvitation from "./Top1kAndShareInvitation";
import CornerActionButton from "./CornerActionButton";

export type ResponseProps = {
  won: boolean;
  secret: string;
  puzzleNumberForDisplay: number;
  guesses: Array<GuessesEntry>;
  hintsUsed: number;
  stats: Stats;
  numberOfGuessesToEnd: number;
  openDonateDialog: () => void;
};

const Response = memo(function Response_(props: Readonly<ResponseProps>) {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <div
      aria-live="assertive"
      className="gameendresponse"
      data-testid="response"
    >
      <CornerActionButton
        close={() => {
          setCollapsed((oldCollapsed) => !oldCollapsed);
        }}
        type={collapsed ? "expand" : "collapse"}
      />

      {props.won ? (
        <ResponseContentWon
          guesses={props.guesses}
          numberOfGuessesToEnd={props.numberOfGuessesToEnd}
          secret={props.secret}
        />
      ) : (
        <ResponseContentGivenUp secret={props.secret} />
      )}
      {!collapsed && (
        <>
          <ShareInvitation
            guesses={props.guesses}
            hintsUsed={props.hintsUsed}
            numberOfGuessesToEnd={props.numberOfGuessesToEnd}
            puzzleNumberForDisplay={props.puzzleNumberForDisplay}
            won={props.won}
          />
          <Top1kAndShareInvitation
            openDonateDialog={props.openDonateDialog}
            secret={props.secret}
          />
          <ResponseContentStatistics stats={props.stats} />
        </>
      )}
    </div>
  );
});

export default Response;
