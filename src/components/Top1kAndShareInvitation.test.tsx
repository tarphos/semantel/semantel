/**
 * @jest-environment jsdom
 */

import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Top1kAndShareInvitation from "./Top1kAndShareInvitation";
import userEvent from "@testing-library/user-event";

describe("Top1kAndShareInvitation", function () {
  it("should include encoded secret in link", () => {
    const secret = "hund";
    render(
      <Top1kAndShareInvitation secret={secret} openDonateDialog={jest.fn()} />
    );

    expect(
      screen.getByText("hier die tausend ähnlichsten Wörter anschauen")
    ).toHaveAttribute("href", "top1k?w=aHVuZA%3D%3D");
  });

  it("should open donate dialog on click", async () => {
    const openDonateDialog = jest.fn();
    render(
      <Top1kAndShareInvitation
        secret={"hund"}
        openDonateDialog={openDonateDialog}
      />
    );

    expect(openDonateDialog).not.toHaveBeenCalled();

    await userEvent.click(
      screen.getByText("hier, wie du uns finanziell unterstützen kannst.")
    );

    expect(openDonateDialog).toHaveBeenCalled();
  });
});
