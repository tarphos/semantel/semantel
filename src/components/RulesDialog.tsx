import React, { memo, useEffect } from "react";
import { getLocalTimeString } from "../timeUtils";
import Dialog from "./Dialog";
import { readRules } from "../constants/localStorageKeys";

export type RulesDialogProps = {
  open: boolean;
  close: () => void;
  yesterday: string;
};

const RulesDialog = memo(function RulesDialog_(
  props: Readonly<RulesDialogProps>
) {
  const rulesHeading = "rules-heading";

  useEffect(() => {
    if (props.open) {
      localStorage.setItem(readRules, "true");
    }
  }, [props.open]);

  return (
    props.open && (
      <Dialog
        underlayId="rules-underlay"
        ariaLabelledby={rulesHeading}
        closeButtonId="rules-close"
        close={props.close}
      >
        <h3 id={rulesHeading}>Errate das Lösungswort</h3>
        <p>
          Jeder Tipp muss ein Wort sein. Semantel sagt dir dann, für wie
          semantisch ähnlich zum Lösungswort es dein Wort hält. Anders als bei
          diesem anderen Worträtsel geht es hier nicht um die Schreibewise
          sondern um die Bedeutung. Der Ähnlichkeitswert kommt von Word2vec. Die
          höchstmögliche Ähnlichkeit ist 100 (was bedeutet, dass die Wörter
          identisch sind und du gewonnen hast). Die niedrigste Ähnlichkeit ist
          theoretisch -100, liegt in der Praxis aber eher bei -34. Mit
          „semantisch ähnlich“ ist, grob gesagt, gemeint „wird in ähnlichem
          Kontext verwendet“. Textgrundlage dafür ist die deutsche Wikipedia.
        </p>
        <p>
          Das Lösungswort kann jede beliebige Wortart haben, ist aber immer ein
          einzelnes Wort. Es ist verlockend, nur an Substantive zu denken, weil
          so normalerweise semantische Wort-Ratespiele funktionieren. Lass dich
          nicht aufs Glatteis führen! Groß- und Kleinschreibung wird nicht
          unterschieden. Eigennamen tauchen nicht unter den Lösungsworten auf.
        </p>
        <p>
          Die „Nah dran“-Anzeige sagt dir, wie nah du dran bist – falls dein
          Wort eines der 1000 besten Wörter ist, wird der Rang angezeigt (1000
          ist das Lösungswort selbst). Falls dein Wort keines der 1000 nächsten
          Wörter ist, bist du „kalt“. (Wenn hier ???? steht, dann ist dein Wort
          nicht in der Liste von gebräuchlichen Wörtern, die für die Top-1000
          berücksichtigt werden. Aber es ist dennoch sehr ähnlich.)
        </p>
        <p>
          Du wirst mehr als sechs Versuche benötigen. Vermutlich wirst du
          Dutzende Versuche brauchen.{" "}
          <strong>Es gibt jeden Tag ein neues Wort</strong>, wobei ein Tag um
          Mitternacht nach GMT beginnt {getLocalTimeString()}. Das Wort von
          gestern war <b>„{props.yesterday}“</b>.
        </p>
      </Dialog>
    )
  );
});

export default RulesDialog;
