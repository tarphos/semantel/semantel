import { decodeBase64 } from "@/base64";
import Backlink from "@/components/Backlink";
import DonateDialog from "@/components/DonateDialog";
import Header, { HeaderProps } from "@/components/Header";
import NearestTable from "@/components/NearestTable";
import useDarkMode from "@/useDarkMode";
import useDialogState from "@/useDialogState";
import { getToday } from "@/utils";
import withLoadingSpinnerAndFallback from "@/withLoadingSpinnerAndFallback";
import { memo, useEffect, useState } from "react";

const Top1kComponent = memo(function Top1kComponent_(props: {
  secrets: Array<string>;
}) {
  const [word, setWord] = useState<string>();

  useEffect(() => {
    document.body.classList.remove("dialog-open");
  }, []);

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search);
    const p = searchParams.get("w");
    if (p !== null) {
      setWord(decodeBase64(p));
    } else {
      setWord("");
    }
  }, []);

  const { setDarkMode, storedDarkMode } = useDarkMode();

  const today = getToday(Date.now());

  const [donateOpen, openDonate, closeDonate] = useDialogState();

  const headerProps: HeaderProps = {
    today,
    darkMode: storedDarkMode,
    setDarkMode,
    openDonateDialog: openDonate,
    secrets: props.secrets,
  };

  return (
    <>
      <Header {...headerProps} />
      {withLoadingSpinnerAndFallback<{ word?: string }>(
        ({ word }) =>
          word !== "" ? (
            <NearestTable word={word} />
          ) : (
            <div>Kein Wort angegeben</div>
          ),
        { word }
      )}
      <Backlink />
      <DonateDialog open={donateOpen} close={closeDonate} />
    </>
  );
});

export default Top1kComponent;
