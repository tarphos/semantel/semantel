/**
 * @jest-environment jsdom
 */

import GuessForm from "@/components/GuessForm";
import { EMPTY_STATS } from "@/constants";
import { DoGuessParams } from "@/types";
import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

jest.mock("fetch-retry"); // can't import data module otherwise

const guessModule = require("@/guess");

describe("GuessForm", function () {
  let doGuessStub: jest.SpyInstance;
  let setError: jest.Mock;

  beforeEach(() => {
    doGuessStub = jest.spyOn(guessModule, "doGuess").mockReturnValue(undefined);

    setError = jest.fn();
  });

  it("should guess", async () => {
    render(<GuessFormContainer />);

    await act(() =>
      userEvent.type(screen.getByPlaceholderText("Raten"), "Käse[Enter]")
    );

    expect(doGuessStub).toHaveBeenCalledWith(
      expect.objectContaining<Partial<DoGuessParams>>({
        guess: "käse",
        is_hint: false,
      })
    );
    expectNoErrors();
  });

  it("should remove weird characters", async () => {
    render(<GuessFormContainer />);

    await act(() =>
      userEvent.type(screen.getByPlaceholderText("Raten"), " Ha!*[Enter]")
    );

    expect(doGuessStub).toHaveBeenCalledWith(
      expect.objectContaining({ guess: "ha" })
    );
    expectNoErrors();
  });

  it("should ignore empty guesses", async () => {
    render(<GuessFormContainer />);

    await act(() =>
      userEvent.type(screen.getByPlaceholderText("Raten"), "     [Enter]")
    );

    expect(doGuessStub).not.toHaveBeenCalled();
    expectNoErrors();
  });

  it("should handle error while guessing", async () => {
    doGuessStub.mockImplementation(() => {
      throw new Error("oh no!");
    });

    render(<GuessFormContainer />);

    await act(() =>
      userEvent.type(screen.getByPlaceholderText("Raten"), "Käse[Enter]")
    );

    expect(setError).toHaveBeenCalledWith(
      "An error ooccurred when submitting your guess: oh no!"
    );
  });

  it("should handle non-error throwable while guessing", async () => {
    doGuessStub.mockImplementation(() => {
      throw "oh no!";
    });

    render(<GuessFormContainer />);

    await act(() =>
      userEvent.type(screen.getByPlaceholderText("Raten"), "Käse[Enter]")
    );

    expect(setError).toHaveBeenCalledWith(
      "An error ooccurred when submitting your guess: oh no!"
    );
  });

  const GuessFormContainer = () => {
    return (
      <GuessForm
        winState={{ current: -1 }}
        setWinState={() => {}}
        guesses={[]}
        setGuesses={() => {}}
        hintsUsed={0}
        setNumberOfGuessesToEnd={() => {}}
        puzzleKey={2}
        secret=""
        secretVec={[]}
        setChronoForward={() => {}}
        setError={setError}
        setLatestGuess={() => {}}
        puzzleNumberFromStorage={1}
        stats={{ current: EMPTY_STATS }}
        setStats={() => {}}
        setLoadingSpinnerActive={() => {}}
      />
    );
  };

  function expectNoErrors() {
    const firstError = setError.mock.calls
      .map((call) => call[0])
      .find((arg) => arg !== undefined && arg !== "");

    expect(firstError).toBeUndefined();
  }
});
