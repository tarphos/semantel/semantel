import { DEFAULT_numberOfGuessesToEnd } from "@/constants";
import { memo } from "react";
import { GuessesEntry } from "../types";

export type ResponseContentWonProps = {
  secret: string;
  guesses: Array<GuessesEntry>;
  numberOfGuessesToEnd: number;
};

const ResponseContentWon = memo(function ResponseContentWon_(
  props: Readonly<ResponseContentWonProps>
) {
  const versuche =
    props.numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
      ? props.guesses.length
      : props.numberOfGuessesToEnd;

  return (
    <p data-testid="won">
      <b>
        Du hast es mit {versuche} {versuche === 1 ? "Versuch" : "Versuchen"}{" "}
        gefunden! Das Lösungswort ist {props.secret}.
      </b>{" "}
      Gib ruhig weitere Wörter ein, wenn du neugierig bist, wie die
      Ähnlichkeiten aussehen.
    </p>
  );
});

export default ResponseContentWon;
