/**
 * @jest-environment jsdom
 */

import Footer from "@/components/Footer";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

jest.mock("fetch-retry"); // can't import data module otherwise

const faqDialogModule = require("@/components/FaqDialog");

describe("Footer", function () {
  it("should display correct text", () => {
    render(<Footer />);

    expect(screen.getByText(/Erstellt/)).toHaveTextContent(
      "Erstellt von Tobias Wichtrey auf Grundlage des Originalspiels von David Turner."
    );

    expect(screen.getByText(/Tarphos/)).toHaveTextContent(
      "Tarphos Apps | Tobias Wichtrey | Münchener Str. 7 | 82362 Weilheim i. OB | apps@tarphos.de"
    );
  });

  it("should have clickable email address", () => {
    render(<Footer />);

    expect(screen.getByText(/^apps@tarphos.de$/)).toHaveAttribute(
      "href",
      "mailto:apps@tarphos.de"
    );

    expect(screen.getByText(/^Tobias Wichtrey$/)).toHaveAttribute(
      "href",
      "mailto:apps@tarphos.de"
    );
  });
});
