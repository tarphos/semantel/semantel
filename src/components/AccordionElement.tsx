import React, { ReactNode, memo } from "react";

type AccordionElementProps = {
  title: string;
  children: ReactNode;
  active: boolean;
  toggleActive: () => void;
};

const AccordionElement = memo(function AccordionElement(
  props: Readonly<AccordionElementProps>
) {
  // Stryker disable StringLiteral, ObjectLiteral : TODO use some CSS framework
  const buttonClass = "accordion" + (props.active ? " active" : "");
  const panelStyle = { display: "block" };
  // Stryker restore StringLiteral, ObjectLiteral

  return (
    <>
      <button className={buttonClass} onClick={() => props.toggleActive()}>
        {props.title}
      </button>
      {props.active && (
        <div className="panel" style={panelStyle}>
          {props.children}
        </div>
      )}
    </>
  );
});

export default AccordionElement;
