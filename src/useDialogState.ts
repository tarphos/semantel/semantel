import { useCallback, useState } from "react";

const useDialogState: () => [boolean, () => void, () => void] = () => {
  const [isOpen, setIsOpen] = useState(false);
  const open = useCallback(
    () => setIsOpen(true),
    // Stryker disable next-line ArrayDeclaration
    []
  );
  const close = useCallback(
    () => setIsOpen(false),
    // Stryker disable next-line ArrayDeclaration
    []
  );
  return [isOpen, open, close];
};

export default useDialogState;
