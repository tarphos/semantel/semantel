import { decodeBase64, encodeBase64 } from "@/base64";

describe("base64", () => {
  it("should encode and decode strings correctly", () => {
    const input = "Schönen Gruß!";
    const encoded = encodeBase64(input);
    const decoded = decodeBase64(encoded);

    expect(decoded).toBe(input);
  });

  it("should decode string encoded in Python correctly", () => {
    const decoded = decodeBase64("a8Okc2XDnw==");

    expect(decoded).toBe("käseß");
  });
});
