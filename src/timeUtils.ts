export function getLocalTimeString() {
  const midnight = new Date();
  midnight.setUTCHours(24, 0, 0, 0);

  return `oder ${midnight.getHours()}:${midnight
    .getMinutes()
    .toString()
    .padStart(2, "0")} in deiner Zeit`;
}
