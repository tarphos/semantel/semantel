/**
 * @jest-environment jsdom
 */

import assert from "assert";
import {
  describe as describeFromUtils,
  finishedAfterOneGuess,
  getFirstHit,
  plural,
  share,
  solveStory,
} from "./share";

const copyToClipboardModule = require("../src/copyToClipboard");

import { DEFAULT_numberOfGuessesToEnd } from "@/constants";
import { GuessesEntry } from "./types";

describe("share", function () {
  describe("#finishedAfterOneGuess()", function () {
    it("should return correct message for won game", function () {
      const puzzleNumber = 42;
      const message = finishedAfterOneGuess(1, puzzleNumber);
      assert.equal(
        message,
        `Ich habe Semantel ${puzzleNumber} mit dem ersten Versuch gelöst! https://semantel.tarphos.de/`
      );
    });

    it("should return correct message for not aborted game", function () {
      const puzzleNumber = 23;
      const message = finishedAfterOneGuess(0, puzzleNumber);
      assert.equal(
        message,
        `Ich habe Semantel ${puzzleNumber} nach dem ersten Versuch aufgegeben! https://semantel.tarphos.de/`
      );
    });
  });

  describe("#describe()", function () {
    it("should return correct message without percentile", function () {
      const guess: GuessesEntry = { similarity: 0.123, word: "foo", index: 7 };
      const message = describeFromUtils(guess);
      assert.equal(message, `hatte eine Ähnlichkeit von 0,12`);
    });

    it("should return correct message with percentile", function () {
      const guess: GuessesEntry = {
        similarity: 0.123,
        word: "foo",
        percentile: 420,
        index: 7,
      };
      const message = describeFromUtils(guess);
      assert.equal(message, `hatte eine Ähnlichkeit von 0,12 (420/1000)`);
    });
  });

  describe("#getFirstHit()", function () {
    it("should return correct message when first guess was a hit", function () {
      const guesses: Array<GuessesEntry> = [
        { similarity: 0.123, word: "foo", percentile: 13, index: 1 },
        { similarity: 0.223, word: "bar", percentile: 23, index: 2 },
        { similarity: 0.323, word: "fiz", percentile: 100, index: 3 },
      ];
      const message = getFirstHit(guesses);
      assert.equal(message, "");
    });

    it("should return correct message when second guess was first hit", function () {
      const guesses: Array<GuessesEntry> = [
        { similarity: 0.123, word: "foo", percentile: undefined, index: 1 },
        { similarity: 0.223, word: "bar", percentile: 23, index: 2 },
        { similarity: 0.323, word: "fiz", percentile: 100, index: 3 },
      ];
      const message = getFirstHit(guesses);
      assert.equal(
        message,
        "Mein erstes Wort in den Top-1000 war bei Versuch #2. "
      );
    });

    it("should return correct message when never hit top1000", function () {
      const guesses: Array<GuessesEntry> = [
        { similarity: 0.123, word: "foo", index: 1 },
        { similarity: 0.223, word: "bar", index: 2 },
        { similarity: 0.323, word: "fiz", index: 3 },
      ];
      const message = getFirstHit(guesses);
      assert.equal(message, "");
    });
  });

  describe("#plural()", function () {
    it("should return plural", async function () {
      const result = plural(2, "Hinweis");
      assert.equal(result, "Hinweisen");
    });

    it("should return singular", async function () {
      const result = plural(1, "Hinweis");
      assert.equal(result, "Hinweis");
    });
  });

  describe("#solveStory()", function () {
    const testCases: Array<{
      guesses: Array<GuessesEntry>;
      puzzleNumber: number;
      won: boolean;
      hints_used: number;
      expectedResult: string;
    }> = [
      {
        guesses: [],
        puzzleNumber: 1,
        won: false,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel 1 aufgegeben, ohne überhaupt einmal zu raten. https://semantel.tarphos.de/",
      },
      {
        guesses: [{ similarity: 7, word: "Hund", percentile: 1000, index: 0 }],
        puzzleNumber: 2,
        won: true,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel 2 mit dem ersten Versuch gelöst! https://semantel.tarphos.de/",
      },
      {
        guesses: [{ similarity: 7, word: "Hund", percentile: 800, index: 0 }],
        puzzleNumber: 3,
        won: false,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel 3 nach dem ersten Versuch aufgegeben! https://semantel.tarphos.de/",
      },
      {
        guesses: [
          { similarity: 7, word: "Hund", percentile: 0, index: 1 },
          { similarity: 8, word: "Katze", percentile: 800, index: 2 },
          { similarity: 9, word: "Maus", percentile: 1000, index: 3 },
        ],
        puzzleNumber: 4,
        won: true,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel #4 mit 3 Versuchen gelöst. Mein erster Tipp hatte eine Ähnlichkeit von 7,00. Mein erstes Wort in den Top-1000 war bei Versuch #2. Mein vorletzter Tipp hatte eine Ähnlichkeit von 8,00 (800/1000). https://semantel.tarphos.de/",
      },
      {
        guesses: [
          { similarity: 7, word: "Hund", percentile: 0, index: 1 },
          { similarity: 8, word: "Katze", percentile: 800, index: 2 },
          { similarity: 9, word: "Maus", percentile: 900, index: 3 },
        ],
        puzzleNumber: 4,
        won: false,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel #4 mit 3 Versuchen aufgegeben. Mein erster Tipp hatte eine Ähnlichkeit von 7,00. Mein erstes Wort in den Top-1000 war bei Versuch #2. Mein letzter Tipp hatte eine Ähnlichkeit von 9,00 (900/1000). https://semantel.tarphos.de/",
      },
      {
        guesses: [
          { similarity: 7, word: "Hund", percentile: 0, index: 1 },
          { similarity: 8, word: "Katze", percentile: 800, index: 2 },
          { similarity: 9, word: "Maus", percentile: 1000, index: 3 },
        ],
        puzzleNumber: 4,
        won: true,
        hints_used: 1,
        expectedResult:
          "Ich habe Semantel #4 mit 2 Versuchen und 1 Hinweis gelöst. Mein erster Tipp hatte eine Ähnlichkeit von 7,00. Mein erstes Wort in den Top-1000 war bei Versuch #2. Mein vorletzter Tipp hatte eine Ähnlichkeit von 8,00 (800/1000). https://semantel.tarphos.de/",
      },
      {
        guesses: [
          { similarity: 7, word: "Hund", percentile: 0, index: 1 },
          { similarity: 10, word: "Maus", percentile: 1000, index: 4 },
          { similarity: 8, word: "Katze", percentile: 800, index: 2 },
          { similarity: 9, word: "Vogel", percentile: 900, index: 3 },
        ],
        puzzleNumber: 5,
        won: true,
        hints_used: 2,
        expectedResult:
          "Ich habe Semantel #5 mit 2 Versuchen und 2 Hinweisen gelöst. Mein erster Tipp hatte eine Ähnlichkeit von 7,00. Mein erstes Wort in den Top-1000 war bei Versuch #2. Mein vorletzter Tipp hatte eine Ähnlichkeit von 9,00 (900/1000). https://semantel.tarphos.de/",
      },
      {
        guesses: [
          { similarity: 7, word: "Hund", percentile: 700, index: 1 },
          { similarity: 8, word: "Katze", percentile: 800, index: 2 },
          { similarity: 9, word: "Maus", percentile: 1000, index: 3 },
        ],
        puzzleNumber: 6,
        won: true,
        hints_used: 0,
        expectedResult:
          "Ich habe Semantel #6 mit 3 Versuchen gelöst. Mein erster Tipp hatte eine Ähnlichkeit von 7,00 (700/1000). Mein vorletzter Tipp hatte eine Ähnlichkeit von 8,00 (800/1000). https://semantel.tarphos.de/",
      },
    ];

    testCases.forEach(
      ({ guesses, puzzleNumber, won, hints_used, expectedResult }) => {
        it(`should return correct result for Semantel ${puzzleNumber}`, async function () {
          const originalGuesses = guesses.slice();
          const result = solveStory(guesses, puzzleNumber, won, hints_used);
          assert.equal(result, expectedResult);
          assert.deepEqual(guesses, originalGuesses);
        });
      }
    );
  });

  describe("#share()", function () {
    const puzzleNumber = 123;
    const hints_used = 0;
    const alerts: Array<string> = [];

    function alertMock(message: string) {
      alerts.push(message);
    }

    beforeEach(async function () {
      alerts.length = 0;

      window.alert = alertMock;
    });

    it.each([DEFAULT_numberOfGuessesToEnd, 2])(
      "shouldCopyGivingUpTextToClipboard (numberOfGuessesToEnd: %p)",
      async function (numberOfGuessesToEnd) {
        const copyMock = jest
          .spyOn(copyToClipboardModule, "copyToClipboard")
          .mockReturnValue(true);

        share(
          puzzleNumber,
          false,
          [
            { similarity: 33.8613687072138, word: "hund", index: 1 },
            { similarity: 29.321888197666823, word: "katze", index: 2 },
            { similarity: 15.273979081016886, word: "maus", index: 3 },
          ],
          hints_used,
          numberOfGuessesToEnd
        );

        assert.deepEqual(alerts, ["In die Zwischenablage kopiert"]);
        const versuche =
          numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
            ? 3
            : numberOfGuessesToEnd;
        expect(copyMock).toHaveBeenCalledWith(
          `Ich habe Semantel #${puzzleNumber} mit ${versuche} Versuchen aufgegeben. Mein erster Tipp hatte eine Ähnlichkeit von 33,86. Mein letzter Tipp hatte eine Ähnlichkeit von ${
            numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
              ? "15,27"
              : "29,32"
          }. https://semantel.tarphos.de/`
        );
      }
    );

    it.each([DEFAULT_numberOfGuessesToEnd, 2])(
      "shouldCopyWinningTextToClipboard (numberOfGuessesToEnd: %p)",
      async function (numberOfGuessesToEnd) {
        const copyMock = jest
          .spyOn(copyToClipboardModule, "copyToClipboard")
          .mockReturnValue(true);
        share(
          puzzleNumber,
          true,
          [
            { similarity: 33.8613687072138, word: "hund", index: 1 },
            { similarity: 29.321888197666823, word: "katze", index: 2 },
            { similarity: 15.273979081016886, word: "maus", index: 3 },
          ],
          hints_used,
          numberOfGuessesToEnd
        );

        assert.deepEqual(alerts, ["In die Zwischenablage kopiert"]);
        const versuche =
          numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
            ? 3
            : numberOfGuessesToEnd;
        expect(copyMock).toHaveBeenCalledWith(
          `Ich habe Semantel #${puzzleNumber} mit ${versuche} Versuchen gelöst. Mein erster Tipp hatte eine Ähnlichkeit von 33,86. Mein vorletzter Tipp hatte eine Ähnlichkeit von ${
            numberOfGuessesToEnd === DEFAULT_numberOfGuessesToEnd
              ? "29,32"
              : "33,86"
          }. https://semantel.tarphos.de/`
        );
      }
    );

    it("shouldAlertIfCopyingFails", async function () {
      jest
        .spyOn(copyToClipboardModule, "copyToClipboard")
        .mockReturnValue(false);

      share(
        puzzleNumber,
        true,
        [
          { similarity: 33.8613687072138, word: "hund", index: 1 },
          { similarity: 29.321888197666823, word: "katze", index: 2 },
          { similarity: 15.273979081016886, word: "maus", index: 3 },
        ],
        hints_used,
        DEFAULT_numberOfGuessesToEnd
      );

      assert.deepEqual(alerts, ["Fehler beim Kopieren in die Zwischenablage"]);
    });
  });
});
