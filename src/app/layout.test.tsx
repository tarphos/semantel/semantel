/**
 * @jest-environment jsdom
 */

import RootLayout from "@/app/layout";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { mockComponent } from "../testutils/mockComponent";

const footerModule = require("@/components/Footer");

describe("Root layout", function () {
  let testIdFooter: string;
  let mockedFooter: jest.Mock;

  ({ testId: testIdFooter, mockedComponent: mockedFooter } =
    mockComponent(footerModule));

  it("should display footer", () => {
    render(<RootLayout>foo</RootLayout>);

    expect(screen.getByTestId(testIdFooter)).toBeVisible();

    expect(mockedFooter).toHaveBeenCalled();
  });
});
