"use client";
import ErrorMessage from "@/components/ErrorMessage";
// IMPORTANT NOTE: We need "use client" in order to have Date.now() update correctly

import SemantelRoot from "@/components/SemantelRoot";
import useSecrets from "@/useSecrets";
import { getToday } from "@/utils";
import withLoadingSpinnerAndFallback from "@/withLoadingSpinnerAndFallback";

import { useCallback, useEffect, useState } from "react";

// Stryker disable all : TODO add tests

const EVERY_45_MINUTES = 45 * 60 * 1000;

export default function Home() {
  useEffect(() => {
    document.body.classList.remove("dialog-open");
  }, []);

  const today = getToday(Date.now());

  const [puzzleNumber, setPuzzleNumber] = useState<number>();
  const [secrets, setSecrets] = useState<Array<string>>();
  const [error, setError] = useState<string>();

  const [newSecrets, newPuzzleNumber, resetNewSecretsAndPuzzleNumber] =
    useSecrets(setError);

  const openNewPuzzle = useCallback(() => {
    setPuzzleNumber(newPuzzleNumber);
    setSecrets(newSecrets);
    resetNewSecretsAndPuzzleNumber();
  }, [newPuzzleNumber, newSecrets, resetNewSecretsAndPuzzleNumber]);

  useEffect(() => {
    if (puzzleNumber === undefined || secrets === undefined) {
      openNewPuzzle();
    }
  }, [openNewPuzzle, puzzleNumber, secrets]);

  return error === undefined ? (
    withLoadingSpinnerAndFallback<{
      puzzleNumber?: number;
      secrets?: Array<string>;
    }>(
      (props) => (
        <SemantelRoot
          today={today}
          puzzleNumber={props.puzzleNumber}
          secrets={props.secrets}
          newPuzzleNumber={newPuzzleNumber}
          openNewPuzzle={openNewPuzzle}
        />
      ),
      { puzzleNumber, secrets }
    )
  ) : (
    <ErrorMessage message={error} />
  );
}
