import Footer from "@/components/Footer";
import type { Metadata } from "next";
import "./globals.css";

// Stryker disable ArrayDeclaration, StringLiteral, ObjectLiteral : This is a Next.js metadata object
export const metadata: Metadata = {
  title: "Semantel",
  description: "Das semantische Worträtsel",
  authors: [
    { name: "David Turner", url: "https://novalis.org/" },
    { name: "Tobias Wichtrey", url: "https://www.tarphos.de/" },
  ],
};
// Stryker restore all

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="de">
      <body>
        {children}
        <Footer />
      </body>
    </html>
  );
}
