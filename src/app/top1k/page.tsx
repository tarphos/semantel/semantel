"use client";

import ErrorMessage from "@/components/ErrorMessage";
import Top1kComponent from "@/components/Top1kComponent";
import useSecrets from "@/useSecrets";
import withLoadingSpinnerAndFallback from "@/withLoadingSpinnerAndFallback";
import { useEffect, useState } from "react";

// Stryker disable all : TODO add tests

export default function Top1kPage() {
  const [error, setError] = useState<string>();
  const [secrets, setSecrets] = useState<Array<string>>();
  const [newSecrets, __, resetNewSecretsAndPuzzleNumber] = useSecrets(setError);

  useEffect(() => {
    if (newSecrets !== undefined) {
      setSecrets(newSecrets);
      resetNewSecretsAndPuzzleNumber();
    }
  }, [newSecrets, resetNewSecretsAndPuzzleNumber]);

  return error === undefined ? (
    withLoadingSpinnerAndFallback<{ secrets?: Array<string> }>(
      (props) => <Top1kComponent secrets={props.secrets} />,
      { secrets }
    )
  ) : (
    <ErrorMessage message={error} />
  );
}
