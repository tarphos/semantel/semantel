import "./top1k.css";
import type { Metadata } from "next";

// Stryker disable all : TODO add tests

export const metadata: Metadata = {
  title: "Semantel: nächste Wörter",
  description: "Die tausend ähnlichsten Wörter",
  robots: "noindex",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return children;
}
