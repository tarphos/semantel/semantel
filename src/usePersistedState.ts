import {
  Dispatch,
  MutableRefObject,
  SetStateAction,
  useCallback,
  useRef,
  useState,
} from "react";
import { readAndSetValue } from "./readAndSetValue";
import { GuessesEntry, Stats } from "./types";

type UsePersistedStateParams<T, S> = {
  storageKey: string;
  defaultValue: T;
  defaultValueForInit?: T;
  mapper?: (value: T) => S;
  backMapper?: (value: S) => T;
  updater?: (mappedValue: T) => T | undefined;
};

type UsePersistedStateResult<T> = {
  value: T;
  setAndPersistValue: Dispatch<SetStateAction<T>>;
  initValueFromStorage: () => T;
  resetValue: () => void;
};

export function usePersistedState<
  T extends Stats | number | Array<GuessesEntry> | (boolean | undefined),
  S = undefined
>(params: UsePersistedStateParams<T, S>): UsePersistedStateResult<T> {
  const { ref, ...resultWithoutRef } = usePersistedStateWithRef(params);
  return resultWithoutRef;
}

export function usePersistedStateWithRef<
  T extends Stats | number | Array<GuessesEntry> | (boolean | undefined),
  S = undefined
>(
  params: UsePersistedStateParams<T, S>
): UsePersistedStateResult<T> & { ref: MutableRefObject<T> } {
  const [value, setValue] = useState<T>(params.defaultValue);
  const ref = useRef(value);
  const { mapper, storageKey } = params;
  const setAndPersistValue: Dispatch<SetStateAction<T>> = useCallback(
    (setStateAction) =>
      setValue((originalValue) => {
        const newValue =
          typeof setStateAction === "function"
            ? setStateAction(originalValue)
            : setStateAction;
        const mappedValue = mapper === undefined ? newValue : mapper(newValue);
        localStorage.setItem(storageKey, JSON.stringify(mappedValue));
        ref.current = newValue;
        return newValue;
      }),
    // Stryker disable next-line ArrayDeclaration
    [mapper, storageKey]
  );
  const initValueFromStorage = useCallback(
    () =>
      readAndSetValue(
        params.storageKey,
        params.defaultValueForInit ?? params.defaultValue,
        setValue,
        params.backMapper,
        params.updater,
        ref
      ),
    // Stryker disable next-line ArrayDeclaration
    [
      params.backMapper,
      params.defaultValue,
      params.defaultValueForInit,
      params.storageKey,
      params.updater,
    ]
  );
  const resetValue = useCallback(
    () => {
      window.localStorage.removeItem(params.storageKey);
      setValue(params.defaultValue);
    },
    // Stryker disable next-line ArrayDeclaration
    [params.defaultValue, params.storageKey]
  );

  return { value, setAndPersistValue, initValueFromStorage, resetValue, ref };
}
