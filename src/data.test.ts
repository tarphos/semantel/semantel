import assert from "assert";

import { encodeBase64 } from "@/base64";
import { DEFAULT_WIN_STATE, EMPTY_STATS } from "@/constants";
import {
  getModel,
  getNearby,
  getSecrets,
  getSimilarityStory,
  hint,
} from "./data";
import { GuessesEntry, HintParams } from "./types";
import { baseUrl } from "./urls";

const dataModule = require("../src/data");
const guessModule = require("../src/guess");

const secret = "kunst";
const secret64 = encodeURIComponent(encodeBase64(secret));

let secretsApiResponse = undefined as unknown as Response; // to be filled in tests

const fetchMock2 = async (input: any, init?: RequestInit | undefined) => {
  const url = input as string;
  if (url === `${baseUrl}/model/wand`) {
    return successWithJson("wandModel");
  } else if (url === `${baseUrl}/modelv2/${secret64}`) {
    return successWithJson("kunstModel");
  } else if (url === `${baseUrl}/percentilev2/${secret64}/wand`) {
    return successWithJson("kunstWandPercentile");
  } else if (url === `${baseUrl}/model/hand`) {
    return successWithJson("handModel");
  } else if (
    url === `${baseUrl}/model/nein` ||
    url === `${baseUrl}/percentilev2/${secret64}/hand`
  ) {
    return errorWithStatus(404);
  } else if (url === `${baseUrl}/model/perc500`) {
    return successWithJson("perc500Model");
  } else if (url === `${baseUrl}/percentilev2/${secret64}/perc500`) {
    return errorWithStatus(500);
  } else if (url === `${baseUrl}/model/vec500`) {
    return errorWithStatus(500);
  } else if (url === `${baseUrl}/similarityv2/${secret64}`) {
    return successWithJson("kunstSimilarity");
  } else if (
    url ===
      `${baseUrl}/similarityv2/ZmVobGVy` /*- that's base64 of 'fehler -*/ ||
    url === `${baseUrl}/nearby/fehler` ||
    url === `${baseUrl}/nth_nearby/${secret}/997`
  ) {
    return {
      json: async () => {
        throw new Error();
      },
    } as unknown as Response;
  } else if (url === `${baseUrl}/nearby/wand`) {
    return successWithJson("wandNearby");
  } else if (url === `${baseUrl}/nth_nearby/${secret}/996`) {
    return successWithJson("996thNearby");
  } else if (url === `${baseUrl}/secret`) {
    if (secretsApiResponse === undefined) {
      throw new TypeError("foo");
    } else {
      return secretsApiResponse;
    }
  } else if (url === "https://semantel.tarphos.de/api/model/a_b_c_d") {
    return successWithJson("escapedVec");
  } else if (
    url === "https://semantel.tarphos.de/api/percentilev2/a3Vuc3Q%3D/a_b_c_d"
  ) {
    return successWithJson("escapedPercentile");
  } else {
    return {
      json: async () => {
        return undefined;
      },
    } as Response;
  }
};

jest.mock("fetch-retry", () => {
  return jest.fn(() => fetchMock2);
});

const fetchRetry = require("fetch-retry");
const fetchMock = fetchRetry(global.fetch);

describe("data", function () {
  beforeEach(() => {
    global.fetch = fetchMock;
    // console.log = jest.fn();
    console.error = jest.fn();
  });

  describe("#getModel()", function () {
    it("should return model", async function () {
      const guess = "wand";
      const model = await getModel(guess, secret);
      assert.deepEqual(model, {
        percentile: "kunstWandPercentile",
        vec: "wandModel",
      });
    });

    it("should return no percentile on 404", async function () {
      const guess = "hand";
      const model = await getModel(guess, secret);
      assert.deepEqual(model, {
        percentile: undefined,
        vec: "handModel",
      });
    });

    it("should return highest percentile for secret", async function () {
      const model = await getModel(secret, secret);
      assert.deepEqual(model, {
        percentile: 1000,
        vec: "kunstModel",
      });
    });

    it("should reject on error", async function () {
      const guess = "fehler";
      await assert.rejects(() => getModel(guess, secret));
    });

    it("should return null on 404", async function () {
      const guess = "nein";
      const model = await getModel(guess, secret);
      assert.equal(model, null);
    });

    it("should reject if percentile request has status 500", async function () {
      const guess = "perc500";
      await assert.rejects(() => getModel(guess, secret), {
        message: `Could not get percentile data for ${guess}`,
      });
    });

    it("should reject if vector request has status 500", async function () {
      const guess = "vec500";
      await assert.rejects(() => getModel(guess, secret), {
        message: `Could not get word2vec data for ${guess}`,
      });
    });

    it("should escape input", async function () {
      const guess = "a b/c#d";
      const model = await getModel(guess, secret);
      expect(model).toEqual({
        percentile: "escapedPercentile",
        vec: "escapedVec",
      });
    });
  });

  describe("#getSimilarityStory()", function () {
    it("should get similarity", async function () {
      const simialarityStory = await getSimilarityStory(secret);

      assert.equal(simialarityStory, "kunstSimilarity");
    });

    it("should throw exception on error", async function () {
      assert.rejects(() => getSimilarityStory("fehler"));
    });
  });

  describe("#getNearby()", function () {
    it("should get nearby", async function () {
      const nearby = await getNearby("wand");

      assert.equal(nearby, "wandNearby");
    });

    it("should reject on error", async function () {
      assert.rejects(() => getNearby("fehler"));
    });
  });

  describe("#hint()", function () {
    let alertCalledWith: any;
    const alert = (message?: any) => {
      alertCalledWith = message;
    };

    const specificHintParams: (guesses: Array<GuessesEntry>) => HintParams = (
      guesses: Array<GuessesEntry>
    ) =>
      ({
        alert,
        secret,
        puzzleKey: 42,
        setError: () => {},
        setChronoForward: () => {},
        setLatestGuess: () => {},
        secretVec: new Array<number>(),
        puzzleNumberFromStorage: 0,
        guesses,
        setGuesses: () => {},
        stats: EMPTY_STATS,
        setStats: () => {},
        winState: DEFAULT_WIN_STATE,
        setWinState: () => {},
        hintsUsed: 0,
        setHintsUsed: jest.fn(),
        setNumberOfGuessesToEnd: () => {},
      } as HintParams);

    beforeEach(() => {
      alertCalledWith = undefined;
      jest
        .spyOn(dataModule, "getModel")
        .mockReturnValue(Promise.resolve({ vec: [42] }));
      jest.spyOn(guessModule, "doGuess");
    });

    it("should notify that no hints available", async function () {
      const guesses: Array<GuessesEntry> = [...Array(999).keys()].map((i) => ({
        similarity: 40 - i / 500,
        word: "" + i,
        percentile: 999 - i,
        index: 999 - i,
      }));

      const params = specificHintParams(guesses);

      await hint({
        ...params,
      });

      assert.equal(alertCalledWith, "Keine weiteren Hinweise verfügbar.");

      expect(guessModule.doGuess).not.toHaveBeenCalled();

      expect(params.setHintsUsed as jest.Mock).not.toHaveBeenCalled();
    });

    it("should show hint", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 999,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 998,
          index: 8,
        },
        {
          similarity: 34.29144630553826,
          word: "fliehkraft",
          percentile: 997,
          index: 6,
        },
      ];

      const params = specificHintParams(guesses);

      await hint({
        ...params,
      });

      assert.equal(alertCalledWith, undefined);

      expect(guessModule.doGuess).toHaveBeenCalledWith(
        expect.objectContaining({
          guess: "996thNearby",
          is_hint: true,
        })
      );

      assert.equal((params.setHintsUsed as jest.Mock).mock.calls[0][0](0), 1);
    });

    it("should show error", async function () {
      const guesses: Array<GuessesEntry> = [
        {
          similarity: 37.243274402231194,
          word: "lunge",
          percentile: 999,
          index: 7,
        },
        {
          similarity: 36.64150683700547,
          word: "gasdruck",
          percentile: 998,
          index: 8,
        },
      ];

      const params = specificHintParams(guesses);
      await hint({
        ...params,
      });

      assert.equal(alertCalledWith, "Holen von Hinweis fehlgeschlagen");
      expect(guessModule.doGuess).not.toHaveBeenCalled();

      expect(params.setHintsUsed as jest.Mock).not.toHaveBeenCalled();
    });
  });

  describe("#getSecrets()", function () {
    it("should get secrets", async function () {
      secretsApiResponse = {
        status: 200,
        ok: true,
        json: async () => {
          return {
            puzzleNumber: 832,
            data: [
              "bmljaHRz",
              "aGlsZmU=",
              "cmVnZWxtw6TDn2ln",
              "d2FmZmU=",
              "bnVtbWVy",
              "emVobg==",
              "emVudHJ1bQ==",
              "ZmVobGVy",
              "ZmVicnVhcg==",
            ],
          };
        },
      } as Response;

      const secretsResponsex = await getSecrets();

      expect(secretsResponsex).toEqual({
        puzzleNumber: 832,
        secrets: [
          "nichts",
          "hilfe",
          "regelmäßig",
          "waffe",
          "nummer",
          "zehn",
          "zentrum",
          "fehler",
          "februar",
        ],
      });
    });

    it("should throw on invalid response payload", async function () {
      secretsApiResponse = {
        status: 200,
        ok: true,
        json: async () => {
          return {
            invalidPayload: true,
          };
        },
      } as Response;

      expect(getSecrets).rejects.toThrowErrorMatchingInlineSnapshot(
        `"Cannot read properties of undefined (reading 'map')"`
      );
    });

    it("should throw on non-2xx response", async function () {
      secretsApiResponse = {
        status: 500,
        ok: false,
        statusText: "Internal Server Error",
      } as Response;

      expect(getSecrets).rejects.toThrowErrorMatchingInlineSnapshot(
        `"Could not get secrets (HTTP status ${secretsApiResponse.status}): ${secretsApiResponse.statusText}"`
      );
    });

    it("should throw on non-2xx response XXX", async function () {
      secretsApiResponse = undefined as unknown as Response; // to trigger TypeError

      expect(getSecrets).rejects.toThrowErrorMatchingInlineSnapshot(`"foo"`);
    });
  });
});

function successWithJson(jsonResult: string) {
  return {
    status: 200,
    json: async () => jsonResult,
  } as Response;
}

function errorWithStatus(status: number) {
  return {
    status,
  } as Response;
}
