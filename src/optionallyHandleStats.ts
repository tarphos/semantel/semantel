import { OptionallyHandleStatsParams } from "./types";

export function optionallyHandleStats(params: OptionallyHandleStatsParams) {
  params.setStats((oldStats) => {
    const newStats = { ...oldStats };
    const onStreak = oldStats.lastEnd === params.puzzleNumber - 1;

    newStats.lastEnd = params.puzzleNumber;
    if (params.won) {
      if (onStreak) {
        newStats.winStreak += 1;
      } else {
        newStats.winStreak = 1;
      }
      newStats.wins += 1;
    } else {
      newStats.winStreak = 0;
      newStats.giveups += 1;
    }
    newStats.hints += params.hintsUsed;

    return newStats;
  });
}
