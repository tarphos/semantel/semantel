// @ts-check
/**
 * @type {import('@stryker-mutator/api/core').PartialStrykerOptions}
 */
module.exports = {
  packageManager: "yarn",
  reporters: ["progress", "clear-text", "html"],
  testRunner: "jest",
  checkers: ["typescript"],
  tsconfigFile: "tsconfig.json",
  typescriptChecker: {
    prioritizePerformanceOverAccuracy: true,
  },
  coverageAnalysis: "off",
  incremental: true,
  timeoutFactor: 128,
  incrementalFile: "reports/stryker-incremental.json",
  htmlReporter: {
    fileName: "reports/mutation/mutation.html",
  },
  jest: {
    configFile: "jest.config.js",
  },
};
